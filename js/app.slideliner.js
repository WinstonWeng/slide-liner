app.slideliner={
	spiner:null,
	frameFetures:{
		"1":{extend:[2,3,4,5],extendIncreaseRight:5, point:{bottom:"40%",right:"50%"},text:"Max Speed: 20km/H<p>Almost as fast as a flying Izzy Folau.</p>",info:{bottom:"80%",right:"50%"}},
		/*"70":{extend:[71,72,73,74],extendIncreaseRight:0,point:{bottom:"50%",right:"40%"},text:"Samsung SlideLiner weight: 340kgs<p>Less than half the weight of the Wallabies forward pack.</p>",info:{bottom:"80%",right:"30%"}},*/
		"38":{extend:[39,40,41,42],extendIncreaseRight:15,"42":{point:{bottom:"8%",right:"0"}},point:{bottom:"4%",right:"48%"},text:"Track Length: 80m<p>Taking you from try line to try line.</p>",info:{bottom:"80%",right:"4%"}},
		"11":{extend:[12,13,14,15],extendIncreaseRight:1,point:{bottom:"49%",right:"39%"},text:"Custom Racing-Style Seats<p>Designed for comfortable viewing</p>",info:{bottom:"78%",right:"20%"}},
		"28":{extend:[29,30,31,32],extendIncreaseRight:2,point:{bottom:"49%",right:"61%"},text:"4-point harness system<p>Strap yourself in and watch the action.</p>",info:{bottom:"80%",right:"50%"}},
		"56":{extend:[57,58,59,60,61,62],extendIncreaseRight:0,point:{bottom:"58%",right:"74%"},text:"Integrated video cameras<p>4 individual cameras streaming live action.</p>",info:{bottom:"80%",right:"57%"}},
		"20":{extend:[21,22,23,24],extendIncreaseRight:0,point:{bottom:"49%",right:"40%"},text:"Built-in Samsung tablets<p>Stream, capture and share to social media, straight from the SlideLiner.</p>",info:{bottom:"85%",right:"34%"}}
	},
	getRotate:function(sidea,sideb){
	   var sidec,tanx,atanx,anglex;
	    sidec = Math.pow(sidea,2)+Math.pow(sideb,2);
		sidec = Math.sqrt(sidec);
		tanx = sidea / sideb;
	   	atanx = Math.atan(tanx);
	    anglex = atanx * 180 / Math.PI;
	    return anglex;
	},

	 getTransformCSS:function(x,y,deg,second){
		var css= {
				'-webkit-transform':'translate('+x+','+y+') rotate('+deg+'deg)',
				'transform':'translate('+x+','+y+')  rotate('+deg+'deg)'
			}
		if(second){
			css['-webkit-transition']='-webkit-transform '+second+'ms ease-out';
			css['transition']='transform '+second+'ms ease-out';
			css['-ms-transition']='transform '+second+'ms ease-out';
		}
		else{
			//css['-webkit-transition']='none';
			//css['transition']='none';
		}
		return css;
	},
	showFeature:function(index){
		var outerCon=$(".content.slideliner .frames");
		var con=$(".content.slideliner .frames .feature-bubble");
		var centerPoint = {x:con.width()/2,y:con.height()-50};
		var ff=app.slideliner.frameFetures[index];
		if(ff){
			outerCon.addClass("show-feature");
			/*var sidea = centerPoint.x-ff.right,
				sideb = centerPoint.y-ff.bottom,
				angle = app.slideliner.getRotate(sidea,sideb),*/
			var
				point = con.find(".point"),
				line  = con.find(".line"),
				info  = con.find(".info-container");
			info.html(ff.text);
			point.css(ff.point);
			info.css(ff.info).css({marginLeft:-info.width()/2,marginTop:-info.height()/2});
			
			var p1=point.position();
			var p2=info.position();
			var dir=-1;
			if((p1.left-p2.left)<0){
				dir=1;
			}
			var sidea = Math.abs(p2.left- p1.left);
			var sideb = Math.abs(p2.top - p1.top);
			var angle = app.slideliner.getRotate(sidea,sideb);
			var sidec = Math.pow(sidea,2)+Math.pow(sideb,2);
				sidec = Math.sqrt(sidec);
			//console.log("sidea:"+sidea+"	sideb:"+sideb+"	 angle:"+angle);
			var transformCSS=app.slideliner.getTransformCSS(0,0,angle*dir);
			transformCSS["-webkit-transform-origin"]="0 100%";
			transformCSS["transform-origin"]="0 100%";
			transformCSS.height=((sidec/outerCon.height())*100)+"%";
			if(isIE8){
				transformCSS.height=sideb;
				var right=parseInt(ff.point.right);
				var percent=((info.width()/2)/outerCon.width())*100;
				console.log("right:"+right+"	percent:"+percent);
				right=(right-percent)+"%";
				info.css({right:right,marginLeft:0});
			}
			$.extend(transformCSS,ff.point);
			//line.css(ff.point);
			var bottom=parseInt(transformCSS.bottom);
			var right =parseInt(transformCSS.right);
			console.log(bottom+"	"+right);
			line.css(transformCSS);
			//console.log(angle);
		}
		else{
			outerCon.removeClass("show-feature");
		}
	},
	onKeyDown:function(e){
		
		if(e.keyCode==38 || e.keyCode==37){
			e.stopPropagation();
			e.preventDefault();
			if(app.spiner){
				app.spiner.previousFrame();
			}
		}
		else if(e.keyCode==39 || e.keyCode == 40){
			e.stopPropagation();
			e.preventDefault();
			if(app.spiner){
				app.spiner.nextFrame();
			}
		}
	},
	onEnter:function(slide){
		$(document).on("keydown",app.slideliner.onKeyDown);
	},
	onExit:function(slide){
		$(document).off("keydown",app.slideliner.onKeyDown);
	},
	onExitStart:function(slide){
		 app.slideliner.onExit(slide);
	},
	start:function () {
		var setting={
			length:76,
            imgBaseSrc:"assets/spin3d-new/FINAL_Frame_.00[index].png",
            canvas:$(".content.slideliner .frames"),
            indexPadding:"0",
            startIndex:0,
            addEvent:true,
            from:null,
            getFilename:function(index){
            	return index;
            	//return index%2>0  ? index : index+1;
            }
		};
		var track=$(".spin-3d .track-outer");
		var toucher=new app.touchUtil(track,{preventDefault:true,stopPropagation:true});
		track.on("ontouchstart",function(e){
			e.stopPropagation();
			e.preventDefault();
		}).on("moveleft",function(e){
			console.log($(e.target).attr("class")+":moveleft");
	    	e.preventDefault();
	    	e.stopPropagation();
        }).on("moveright",function(e){
        	console.log($(e.target).attr("class")+":moveright");
            e.preventDefault();
	    	e.stopPropagation();
        }).on('swipeleft',function(e){
        	console.log($(e.target).attr("class")+":swipeleft");
        	e.preventDefault();
	    	e.stopPropagation();
        }).on('swiperight',function(e){
        	console.log($(e.target).attr("class")+":swiperight");
        	e.preventDefault();
	    	e.stopPropagation();
        });
        var toExtend={};
        for(var f in app.slideliner.frameFetures){
        	var ff=app.slideliner.frameFetures[f];
        	if(ff.extend){
        		$(ff.extend).each(function(i,item){

        			toExtend[item]=$.extend(true,{},ff);
        			var newPercent=(parseInt(toExtend[item].point.right)-(ff.extendIncreaseRight*(item-(parseInt(f)))));
        			if(newPercent<1){
        				newPercent=0;
        			}
        			toExtend[item].point.right=newPercent+"%";
        			if(ff[item]){
        				toExtend[item].point=ff[item].point;
        			}
        		});
        	}
        }
        $.extend(app.slideliner.frameFetures,toExtend);
		setting.showIndexChanged=function(index){
			$(".spin-slider .track  .inner").slider('setValue', index+1);
			app.slideliner.showFeature(index+1);
		};	
		$(".spin-slider .track  .inner").slider({tooltip:"hide",handle:"square",min:1,max:76,step:1,value:1}).on("slide",function(ev){
			app.spiner.showFrame(ev.value-1);
		});
		$(".spin-slider .track  .slider-handle").addClass("diamond-shape");
		app.spiner=new app.control.spin3d(setting);
		
	}
    
};