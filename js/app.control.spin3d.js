app.control.spin3d=function(options){
    var 
        //
        hasTouch    =  Modernizr.touch,
        toucher     =  null,
        slider      =  null,
        startEvt    =  null,   
        movePercent =  1,
        loadedTotal =  0,
        currentIndex=  0,
        items       =  [],
        lastMove    =  null,
        addEventTimer = null,
        defaults    =  {
                            length:31,
                            imgBaseSrc:null,
                            canvas:null,
                            indexPadding:null,
                            startIndex:0,
                            addEvent:true,
                            from:null
                       },

        _nextFrame   = function(){
            var next=currentIndex+1;
            if(next>defaults.length-1){
                next=0;
            }
            _showFrame(next);
        },

        _previousFrame = function(){
            var pre=currentIndex-1;
            if(pre<0){
                pre=defaults.length-1;
            }
            _showFrame(pre);
        },
        _showFrame   = function(showIndex){
            console.log("spin3d:playframe>"+showIndex);
            currentIndex = showIndex;
            defaults.canvas.find("img.show").removeClass("show");
            items[showIndex].addClass("show");
            if(options.showIndexChanged){
                options.showIndexChanged(showIndex);
            }
        },
        /*
         *
         */ 
        spinner     = function() {
            var i=0;
            if(defaults.from){
                i=defaults.from;
            }
            defaults.canvas.addClass("loading");
            var loader=defaults.canvas.find(".loader");
            for(i=0; i<defaults.length; i++){
                var img=$("<img>");
                var index=i;
                 var newFilename=index;
                if(defaults.getFilename){
                    newFilename=defaults.getFilename(index);
                }
                if(defaults.indexPadding!==null){
                    if(newFilename.toString().length===1){
                        newFilename=defaults.indexPadding+""+newFilename;
                    }
                }

                img.load(function(e){
                    loadedTotal++;
                    if(loader.length>0){
                        var percent=parseInt(Math.round((loadedTotal/defaults.length) * 100));
                        loader.find(".percent .inner").css({width:percent+"%"}).html(percent+"%");
                    }
                    if(loadedTotal==defaults.length){
                        defaults.canvas.removeClass("loading"); 
                        setTimeout(function(){ 
                            _showFrame(currentIndex);
                        },500);
                    }
                }).attr({src:defaults.imgBaseSrc.replace("[index]",newFilename)});
                if(i===defaults.startIndex){
                    currentIndex = i;
                }
                items.push(img);
                $(defaults.canvas).append(img);
            };
            movePercent = defaults.length/defaults.canvas.clientWidth;
            if(defaults.addEvent)
            {
                addTouchEvent();
            }
        },

        onTouchMove     =  function(e){
            e.stopPropagation();
            e.preventDefault();
            if(e.originalEvent){
                e.originalEvent.stopPropagation();
                e.originalEvent.preventDefault();
            }

            var width    = defaults.canvas.width(),
                newIndex = currentIndex;
                interval = width/defaults.length;
            console.log("spin3d:onTouchMove:"+e.info.diffX+"  "+interval);
            if(lastMove==null){
                if(Math.abs(e.info.diffX)>=(0)){
                    lastMove=e;
                    switch(e.info.dir){
                        case "left":
                            newIndex--;
                            break;
                        case "right":
                            newIndex++;
                            break;
                    }
                }
            }
            else
            {
                if((Math.abs(e.info.diffX)-Math.abs(lastMove.info.diffX))>=(0)) {
                     lastMove=e;
                     switch(e.info.dir){
                        case "left":
                            newIndex--;
                            break;
                        case "right":
                            newIndex++;
                            break;
                    }
                }
            }
            if(newIndex>(defaults.length-1)){
                newIndex=0;
            }
            if(newIndex<0){
                newIndex=defaults.length-1;
            }
            _showFrame(newIndex);
        },
        /*
         *
         */        
        addTouchEvent = function() {
        toucher=new app.touchUtil(defaults.canvas,{moveDistance:1,preventDefault:true,stopPropagation:true});
            var c=defaults.canvas;           
            c.on("moveleft",function(e){
                onTouchMove(e);}
            ).on("moveright",function(e){
                onTouchMove(e);
            });

            c.on("ontouchend",function(e){
                if(addEventTimer){
                    clearTimeout(addEventTimer);
                }
                //addEventTimer=setTimeout(function(){app.slider.toggleSwipe(true);},10);
                e.stopPropagation();
                e.preventDefault();
                if(e.originalEvent){
                    e.originalEvent.stopPropagation();
                    e.originalEvent.preventDefault();
                }
                console.log("touchend");
                lastMove=null;
                /*
                if(e.info.speedX>1000){
                    var o={index:currentIndex};
                    $(o).animate({index:defaults.length-1},{
                        duration:500,
                        step:function(value){
                            value=parseInt(value);
                            
                           console.log(value);
                           _showFrame(value);
                        }
                    });
                };
                */
                //console.log("speedX:"+e.info.speedX);
            });
            defaults.canvas.on('mousewheel', function(event) {
                 console.log("mousewheel:spin3d canvas");
                 event.preventDefault();
                 event.stopPropagation();
                 var newIndex=currentIndex+event.deltaY;
                 if(newIndex>(defaults.length-1)){
                    newIndex=0;
                 }
                 if(newIndex<0){
                    newIndex=defaults.length-1;
                 }
                 _showFrame(newIndex);
            });
        },

        /*
         *
         */
        _destory      = function() {
            //yazApp.removeAllElement(defaults.elm);
        };

        /*
         *
         */
        this.destory             = _destory;
        this.showFrame           = _showFrame;
        this.nextFrame           = _nextFrame;
        this.previousFrame       = _previousFrame;
        this.getCurrentFrameIndex= function(){return currentIndex};
        $.extend(defaults,options);
        spinner();
};