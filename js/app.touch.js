app.touchUtil=function(elm,setting){
	var 
        //
        hasTouch        = Modernizr.touch,
        touchstart      = "mousedown",
        touchend        = "mouseup",
        touchmove   	= "mousemove",
        startEvt		= null,
        lastMoveEvt 	= null,		
        root			= null,
        swipeDistance   = 50,
        moveDistance    = ((setting && setting.moveDistance) ? setting.moveDistance : 15),
        formatEvent = function(e){
        	if(hasTouch)
        	{	
        		if(e.originalEvent){
        			e=e.originalEvent;
        		}
        		if(e.touches && e.touches.length>0)
        		{	 
        			var touch = e.touches[0];
            		e.clientY = touch.pageY;
            		e.clientX = touch.pageX;
            	}
            	else if(lastMoveEvt)
            	{
            		e.clientX = lastMoveEvt.clientX;
            		e.clientY = lastMoveEvt.clientY;
            	}
            	else
            	{
            		e.clientX=0;
            		e.clientY=0;
            	}
        	}
            return e;
        },
        onTouchCancel  = function (e){
            console.log("onTouchCancel:"+e.touches)
        },

        onTouchLeave = function(e){
            console.log("onTouchLeave:"+e.touches);
        },
        /*
        */
        onTouchStart    = function (e) {
        	e=formatEvent(e);
        	startEvt=e;
            startEvt.startTime=new Date();
            lastMoveEvt=null;
            console.log("onTouchStart");
            $(document.body).addClass("on-drag");
            if(setting.onStartOnly==undefined)
            {
                elm.on(touchend, onTouchEnd);
            //elm.on("touchcancel",onTouchCancel);
            //elm.on("touchleave",onTouchLeave);
                elm.on(touchmove, onTouchMove);
            }
            root.trigger({type:"ontouchstart",e:startEvt});  
             if(setting){
                if(setting.preventDefault)
                {
                    e.preventDefault();
                }
                if(setting.stopPropagation){
                    e.stopPropagation();
                }
            } 
        },

        onTouchEnd		= function (e){
           $(document.body).removeClass("on-drag");
            elm.off(touchend, onTouchEnd);
            elm.off(touchmove, onTouchMove);
            console.log("onTouchEnd");
            var endTime=new Date();
            if(setting){
                if(setting.preventDefault)
                {
                    e.preventDefault();
                }
                if(setting.stopPropagation){
                    e.stopPropagation();
                }
            }
            
            e=formatEvent(e);
            var startX      = startEvt.clientX,
                startY      = startEvt.clientY,
                endX        = e.clientX,
                endY        = e.clientY,
                showIndex   = 0;
                diffX       = startX - endX,
                diffY       = startY-endY,
                data        ={
                    info:{
                        startY: startY,
                        startX: startX,
                        diffX : diffX,
                        diffY : diffY,
                        speedX :Math.abs(diffX)/((endTime.valueOf()-startEvt.startTime.valueOf())/1000),
                        speedY :Math.abs(diffY)/((endTime.valueOf()-startEvt.startTime.valueOf())/1000)
                    },
                    type  : "ontouchend"
                };
            
            if(Math.abs(diffX) >  Math.abs(diffY))
            {  
                 if(diffX>swipeDistance){
                 	data.type="swipeleft";
                    data.info.dir="left";
                    root.trigger(data);
                 }
                 else if(diffX<-swipeDistance)
                 {
                    data.info.dir="right";
                    data.type="swiperight";
                    root.trigger(data);  
                 }
                 
            }
            else
            {
                if(diffY>swipeDistance){
                    data.info.dir="down";
                    data.type="swipedown";
                    root.trigger(data);   
                }
                else if(diffY<-swipeDistance){
                    data.info.dir="up";
                    data.type="swipeup";
                    root.trigger(data);     
                }
                
            }
                //console.log(diffX+"     "+diffY);
            
         	startEvt = null;
            lastMoveEvt=null;
         	root.trigger({type:"ontouchend",info:data.info});   
        },

        onTouchMove   = function (e){
            console.log("onTouchMove:"+e.touches);
            if(setting){
                if(setting.preventDefault)
                {
                    e.preventDefault();
                }
                if(setting.stopPropagation){
                    e.stopPropagation();
                }
            }
            if(startEvt)
            {

                e=formatEvent(e);
                if(e.clientX && e.clientY)
                {
                	lastMoveEvt = e;
                    //console.log("startEvt:"+startEvt.clientX+" "+startEvt.clientY)
                    var startX      = startEvt.clientX,
                        startY      = startEvt.clientY,
                        endX        = e.clientX,
                        endY        = e.clientY,
                        showIndex   = 0;
                        diffX       = startX - endX,
                        diffY       = startY-endY,
                        data        ={
                            info:{
                                startY: startY,
                                startX: startX,
                                diffX : diffX,
                                diffY : diffY
                            },
                            type  : "ontouchmove"
                        };
                     //console.log(diffX);
                    if(!isNaN(diffX))
                    {
	                    if(Math.abs(diffX) >  Math.abs(diffY))
	                    {  
	                         if(diffX>moveDistance){
                                e.stopPropagation();
                                e.preventDefault();
	                            data.info.dir="left";
	                            data.type="moveleft";
	                            root.trigger(data);
	                         }
	                         else if(diffX<-moveDistance)
	                         {
	                            data.info.dir="right";
	                            data.type="moveright";
	                            root.trigger(data);  
	                         }
                             e.stopPropagation();
                             e.preventDefault();
	                    }
	                    else
	                    {
	                        if(diffY>moveDistance){
	                            data.info.dir="down";
	                            data.type="movedown";
	                            root.trigger(data);   
	                        }
	                        else if(diffY<-moveDistance){
	                            data.info.dir="up";
	                            data.type="moveup";
	                            root.trigger(data);     
	                        }
	                    }
                	}
                	else{
                		var k=0;
                	}
            	}
                //console.log(diffX+"     "+diffY);
            };
        	 
        },
        /*
        */
        addTouchEvent   = function (target) {
        	root=$(target);
            target.on(touchstart, onTouchStart);            
            $(document).on("dragstart",function(e){
                e.preventDefault();
                return false;
            });
        };
    if (hasTouch) {
        touchstart = "touchstart";
        touchend   = "touchend";
        touchmove  = "touchmove";
    };
	addTouchEvent(elm);
}