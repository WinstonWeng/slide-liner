app.gallerynew = {};
(function(g){
	g.api = null;
	g.setting ={
		container:null,
		template : null,
		data:null
	};
	g.getAllGalleryItems = function(){
		var allItems = [];
		$(app.gallerydata.photo).each(function(i,item){
			$(item.items).each(function(j,photo){
				photo.type = "photo";
				photo.location= item.location;
				allItems.push(photo);
			});
		});

		$(app.gallerydata.video).each(function(i,video){
				video.type = "video";
			 	allItems.push(video);
		});
		//allItems.sort(function() {return 0.5 - Math.random()});
		return allItems;
	},
	g.getAllGalleryItemInVideoAndPhoto = function(){
		var photoes=[];
		for(var i=app.gallerydata.photo.length-1;i>-1;i--){
			var p=app.gallerydata.photo[i];
			for(var j=p.items.length-1;j>-1;j--){
				var photo=p.items[j];
				photo.type = "photo";
				photo.thumburl=photo.thumburl.replace("/thumb/","/medium/");
				photo.location= p.location;
				photoes.push(photo);
			}
		};

		var videos=[]
		$(app.gallerydata.video).each(function(i,video){
				video.type = "video";
			 	videos.push(video);
		});	
		return {photo:photoes,video:videos};
	},
	g.galleryView= function(setting){
		var 
			loadedImage=[],
			loader=$('<div class="loader">'),
			mapi = null,
			startMasonry = function(){
				if(mapi == null)
				{
					setting.container.masonry({
						itemSelector: setting.itemSelector,
						columnWidth: setting.width ? setting.width : 320,
						isFitWidth:true
					});
					mapi = setting.container.data('masonry');
				}
			},
			checkAllLoaded = function (length,index,elems){
				loadedImage.push(index);
				console.log(length+"  "+index+"  "+loadedImage.length);
				if(loadedImage.length === length){
					//console.log("allLoaded");

					setting.container.removeClass("loading");
					loadedImage = [];
					if(mapi === null)
					{
						if(isIE8)
						{
							setTimeout(function(){startMasonry();},700);
						}
						else
						{
							setTimeout(function(){startMasonry();},200);
						}
					}
					else
					{
						setting.container.masonry('appended',elems);
					}
					loader.detach();
				}
			},
			loadMainGallery = function(items,selector){
		 		var mainViewSetting={
				    center: true,
				    items:2,
				    loop:true,
				    margin:10,
				    lazyLoad:true,
				    nav:true,
				    dots:false,
				    navText:["",""],
				    responsive:{
				        600:{
				            items:1
				        }
				    }
				};
				var owlContainer=$(selector);
				$(items).each(function(i,item){
					var mdiv=$("<div>").addClass("item");
					var mimg,responsiveDiv,responsiveDivInner;
					item.index=i;
	 				responsiveDiv=$("<div>").addClass("image-container");
					responsiveDivInner=$("<div>").addClass("inner");
					responsiveDiv.append(responsiveDivInner);
					mimg=$("<img>").addClass("owl-lazy").attr({'data-src':item.url});
					mimg.appendTo(responsiveDivInner);
					responsiveDiv.appendTo(mdiv);
					mdiv.appendTo(owlContainer);
				});
				return owlContainer.owlCarousel(mainViewSetting).data('owlCarousel');	
			},
			viewDetailSingle = function(gallery){
				var message=$(".template-gallery-view").val();
				app.ui.modal({
					className:"gallery-player gallery-"+gallery.id,
					message:message,
					modalSize:'modal-lg',
					title:" ",
					onHidden:function(){
						//$("html,body").css({overflow:""});
						if(app.gallery.videoPlayer)
						{
							app.gallery.videoPlayer.destroy();
						}
						app.gallery.videoPlayer=null;
					}, 
					onShown:function(ui){
						var container = ui.find(".carousel .image-container .inner");
						loader.detach().appendTo(container);
						ui.addClass(gallery.type);
						if(gallery.type=="video")
						{
							ui.find(".title-container").html(gallery.title).parent().show();
						}
						else
						{
							ui.find(".title-container").parent().hide();
						}
						if(gallery.location){
							ui.find(".location-container").html(gallery.location);
						}
						//$("html,body").css({overflow:"hidden"});
						addToSocialShare(ui,gallery);
						if(gallery.type=="photo"){
							$("<img>").load(function(e){
								loader.remove();
							}).error(function(e){
								loader.remove();
							}).attr({
								src:gallery.url
							}).appendTo(container);
						}
						else{
							$("<img>").load(function(e){
								loader.remove();
							}).error(function(e){
								loader.remove();
							}).attr({
								src:gallery.url,
								width:"100%"
							}).appendTo(container);
							var div=$("<div>").addClass("video-container").appendTo(container);
							app.gallery.playerVideo({videoUrl:gallery.videoUrl},div);
						}
					}
				});
			},
			addToSocialShare = function(ui,item){
				var urlToShare = siteUrl+"/fbshare.php?gallery="+item.thumburl;
				var shareOnTwitter="https://twitter.com/intent/tweet?url=[source_url]&text=Check%20out%20the%20Samsung%20SlideLiner%20in%20action&hashtags=SlideLiner";
				if(item.type=="video"){
					urlToShare="http://youtu.be/"+item.videoUrl;
				}
				var faceShare="https://www.facebook.com/sharer.php?app_id=257838867740570&u="+urlToShare+"&display=popup";
				var facebookshareurl   = "window.open('"+faceShare+"','','width=500,height=580')";
				shareOnTwitter = shareOnTwitter.replace("[source_url]",urlToShare);
				var twittershareurl = "window.open('"+shareOnTwitter+"','','width=500,height=580')";
				ui.find(".fb-share").attr({onclick:facebookshareurl});
				ui.find(".twitter-share").attr({onclick:twittershareurl});
			},
			populateGallery = function(data){
				var vp  = g.getAllGalleryItemInVideoAndPhoto();
				var rows = Math.ceil(data.length/5);
				var cols = 2;
				var count =0;
				for(var i=0;i<rows;i++){
					for(var j=0;j<cols;j++){
						var div=$("<div>").addClass("gallery-group-item waiting").appendTo(setting.container);
						$("<div>").addClass("gallery-group-item-inner").appendTo(div);
						 if(j==0)
						 {
							 if(count%4===0){
							 	div.addClass("single");
							 }
							 else
							 {
							 	div.addClass("four-item");
							 }
						}
						else 
						{
							if(count%4===3){
							 	div.addClass("single")
							}
							else
							{
								div.addClass("four-item");
							}
						}
						count++;
					}
				};

				var photoCounter = 0;
				var videoCounter = 0;

				var addImageToContainer = function(selector){
					$(setting.container.find(selector)).each(function(i,ui){
						var item=null;
						console.log(i);
						if((i%2)===0){
							if(photoCounter<vp.photo.length){
								item=vp.photo[photoCounter];
								photoCounter++;
							}
							else if(videoCounter<vp.video.length){
								item=vp.video[videoCounter];
								videoCounter++;
							}
						}
						else
						{
							if(videoCounter<vp.video.length){
								item=vp.video[videoCounter];
								videoCounter++;
							}
							else if(photoCounter<vp.photo.length){
								item=vp.photo[photoCounter];
								photoCounter++;
							}
						}
						addImage(data,$(ui).find(".gallery-group-item-inner"),item,[]);
					});
				};
				addImageFourToContainer = function(selector){

					$(setting.container.find(selector)).each(function(j,ui){
						for(var i=0; i<4;i++){
							var item = null
							
							if(i===0 || i==3){
								if(photoCounter<vp.photo.length){
									item=vp.photo[photoCounter];
									photoCounter++;
								}
								else if(videoCounter<vp.video.length){
									item=vp.video[videoCounter];
									videoCounter++;
								}
							}
							else
							{
								if(videoCounter<vp.video.length){
									item=vp.video[videoCounter];
									videoCounter++;
								}
								else if(photoCounter<vp.photo.length){
									item=vp.photo[photoCounter];
									photoCounter++;
								}
							}
							addImage(data,$(ui).find(".gallery-group-item-inner"),item,[]);
						}
					});
				};
				addImageToContainer(".gallery-group-item.single");
				addImageFourToContainer(".gallery-group-item.four-item");
			},
			populateGalleryV1 = function(data){
				var elems=[];	
				var group=Math.ceil(data.length / 2);
				var nextSingle=true;
				var goBy2=0;
				var count=0;
				for(var i=0;(i<=group && (count<data.length));i++){
					var div=$("<div>").addClass("gallery-group-item").appendTo(setting.container);	
					if(i==0){
						nextSingle = true;
					}
					if(nextSingle)
					{
						div.addClass("single");
						addImage(data,div,data[count],elems);
						count++;
						if(i==0){
							nextSingle =false
						}
						else
						{
							goBy2++;
							if(goBy2==2){
								nextSingle=false;
								goBy2=0;
							}
						}
					}
					else
					{
						for(var j=0;j<4;j++){
							addImage(data,div,data[count],elems);
							count++;
						}
						goBy2++;
						if(goBy2==2){
							nextSingle=true;
							goBy2=0;
						}
					}
					console.log("i="+i+" count="+count);
				}
				
			},
			addImage = function(data,container,item,elems){
				if(item)
				{
					var div = $(setting.template);
					div.addClass(item.type);
					var img = $("<img>").load(function(e){
						checkAllLoaded(data.length,i,elems);
					}).error(function(e){
						checkAllLoaded(data.length,i,elems);
					}).attr({
						src:item.thumburl
					}).appendTo(div.find(".media-container"));
					img.click(function(e){
						viewDetailSingle(item);
					});
					div.find(".btn-play").click(function(e){
						viewDetailSingle(item);
					});
					div.find(".title").html(item.location);
					addToSocialShare(div,item);
					div.appendTo(container);
				}
			},
			populateGallery_new  = function(data){
				
				$(data).each(function(i,item){
					item.items=data;
					var div = $(setting.template);
					div.addClass(item.type);
					var img = $("<img>").load(function(e){
						checkAllLoaded(data.length,i,elems);
					}).error(function(e){
						checkAllLoaded(data.length,i,elems);
					}).attr({
						src:item.thumburl
					}).appendTo(div.find(".media-container"));
					img.click(function(e){
						viewDetailSingle(item);
					});
					div.find(".btn-play").click(function(e){
						viewDetailSingle(item);
					});
					div.find(".title").html(item.location);
					addToSocialShare(div,item);
					div.appendTo(setting.container);
				});
			};
			populateGallery(g.setting.data);

	},
	g.start = function(setting){
		app.gallery.installYouTube();
		g.setting = $.extend(g.setting,setting);
		g.setting.data = g.getAllGalleryItems();

		if(g.api == null){
			g.api =new g.galleryView(g.setting);
		}
	}
})
(app.gallerynew)