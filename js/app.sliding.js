app.slidingNav=function(){
  	 var 
	 		slides=[
	 			{title:'home',view:'home',route:'home',template:"slides/_home.php",versaTag:"http://www.samsung.com.au/samsungslideliner"},
	 			{title:'Samsung slideliner',view:'slideliner',route:'slideliner',template:"slides/_slideliner.html",versaTag:"http://www.samsung.com.au/samsungslideliner/whatsslideliner"},
	 			{title:'Samsung SMART TVs',view:'smarttv',route:'smarttv',template:"slides/_smarttv.html",versaTag:"http://www.samsung.com.au/samsungslideliner/tvrange"},
	 			//{title:'Prizes',view:'prizes',route:'prizes',template:"slides/_prizes.html",versaTag:"http://www.samsung.com.au/samsungslideliner/prizes"},
	 			//{title:'Enter now',view:'winseat',route:'winseat',template:"slides/_winseat.html",versaTag:"http://www.samsung.com.au/samsungslideliner/enternow"},
	 			//{title:'Game schedule',view:'game',route:'game',template:"slides/_game.html",versaTag:"http://www.samsung.com.au/samsungslideliner/schedule"},
	 			{
	 				title:'Gallery',
	 				view:'gallery',
	 				route:'gallery',
	 				template:"slides/_gallery.html",
	 				versaTag:"http://www.samsung.com.au/samsungslideliner/gallery"
	 			},
	 			{title:'Social',view:'social',route:'social',template:"slides/_social.html",versaTag:"http://www.samsung.com.au/samsungslideliner/social"}
	 		],
	 		slideContainer   =		$(".main-content"),
	 		navContainer	 =		$(".navbar ul"),
	 		footer			 = 	    $(".footer"),
	 		root 			 =      $(document.body),
	 		me 				 =		this,
	 		isMobile		 =      function(){return $(".navigation-small").css("display").toLowerCase()!=="none";},
	 		toucher			 = 		null,
	 		isHashChangeFromCode = false;
	 		current 		 =      {
	 			 slideContainer:{
	 			 	currentSlideIndex:0,
	 			 	translate:0,
	 			 	isSliding:false
	 			 }
	 		};
	this.getTransformCSS=function(x,y,second){
		var css= {
				'-webkit-transform':'translate('+x+','+y+')',
				'transform':'translate('+x+','+y+')'
			}
		if(second){
			css['-webkit-transition']='-webkit-transform '+second+'ms ease-out';
			css['transition']='transform '+second+'ms ease-out';
			css['-ms-transition']='transform '+second+'ms ease-out';
		}
		else{
			css['-webkit-transition']='none';
			css['transition']='none';
		}
		if(isIE8){
			css={
				left:x
			}
		}
		return css;
	};
	this.loadSlide=function(slide,callback){
		//var loader=$("<div>").addClass("loader");
		//loader.appendTo(slide.ui.div);
		 
		slide.ui.div.load(slide.template,function(e){
			//loader.remove();
			slide.loaded=true;
			slide.ui.div.removeClass("loader").removeClass("center-no-width");
			if(callback){
				callback(slide);
			}
		});
		
	}
	this.toggleSwipe=function(allowSwipe){
		if(allowSwipe)
		{
			slideContainer.on('swipeleft',me.onTouchEnd);
			slideContainer.on('swiperight',me.onTouchEnd);
			slideContainer.on("moveleft",me.onTouchMove);
			slideContainer.on("moveright",me.onTouchMove);
		}
		else
		{
			slideContainer.off('swipeleft',me.onTouchEnd);
			slideContainer.off('swiperight',me.onTouchEnd);	
			slideContainer.off("moveleft",me.onTouchMove);
			slideContainer.off("moveright",me.onTouchMove);
		}
	};
	this.goToSlideByRoute= function(route){
		for(var i=0;i<slides.length;i++){
			if(slides[i].route===route){
				if(i!==current.slideContainer.currentSlideIndex){
					me.goToSlide(slides[i]);
				}
				break;
			}
		}
	};
	this.goToSlide=function(slide){
		if(!current.slideContainer.isMobile)
		{
			//console.log("goToSlide:"+slide.index);
			//me.toggleNavBar(false);
			var x=slide.index*-100;
			var timing=800;
			var cIndex=current.slideContainer.currentSlideIndex;
			console.log("goToSlide:"+slide.index);
			current.slideContainer.isSliding=true;
			$(document.body).removeClass(slides[current.slideContainer.currentSlideIndex].route);
			$(document.body).addClass(slide.route);
			slide.ui.div.removeClass("exiting").addClass("entering");
			slides[current.slideContainer.currentSlideIndex].ui.div.addClass("exiting");
			var currentView=slides[current.slideContainer.currentSlideIndex].view;
			if(typeof(window[currentView+"Slide"]) !== "undefined"){
				if(window[currentView+"Slide"].onExitStart)
				{

					window[currentView+"Slide"].onExitStart(slide);
				}
			};
			$(document.body).addClass("sliding");
			slideContainer.addClass("sliding-working").css(me.getTransformCSS(x+"%",0,timing));
			console.log("goToSlide:"+slide.index+" 	translate:"+x);
			navContainer.find("li.active").removeClass("active");
			current.slideContainer.translate=x;
			current.slideContainer.currentSlideIndex=slide.index;
			
			var onSlideLoaded = function(slide){
				if(typeof(window[slide.view+"Slide"]) !== "undefined"){
					window[slide.view+"Slide"].onEnter(slide);
				};
			}
			setTimeout(function(){
				$(document.body).removeClass("sliding");
				current.slideContainer.isSliding=false;
				onSlideLoaded(slide);
				if(slide.index>0){
					
					var previousSlide=null;
					previousSlide=slides[cIndex];					 
					
					previousSlide.ui.div.removeClass("exiting");
					if(typeof(window[previousSlide.view+"Slide"]) !== "undefined"){
						window[previousSlide.view+"Slide"].onExit(previousSlide);
					}
				}
				slide.ui.div.removeClass("entering");
				slideContainer.removeClass("sliding-working");
			},timing+400);
			if(!slide.loaded){
				me.loadSlide(slide,onSlideLoaded);
			}
			//window.location.hash=slide.route;
			window.parent.postMessage(JSON.stringify({t:{type:"hashchange",hash:slide.route}}),"*");
			isHashChangeFromCode=true;
			slide.ui.li.addClass("active");
			setTimeout(function(){
				if(isMobile())
				{
					me.toggleNavBar(false);
				}
			},0);
		}
	};
	this.toggleNavBar=function(show){
		if(show)
		{
			$(".navbar-outer").removeClass("out");
			$(".navigation-small").addClass("out");
			footer.removeClass("out");
		}
		else
		{
			$(".navbar-outer").addClass("out");		
			$(".navigation-small").removeClass("out");
			footer.addClass("out");
		}
	};
	this.getCurrentSlide=function(){
		return slides[current.slideContainer.currentSlideIndex];
	};
	this.onTouchEnd  = function(e){
		console.log($(e.target).attr("class"));
		if($(e.target).hasClass("frames")){
			return;
		}
		if($(e.target).hasClass("track-outer")){
			return;
		}
		if(e.info && $(".modal-backdrop").length==0)
		{

			//console.log("sliding onTouchEnd:"+e.info);
			var slideIndex=current.slideContainer.currentSlideIndex;
			var isLeftAndRight=false;
			var minMove=$(window).width()*0.3;
			if(minMove>300){
				minMove=300;
			}
			if(Math.abs(e.info.diffX)>minMove)
			{
				switch(e.info.dir){
					case "left":
						slideIndex+=1;
						isLeftAndRight=true;
						break;
					case "right":
						slideIndex--;
						isLeftAndRight=true;
						break;
				}
			}
			console.log("sliding onTouchEnd:"+e.info.diffX+"	"+minMove+"		"+slideIndex+"		"+current.slideContainer.currentSlideIndex+"	"+isLeftAndRight);
			//e.preventDefault();
			/*if(slideIndex==current.slideContainer.currentSlideIndex){
				//return;
			}*/
			if(slideIndex>=0 && slideIndex<slides.length){
				me.goToSlide(slides[slideIndex]);
				//window.location.hash=slides[slideIndex].route;
				console.log("touchend:goToSlide:"+slideIndex);
			}
			else{
				console.log("touchend:goToSlide:"+current.slideContainer.currentSlideIndex);
				//window.location.hash=slides[current.slideContainer.currentSlideIndex].route;
				me.goToSlide(slides[current.slideContainer.currentSlideIndex]);	
			}
		}
		 
	}; 
	this.onTouchMove = function(e){
		if($(e.target).hasClass("track-outer")){
			return;
		}
		if(e.info)
		{
			console.log("sliding:onTouchMove"+e.info);
			if(e.info.dir=="left" || e.info.dir=="right")
			{ 
				slideContainer.addClass("sliding-working")
				var mvx=e.info.diffX;
				var percent=(mvx/($(window).width()))*100;
				var x=current.slideContainer.translate-percent;
				//current.slideContainer.translate=x;
				slideContainer.css(me.getTransformCSS(x+"%",0,null));
			}
		}
	};
	this.start=function ()  {
		$(slides).each(function(i,slide){
		  	var 
		  	  	  li 			 =		$("<li>"),
		  	  	  a  			 =		$("<a>").attr({"href":"javascript:void(0)"}).html(slide.title),
		  	  	  div			 =		$("<div>").addClass("slide").attr({'viewUrl':slide.template}).addClass(slide.view),
		  	  	  x				 =       100*(i);
		  	 
		  	 a.css({paddingRight:i*(2+i/5)+35});
		  	 
		  	 a.click(function(e){
		  	 	if(isOkToLive)
		  	 	{
			  	 	var prop5 = "au:campaign:seau:<%=omitureName%>:"+slide.route;
			  	 	var hier1  = "au>campaign>seau><%=omitureName%>>"+slide.route;
			  	 	if(window.s_gi)
			  	 	{
				  	 	var s=s_gi(s_account);
				  	 	s.pageName="au:campaign:seau:<%=omitureName%>:"+slide.route;
				  	 	s.prop5 = prop5;
				  	 	s.hier1=hier1;
				  	 	s.linkTrackVars='eVar33,events';
						s.linkTrackEvents='event45';
						s.eVar33="au:campaign:seau:<%=omitureName%>:"+slide.route;
						s.events='event45';
						s.tl(this,"o",s.eVar33);
					}
					_gaq.push(['_trackEvent', "Menu", 'Click', slide.title]);
			  	 	if(slide.versaTag){
						window.sendVersaTag(slide.versaTag,true);
					}
					me.goToSlide(slide);
					document.location.hash=slide.route;
		  	 	}
		  	 });
		  	 div.addClass("loader center-no-width").css(me.getTransformCSS((x)+'%', 0));
		  	 a.appendTo(li);
		  	 li.appendTo(navContainer);
		  	 div.appendTo(slideContainer);
		  	 slide.index=i;
		  	 slide.loaded=false;
		  	 slide.ui={
		  	 	li:li,
		  	 	a:a,
		  	 	div:div
		  	 }
		});
		var hashSlide=window.location.hash;
		if(hashSlide){
			hashSlide=hashSlide.split("/?")[0];
			console.log(hashSlide);
			hashSlide=hashSlide.replace("#","");
			var i=0,isCorrectHash=false;
			for(var j=0;j<slides.length;j++){
				if(hashSlide===slides[j].route){	
					isCorrectHash=true;
					i=j;
					break;
				}
			}
			if(isCorrectHash){
				//slides[i].ui.a.click();
				me.goToSlide(slides[i]);
			}
			else
			{
				//slides[0].ui.a.click();
				me.goToSlide(slides[0]);
			}
			
		}
		else
		{
			//slides[0].ui.a.click();
			me.goToSlide(slides[0]);	
		}
		if(!isMobile())
		{
			me.toggleNavBar(true);
		}
		//slideContainer.bind("touchend",me.onTouchEnd);
		 
		//toucher=new app.touchUtil(slideContainer,{moveDistance:20});
		me.toggleSwipe(true);
		var allTryTimes=0;
		slideContainer.on('_xxxxxmousewheel', function(event) {
			 
			console.log("mousewheel:slideContainer");
			if(!isMobile())
			{
				console.log("event.deltaY="+event.deltaY);				
				var slideIndex=current.slideContainer.currentSlideIndex+(event.deltaY>0 ? -1 : 1);
				var currentSlideIndexItem=slides[current.slideContainer.currentSlideIndex];
				var elm=currentSlideIndexItem.ui.div.find(".content")[0];

				if(elm.scrollTop == 0 || (elm.scrollTop == (elm.scrollHeight - elm.offsetHeight))) { 
					if(slideIndex>=0 && slideIndex<slides.length)
					{
						allTryTimes++;
						var canContinue=true;
						elm=$(elm);
						//if(elm.HasScrollBar()){
						if(true){
							var dir=(event.deltaY>0 ? "up" :"down");
							if(dir=="up"){
								elm.removeAttr("scrolltrydown");
								allTryTimes=0;		
							}
							else
							{
								elm.removeAttr("scrolltryup");
								allTryTimes=0;
							}
							var attrName="scrolltry"+dir;
							var tryTimes=elm.attr(attrName);
							if(tryTimes && parseInt(tryTimes)==20){
								canContinue=true;
							}
							else
							{
								canContinue=false;
								var tt={};
								if(tryTimes){
									tt[attrName]=parseInt(tryTimes)+1;
									elm.attr(tt);					
								}
								else
								{
									tt[attrName]=1;
									elm.attr(tt);	
								}
							}
						}
						if(allTryTimes>5){
							canContinue=true;
						}
						if(canContinue &&  !current.slideContainer.isSliding)
						{
							allTryTimes=0;
							elm.removeAttr("scrolltryup").removeAttr("scrolltrydown");
	    					me.goToSlide(slides[slideIndex]);
	    				}
	    			}
				}
			}
			
		});
		$(window).on("resize",function(e){
			if(isMobile()){
				me.toggleNavBar(false);
			}
			else
			{
				me.toggleNavBar(true);
			}
		}).on("hashchange",function(e){
			me.goToSlideByRoute(window.location.hash.replace("#",""));
		});

		$(".navigation-small .mobile-button-container").click(function(e){
			$(".navbar-outer, .navigation-small").toggleClass("out");
		});
	}
};