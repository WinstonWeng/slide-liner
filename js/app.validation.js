
app.validation={
	preconfig:{
		firstname:{rules:{required:true},messages:{required:'Please enter your first name'}},
		surname:{rules:{required:true},messages:{required:'Please enter your surname'}},
		serialnumber:{rules:{required:true,isSerialNumber:true},
			messages:{
				required:'Please enter product serial number',
				isSerialNumber:"Serial number must be a 14/15 character alphanumeric code"
			}
		},
		phonenumber:{rules:{required:true,digits:true,phoneCheck:true},messages:{required:'Please enter your phone number',digits:"Please enter numbers only",phoneCheck:"Please enter valid Australian phone number"}},
		email:{rules:{required:true,isEmail:true},messages:{required:'Please enter your email',isEmail:"Invalid email address"}},
		emailIfAny:{rules:{required:false, isEmailIfAny:true},messages:{isEmailIfAny:"Invalid email address"}},
		address:{rules:{required:true},messages:{required:'Please enter address'}},
		suburb:{rules:{required:true},messages:{required:'Please enter suburb'}},
		postcode:{rules:{required:true,digits:true,isAuPostcode:true},messages:{required:'Please enter postcode',digits:"Please enter numbers only",isAuPostcode:"Please enter valid Australian postcode"}},
		state:{rules:{required:true},messages:{required:'Please select state'}},
		receiptnumber:{rules:{required:true},messages:{required:'Please enter receipt number'}},
		productpurchased:{rules:{required:true},messages:{required:'Please select product purchased'}},
		fileinput:{rules:{required:true},messages:{required:'Please attach your receipt'}},
		selectmonth:{rules:{required:true},messages:{required:'Please select month'}},
		selectday:{rules:{required:true},messages:{required:'Please select day'}},
		selectyear:{rules:{required:true},messages:{required:'Please select year'}},
		comment:{rules:{required:true},messages:{required:'Please enter your experience'}},
		termcondition:{rules:{required:true},messages:{required:'Please agree with our terms and conditions'}}
	},
	utility:{
		isEmail:function(email){
			var pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
					// pattern=new
					// RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
			pattern=/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;
			return pattern.test(email);
		},
		isHomePhoneNumber: function (value) {
	        var patterns = [/^(\+?61|0)\d{9}$/, /^\D*0(\D*\d){9}\D*$/, /^\d{8}$/];
	        var result = false;
	        while (!result && patterns.length > 0) {
	            var p = (patterns[0]);
	            result = p.test(value.replace(/\s+/g, ""));
	            patterns.splice(0, 1);
	        }
	        return result;
	    },
	    isMobileNumber: function (value) {
	        var pattern = /^(\+?61|0)4\d{8}$/;
	        return pattern.test(value.replace(/\s+/g, ""));
	    },
	    isPhoneNumber: function (value) {
	        if (this.isHomePhoneNumber(value)) {
	            return true;
	        }
	        if (this.isMobileNumber(value)) {
	            return true;
	        }
	        return false;
	    },
	    isAuPostcode: function(value){
	    	var pattern=/^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$/;
	    	return pattern.test(value.replace(/\s+/g, ""));
	    },
	    isAlphaNumeric:function(value){
	    	var pattern=/^[a-zA-Z0-9]+$/;
	    	return pattern.test(value.replace(/\s+/g, ""));	
	    }
	},
	commonSetting:{
			getParentUI:function(element){
				element=$(element);
				var ui=null;
				 
				switch($(element).prop("tagName").toLowerCase()){
					case "select":
						ui=element.parent();					 
						break;
					case "input":
						if(
							element.parent().hasClass("email-wrapper") 
							|| 
							element.parent().hasClass("datetimepicker-wrapper")
						  )
						{
							ui=element.parent();
						}

						break;

				}
				return ui;
			},
			success:function(label,element){
				label.remove();
			},
			highlight:function(element, errorClass, validClass) {
				element=$(element);
				var ui=element;
	 			var parentUI=app.validation.commonSetting.getParentUI(element);
				if(parentUI){
					ui=parentUI;
				}
				ui.addClass(errorClass);
			},
			unhighlight:function(element, errorClass, validClass) {
				element=$(element);
				var ui=element;
				var parentUI=app.validation.commonSetting.getParentUI(element);
				if(parentUI){
					ui=parentUI;
				}
				ui.removeClass(errorClass);
			},
			errorPlacement:function(error,element){
				var ui=element;
				var parentUI=app.validation.commonSetting.getParentUI(element);
				if(parentUI){
					ui=parentUI;
				}
				error.insertAfter(ui);
			}
	},
	getSetting:function(root,formID){
			var setting={
				rules:{},
				messages:{}
			}
			var formID=root.attr("formID");			 
			var nameStart="fieldValidation_";
			if(formID){
				nameStart=formID+"_";
			}
			root.find("[validation]").each(function(i,control){
				var c=$(control);
				var ruleDEF=c.attr("validation");
				var name=c.attr("name");
				if(name===null || name===undefined){
					name=nameStart+i;
					c.attr({name:name});
				}
				var depends=c.attr("depends");
				var equalTo=c.attr("validationEqualTo");
				switch(ruleDEF){
					case "companyName":
						setting.rules[name]={
								required:function(element){return root.find("#chkOnBehalfBusiness")[0].checked;}
						};
						break;
					default:
						setting.rules[name]=$.extend({},app.validation.preconfig[ruleDEF].rules);
						if(depends){
							for(var f in setting.rules[name]){
								setting.rules[name][f]={
									depends:function(element){
										return !$(depends)[0].checked;
									}
								}	
							}
							
						}
						if(equalTo){
							setting.rules[name].equalTo=root.find("[name='"+equalTo+"']")[0];
						}
						break;	
				}

				setting.messages[name]=app.validation.preconfig[ruleDEF].messages;
			});
			var ignore=[];
			root.find("[validationIngore='true']").each(function(i,item){
				var name=$(item).attr("name");
				ignore.push(item);
			});
			if(ignore.length>0){
				setting.ignore=ignore;
			}
			return setting;
	},
	validate:function(form,errorPlacement){
		if(form.length>0){
			var setting=app.validation.getSetting(form);
			$.extend(setting,app.validation.commonSetting);
			if(errorPlacement){
				setting.errorPlacement=errorPlacement;
			}
			else
			{
				setting.errorPlacement=function(error,element){

					var name=$(element).attr("name");
					var ui=form.find("[errorcontainerfor='"+name+"']");
					if(ui.length>0){
						error.appendTo(ui);
						return;
					}
					ui=form.find("div[for='error-container']");
					if(ui.length>0){
						error.appendTo(ui);
						return;
					}			
					var parentUI=app.validation.commonSetting.getParentUI(element);
					if(parentUI){
						error.insertAfter(parentUI);
						return;
					}
					error.insertAfter(element);
				}
			}
			form.validate(setting);	
		}
	},
	start:function(){
		$("form[formvalidation='common']").each(
			function(i,form){
				var validation=new app.validation.validate($(form));
			}
		);
	}
}
$(document).ready(function(e){
	$.validator.addMethod('isEmail', function (value, element) {
	  	return app.validation.utility.isEmail(value);
	}, '');

	$.validator.addMethod("phoneCheck", function (value, element) {
        var valid;
        valid = app.validation.utility.isPhoneNumber(value);
        return valid;
    }, "");

	$.validator.addMethod("isAuPostcode", function (value, element) {
        var valid;
        valid = app.validation.utility.isAuPostcode(value);
        return valid;
    }, "");
    
    $.validator.addMethod("isAlphaNumeric", function (value, element) {
        var valid;
        valid = app.validation.utility.isAlphaNumeric(value);
        return valid;
    }, "");

     $.validator.addMethod("isSerialNumber", function (value, element) {
        var valid=false;
        if(value.length==14 || value.length==15)
        {
        	valid = app.validation.utility.isAlphaNumeric(value);	
        }
        return valid;
    }, "");
	
	app.validation.start();
});