app.social=function(option){
	var started = false;
	this.start   = function(){
			if(started){
				return;
			}
			started=true;
			var $container = option.container;
			var mapi = null;
			var isLoading = false;
			var pageindex = 1;
			var totalPage = null;
			var endOfPage=false;
			var loadedImage=[];
			var loader=$('<div class="loader">');
			var startMasonry =function()
			{
				if(mapi==null)
				{
					$container.masonry({
						itemSelector: option.namespace +' .social-item',
						columnWidth: option.width ? option.width : 320,
						isFitWidth:true
					});
					mapi=$container.data('masonry');
				}
			};
			var checkAllLoaded = function (length,index,elems){
				loadedImage.push(index);
				console.log(length+"  "+index+"  "+loadedImage.length);
				if(loadedImage.length===length){
					//console.log("allLoaded");

					$(option.namespace +" .social-item.loading").removeClass("loading");
					loadedImage=[];
					if(mapi===null)
					{
						if(isIE8)
						{
							setTimeout(function(){startMasonry();},700);
						}
						else
						{
							setTimeout(function(){startMasonry();},200);
						}
					}
					else
					{
						$container.masonry('appended',elems);
					}
					loader.detach();
				}
			};
			var findLinks = function(s,sourceUrl) {
		          var hlink=/\S+\.\S+/g;
		          return (s.replace(hlink, function(a, b, c) {
		              return ('<a href="'+a+'" target="_blank">'+a+'</a>');
		          }));
		     };
			var getShareLink = function (url){
				return "window.open('"+url+"','','width=500,height=580')";
			};
			var showInContainer = function(data){
				var elems=[];
				$(data).each(function(i,feed){
					feed.source=feed.SocialMessage.socialType=="ig" ? "instagram" :"twitter";
					feed.type=(feed.SocialMessage.videoUrl && feed.SocialMessage.videoUrl.length>5 && !isIE8) ? "video" : "photo";
					feed.media_url_https=feed.SocialMessage.videoUrl;
					feed.media_url=feed.SocialMessage.postImageLarge;
					feed.source_id=feed.SocialMessage.postId;
					feed.screen_name=feed.SocialMessage.profileName;
					feed.source_url=feed.SocialMessage.postUrl;
					feed.profile_image_url=feed.SocialMessage.profileImage;
					feed.name=null;
					feed.text=feed.SocialMessage.postMessage;
					var div=$($(".template-social-feed-item."+feed.type).val()).addClass(feed.source);
					div.addClass(feed.type);
					if(feed.type==='photo'){
						$("<img>").load(function(e){
							checkAllLoaded(data.length,i,elems);
						}).error(function(e){ 
							checkAllLoaded(data.length,i,elems);
						}).attr({src:feed.media_url}).appendTo(div.find(".media-container"));
					}
					else if(feed.media_url_https){

						var video=$("<video>").attr({width:320,head:320,controls:true}).append(
							$("<source>").attr({type:'video/mp4',src:feed.media_url_https})
							).appendTo(div.find(".media-container"));
						$("<img>").load(function(e){
							video.attr({poster:feed.media_url});
							checkAllLoaded(data.length,i,elems);
						}).error(function(e){
							checkAllLoaded(data.length,i,elems);
						}).attr({src:feed.media_url})
					}
					var sourceUrl=null;
					var retweeturl="https://twitter.com/intent/retweet?tweet_id=[id]";
					var twitterShareFB="https://www.facebook.com/sharer/sharer.php?u=https://twitter.com/[username]/status/[id]";
					var shareOnTwitter="https://twitter.com/intent/tweet?url=[source_url]&text=Check%20out%20the%20Samsung%20SlideLiner%20in%20action&hashtags=SlideLiner";
					var instagramShareFB="https://www.facebook.com/sharer/sharer.php?u=[source_url]";
					
					var userlinkUrl=null;
					switch(feed.source){
						case "twitter":
							sourceUrl=("https://twitter.com/[username]/status/[id]").replace('[id]', feed.source_id).replace("[username]",feed.screen_name);
							userlinkUrl="http://www.twitter.com/";
							div.find(".action-row a.retweet").attr({onclick:getShareLink(retweeturl.replace('[id]',feed.source_id))});
							div.find(".action-row a.fb-share").attr({onclick:getShareLink(twitterShareFB.replace('[id]', feed.source_id).replace("[username]",feed.screen_name))});
							break;
						case  "instagram":
							sourceUrl=feed.source_url;
							userlinkUrl="http://instagram.com/";
							div.find(".action-row a.twitter-share").attr({onclick:getShareLink(shareOnTwitter.replace('[source_url]',feed.source_url))});
							div.find(".action-row a.fb-share").attr({onclick:getShareLink(instagramShareFB.replace('[source_url]',feed.source_url))});
							break;
					}			
					var a =$("<a>").attr({href:userlinkUrl+feed.screen_name+"/",target:"_blank"});
					a.append($("<img>").attr({src:feed.profile_image_url}));
					div.find(".profile-image").append(a);
					a =$("<a>").attr({href:userlinkUrl+feed.screen_name+"/",target:"_blank"}).html(feed.screen_name);
					//div.find(".user-name .name").append(a)
					div.find(".user-name .screen-name").append(a);
					div.find(".post-message").html(findLinks(feed.text,sourceUrl));	
					div.appendTo($container);
					elems.push(div[0]);
				});
			};
			var  loadNextPage = function(){
				isLoading=true;
				loader.detach().appendTo($container);
				if(option.loadNextPage)
				{
					var data=option.loadNextPage(pageindex);
					if(data && data.length>0){
						showInContainer(data);
						isLoading=false;
					}
				}
				else
				{
					$.ajax({
						type:"POST",
						url:"services/SocialMessages/getImages/"+pageindex
					}).success(function(e){
						if(e.data && e.data.length>0){
							if(e.data.length<20){
								endOfPage=true;
							}
							if(!option.checkScroll){
								if(e.data.length>10){
									e.data=e.data.splice(0,10);
								}
							}
							showInContainer(e.data);
							//totalPage=e.TotalPage;
							isLoading=false;
						}
						else
						{

						}
					}).error(function(e){
						isLoading=false;
					}
					);
				};
			};
			if(option.checkScroll)
			{
				var scrollElm=$(window);
				scrollElm.on("scroll",function(e){
					var currentSlide=app.slider.getCurrentSlide();
					if(currentSlide.route==="social")
					{
						if (($(window).height() + $(window).scrollTop()) >= $(document.body).outerHeight()) {
					        if(!isLoading)
					        {
					          	pageindex++;
					          	if(!endOfPage)
					          	{
					          		loadNextPage();
					          	}
					        }
					    }
					}
				});
			}
			loadNextPage();
	}
}

 