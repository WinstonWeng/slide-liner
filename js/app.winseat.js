app.winseat={
	root:null,
	step2Send:false,
	step3Send:false,
	startUpload:function (e) {
		if(window.FormData){
			$("input[name='data[Participant][uploadType]']").val("ajax");
		}
		else
		{
			$("input[name='data[Participant][uploadType]']").val("iframe");
		}
		var form1=$("#winTicketAccordion .form-1").serializeArray();
		var form2=$("#winTicketAccordion .form-2").serializeArray();
		var form3=$("#winTicketAccordion .form-3").serializeArray();

		var allData=[],allObject={};
		$.merge(allData,form1);
		$.merge(allData,form2);
		$.merge(allData,form3);
		for(var i=0;i<allData.length;i++){
			allObject[allData[i].name]=allData[i].value;
		}
		var files=$("#winTicketAccordion input[type='file']");
		var form=$("<form>");
		for(var f in allObject){
			var input=$("<input>").attr({type:'text',name:f}).val(allObject[f]);
			input.appendTo(form);
		}
		if(allObject['data[Participant][send_txt]']===undefined || allObject['data[Participant][send_txt]'] == "0")
		{
			$(files).each(function(i,f){
				var f=$(f);
				var aCopy=f.clone();
				allObject[f.attr("name")] = {value:f,type:"file",parentCon:f.parent(),fcopy:aCopy};
				f.detach().appendTo(form);
			});
		}
		var loader=$($(".template-loader").val());
		var sender=$(e.target);
		sender.hide().parent().append(loader);
		form.attr({action:"<%=uploadUrl%>"});
		$(form).ssformUpload({
			iframe:$("iframe[name='upload-iframe']"),
			events:{
				uploadReady:function(e){
					 loader.remove();
					 for(var f in allObject){
					 	if(allObject[f].type==="file"){
					 		 try
					 		 {
					 		 	allObject[f].value.appendTo(allObject[f].parentCon);
					 		 }
					 		 catch(error){
								allObject[f].fcopy.appendTo(allObject[f].parentCon);
					 		 }
					 	}
					 }
					 sender.show();
					 var resetForm=function(){
			 			$("#winTicketAccordion .form-1")[0].reset();
			 			$("#winTicketAccordion .form-2")[0].reset();
			 			$("#winTicketAccordion .form-3")[0].reset();
			 			//$('a[href="#winSeatStepOne"]').click();
			 			$('a[href="#winSeatStepTwo"]').attr({disabled:"disabled"});
			 			$('a[href="#winSeatStepThree"]').attr({disabled:"disabled"});
			 			app.winseat.step2Send=false;
					 	app.winseat.step3Send=false;
					 	
					 };
					 var onHidden=function()
					 {
					 	resetForm();
					 }
					 if(e.status==="success"){
					 	var message=$(".template-entry-success").val();
					 	var onShown=function(modalUI){
					 		window.sendVersaTag("http://www.samsung.com.au/samsungslideliner/enternow/thankyou");
					 		app.ui.versaTagStart(modalUI);
					 		modalUI.find(".modal-body").find(".enter-new-receipt").click(function(e){
					 			modalUI.modal("hide");
					 			resetForm();
					 			$('a[href="#winSeatStepOne"]').click();
					 			$('.content.winseat a[data-versa-once="true"]').each(function(i,item){
					 				item=$(item);
					 				var versatagurl=item.attr("_xxxdata-versa");
					 				item.attr({'data-versa':versatagurl,'_xxxdata-versa':null});
					 			});
					 			window.sendVersaTag("http://www.samsung.com.au/samsungslideliner/enternow/step1"); 

					 		});
					 	}
					 	app.ui.modal({message:message,modalSize:"modal-md",title:"&nbsp",className:"black-no-footer",onShown:onShown,onHidden:onHidden});
					 }
					 else
					 {
					 	app.ui.modal({message:e.message,modalSize:"modal-md",title:"&nbsp",className:"black-no-footer with-border error",onHidden:onHidden});
					 }
				},
				uploadProgress:function(percent){
					loader.addClass("with-percent");
					loader.find(".percent .inner").css({width:percent+"%"}).html(percent+"%");
				},
				uploadProcessCompleted:function(e){console.log("uploadProcessCompleted:"+e)},
				uploadError:function(e){loader.remove();sender.show();}
			}
		});

	},
	start:function(){
		app.ui.versaTagStart($(".content.winseat"));
		$(".content.winseat #winTicketAccordion").on("show.bs.collapse",function(e){
			e.stopPropagation();
			var id=($(e.target).attr("id"));
			var heading=$("a[href='#"+id+"']");
			var disabled=heading.attr("disabled");
			if(disabled){
				e.preventDefault();
				return;
			}
			$(".panel-title.active").removeClass("active");
			$("a[href='#"+id+"']").parent().addClass("active");
			 
		});
		/*
		$('.content.winseat .nav.nav-tabs a[data-toggle="tab"]').on("shown.bs.tab",function(e){
			$("input[name='data[Participant][send_txt]']")[0].checked=parseInt(($(e.target).parent().attr("value")));
		});
		*/
		app.control.datetimeSelector($(".content.winseat .date-picker"));
		app.control.uploadWrapper();
		app.validation.start();
		$("a[ss-data-url]").click(function(e){
			e.preventDefault();
			app.ui.modal({dataUrl:$(e.target).attr("ss-data-url"),modalSize:"modal-md",title:"&nbsp",className:"black-no-footer with-border winseat-tvs"})
		});
		$("form .action-row .button").click(function(e){
			var form=$(e.target).attr("form");
			var next=$(e.target).attr("next");
			e.stopPropagation();
			e.preventDefault();
			if(form){
				var aForm=$(".steps ."+form);
				if(aForm.valid()){
					switch(next){
						case "step-two":
							if(!app.winseat.step2Send)
							{
								app.winseat.step2Send=true;
								window.sendVersaTag("http://www.samsung.com.au/samsungslideliner/enternow/step2/"); 
							}
						 break;
						case "step-three":
							if(!app.winseat.step3Send)
							{
								app.winseat.step3Send=true; 
								window.sendVersaTag("http://www.samsung.com.au/samsungslideliner/enternow/step3"); 
							}
						 break;
					}
					$("."+next+" .panel-title a").removeAttr("disabled").click();
					if(aForm.hasClass("form-3")){
						//window.sendVersaTag("http://www.samsung.com.au/samsungslideliner/enternow/step3/button"); 
						app.winseat.startUpload(e);
					}
				}
			}
		});	
		$(".content.winseat a[href='#winSeatStepOne']").click();
		window.sendVersaTag("http://www.samsung.com.au/samsungslideliner/enternow/step1");
		var isShow=false;
		var template=$(".template-where-is-serial").val();
		$('.content.winseat .where-is-serial').popover({selector:document.body,placement:"top",html:true, trigger:"hover",template:template,delay: { show: 500, hide: 100 }}).on("shown.bs.popover",function(e){isShow=true;}).on("hidden.bs.popover",function(e){isShow=false;});
		//var toucher=new app.touchUtil($(".content.winseat"),{moveDistance:20,onStartOnly:true});
		/*
		$(".content.winseat").on("ontouchstart",function(e){
				if(isShow){
					$('.content.winseat .where-is-serial').popover("hide")
				}
		});
		*/
	}
}