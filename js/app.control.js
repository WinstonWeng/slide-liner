if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement, fromIndex) {
      if ( this === undefined || this === null ) {
        throw new TypeError( '"this" is null or not defined' );
      }

      var length = this.length >>> 0; // Hack to convert object.length to a UInt32

      fromIndex = +fromIndex || 0;

      if (Math.abs(fromIndex) === Infinity) {
        fromIndex = 0;
      }

      if (fromIndex < 0) {
        fromIndex += length;
        if (fromIndex < 0) {
          fromIndex = 0;
        }
      }

      for (;fromIndex < length; fromIndex++) {
        if (this[fromIndex] === searchElement) {
          return fromIndex;
        }
      }

      return -1;
    };
};
Number.prototype.padLeft = function (n,str) {
    return (this < 0 ? '-' : '') + 
            Array(n-String(Math.abs(this)).length+1)
             .join(str||'0') + 
           (Math.abs(this));
};
app.control={
	checkbox:function (argument) {
		$(".checkbox-label input").change(function(e){
			var sender=$(e.target);
			if(sender[0].checked){
				sender.parent().addClass("checked");
			}
			else
			{
				sender.parent().removeClass("checked");	
			}
		});
		$(".checkbox-label input").change();
	},
	uploadWrapper:function(){
		$(".upload-wrapper").each(function(i,item){
			$(item).find("input[type='file']").change(function(e){
				var file=this.value,
					allowed="pdf,jpeg,jpg,png,gif,bmp",
					parts=file.split("."),
					ext=parts[parts.length-1],
					fileName=null;
				if(this.files)
				{
					fileName=this.files[0].name;
				}
				else
				{
					var fileNames=file.split("\\");
					fileName=fileNames[fileNames.length-1];
				}
				if(allowed.split(",").indexOf(ext.toLowerCase())>-1)
				{
					$(item).find(".file-display").val(fileName);	
				}
				else
				{
					this.val("");
					$(item).find(".file-display").val("");
				}
				
			});
		});
	},
	start:function(){
		//app.control.checkbox();
		app.control.uploadWrapper();
	}
};

app.formUpload ={
	onMessage: null,
	 uploader: function(options) {
		var me=this;
		var ui=options.events;
		this.upload=function(fileObject){
			

			var formData=new FormData();
			for(var f in fileObject){
				var fd=fileObject[f];
				formData.append(f,fd.value);
			}
			 var xhr = new XMLHttpRequest();
			 xhr.open("POST",options.url);
			 var upload = xhr.upload;
			 xhr.onreadystatechange=function()
			 {
				  if (xhr.readyState==4 && xhr.status==200)
				  {
				    if (ui.uploadReady) {
		                ui.uploadReady($.parseJSON(xhr.responseText));
		            }
				  }
				  else
				  {
				  	if (xhr.readyState==4 && ui.uploadReady) {
		                ui.uploadReady({status:'failed',message:'http error'});
		            }
				  }
			};
			xhr.onerror =function(e){
				var k=0;
			}
			xhr.onload =function(e){
				var k=0;
			}
			upload.addEventListener("progress", function (ev) {
	            if (ev.lengthComputable) {
	                ui.uploadProgress(parseInt(((ev.loaded / ev.total) * 100)));
	            }
	        }, false);
	        upload.addEventListener("load", function (ev) {
	            ui.uploadProcessCompleted();
	            
	        }, false);
	        upload.addEventListener("error", function (ev) {
	            ui.uploadError();

	        }, false);
	        xhr.send(formData);
		};

	   this.uploadIframe=function(fileObject){
	   		if(window.upload && options.iframe){
	   			var iframeSrc=$(options.iframe).attr("src");	   			
	   			app.formUpload.onMessage=function(e){
	   					var  data=null;
						if(e && e.data){
							data=e.data;
						}
						else if(e.originalEvent){
							data=e.originalEvent.data;
						}
						else if(window.event){
							data=window.event.data;
						}
						if(data){
							var obj=$.parseJSON(data);
							if(obj.action==="register" || obj.action==="ssupload"){
								$(window).unbind("message",app.formUpload.onMessage);
					            for(var f in fileObject){
					            	var fb=fileObject[f];
					            	if(fb.type=="file"){
					            		try
					            		{
					            			fb.value.detach()	
					            		}
					            		catch(error){

					            		}
					            		try
					            		{
					            			fb.value.appendTo(fb.parentCon);
					            		}
					            		catch(error){}
					            	}
					            }
								options.iframe.attr({src:iframeSrc});
								if (ui.uploadReady) {
					                ui.uploadReady(obj.result);
					            }
					            
							}
						};
	   			};
	   			$(window).bind("message",app.formUpload.onMessage);	
	   			window.upload(options.url,fileObject);
	   		}
	   		else{
	   			if(ui.uploadReady){
	   				ui.uploadReady({status:'failed',message:'upload not supported'});
	   			}
	   		}
	   };


		this.readData = function(){
			var form,datas,files,uploadObject,hasFormData;
				 
				form 		 = options.form;
				datas		 = form.serializeArray();
				files   	 = form.find("input[type='file']");
				hasFormData	 = ("FormData" in window);
				uploadObject = {};
			for(var i=0;i<datas.length;i++){
				uploadObject[datas[i].name]={type:"text",value:datas[i].value};
			}
	 		for(var i=0;i<files.length;i++){
	 			if(hasFormData)
	 			{
	 				var f=$(files[i]);
	 				uploadObject[f.attr("name")]={type:"file",value:f[0].files[0]};	
	 			}
	 			else
	 			{
	 				uploadObject[$(files[i]).attr("name")]={type:"file",value:$(files[i]),parentCon:$(files[i]).parent()};		
	 				$(files[i]).detach();
	 			}
	 		}
	 		if(hasFormData)
	 		{
	 			me.upload(uploadObject);
	 		}
	 		else
	 		{
	 			me.uploadIframe(uploadObject);	
	 		}
		}
		me.readData();
	}
};

app.control.datetimeSelector=function(elm){
		var 
			selMonth = elm.find(".select-month"),
			selDay   = elm.find(".select-day");
		var allowedDay={
			"07":[14,31],
			"08":[1,31],
			"09":[1,30]
		};
		selMonth.change(function(e){
			var days   = allowedDay[$(e.target).val()],
			    start  = days[0],
			    end    = days[1],
			    dayVal = selDay.val();
			selDay.children().remove();
			var selectDayOpt=$("<option>").attr({value:""}).text("--");
			var existingValue=false;
			selectDayOpt.appendTo(selDay);
			for(var i=start;i<=end;i++){
				var val=i.padLeft(2,"0");

				var opt=$("<option>").attr({value:val}).text(val);
				if(dayVal == val){
					opt.attr({selected:"selected"});
				}
				opt.appendTo(selDay);
			}

		});
};

 

$.fn.ssformUpload=function(options){
	 options.form=this;
	 options.url=this.attr("action");
	 var upload=new app.formUpload.uploader(options);
};
