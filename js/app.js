
var app={};
<%=winConsole%>
app.facebook_share_url = '<%=facebookShareUrl%>';
app.twitter_share_url = '<%=twitterShareUrl%>';
var Modernizr=(typeof(Modernizr)==="undefined" ? {touch:("ontouchstart" in window)} : Modernizr);
<%= insert("../js/app.control.js") %>
<%= insert("../js/app.touch.js") %>
<%= insert("../js/app.sliding.js") %>
<%= insert("../js/app.validation.js") %>
<%= insert("../js/app.gallery.js") %>
<%= insert("../js/app.gallery-new.js") %>
<%= insert("../js/app.winseat.js") %>
<%= insert("../js/app.control.spin3d.js") %>
<%= insert("../js/app.slideliner.js") %>
<%= insert("../js/app.social.js") %>
 $(document).ready(function(e){
	app.slider=new app.slidingNav();
	app.slider.start();
	//app.toucher=new app.touchUtil($(".main-content"));
	setTimeout(function() {
		 
		if(!isOkToLive)
		{
			$(".holding-page").addClass("in");
			var onResize=function(){
				var height=$(document.body).height();
				$(".holding-page").css({height:height,lineHeight:height+"px"});	
			}
			
			$(window).bind("resize",function(e){
				onResize();
			});
			onResize();
		}
		else
		{
			$(".holding-page").remove();	
		}
	},1000);
});
app.ui={
	versaTagStart:function(root){
		root.find("[data-omniture]").each(
			function(i,item){
				$(item).click(function(e){
					var type="o";
					var currentSlide=app.slider.getCurrentSlide();
					var buttonName=$(item).attr("data-omniture");
					type=$(item).attr("omniture-event-type");
					if(type==null || type=="undefined"){
						type="o";
					}
					var prop5 = "au:campaign:seau:<%=omitureName%>:"+buttonName;
			  	 	var hier1  = "au>campaign>seau><%=omitureName%>>"+buttonName;
			  	 	s.pageName="au:campaign:seau:<%=omitureName%>:"+currentSlide.route;
			  	 	s.prop5 = prop5;
			  	 	s.hier1=hier1;
			  	 	s.linkTrackVars='eVar33,events';
					s.linkTrackEvents='event45';
					s.eVar33="au:campaign:seau:<%=omitureName%>:"+buttonName;
					s.events='event45';
					console.log("omniture:"+type+" "+buttonName);
					s.tl(this,type,s.eVar33);
				});
			}
		);
		root.find("[data-versa]").each(
			function(i,item){
				$(item).click(function(e){
					var tag=$(item).attr("data-versa");
					var disabled=$(item).attr("disabled");
					var fireOnce=$(item).attr("data-versa-once");
					if(disabled){
						return;
					}
					if(tag)
					{
						if(fireOnce){
							$(item).attr({"data-versa":""});
							$(item).attr({"_xxxdata-versa":tag});
						}
						window.sendVersaTag(tag); 
					}
				});
			}
		);
	},
	modal:function(messageObj){
		var div='<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'
					+'<div class="modal-dialog">'
						+'<div class="modal-content">'
						+'</div>'
					+'</div>'
				+'</div>';
		var modalUI=$(div);
		if(messageObj.className){
			modalUI.addClass(messageObj.className);
		}
		if(messageObj.title){
			var header=$('<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button><h4 class="modal-title">'+messageObj.title+'</h4></div>');
			header.appendTo(modalUI.find(".modal-content"));
		}
		var body=$("<div>").addClass("modal-body");
		body.appendTo(modalUI.find(".modal-content"));	
		if(messageObj.message){
			body.html(messageObj.message);
		}
		if(messageObj.modalSize){
			modalUI.find(".modal-dialog").addClass(messageObj.modalSize);
		}
		if(messageObj.dataUrl){
			var loader=$("<div>").addClass("loader");
			var mdiv=$("<div>").load(messageObj.dataUrl,function(e){
				loader.remove();
				modalUI.find(".modal-body").append(mdiv);
			});
		}
		var footer=$('<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>');
		footer.appendTo(modalUI.find(".modal-content"));
		modalUI.appendTo($(document.body));
		modalUI.on("hidden.bs.modal",function (e) {
			//$("html,body").css({overflow:""});
			modalUI.remove();
			if(messageObj.onHidden){
				messageObj.onHidden();
			}
		});
		if(messageObj.alertType){
			modalUI.addClass(messageObj.alertType);
		};
		modalUI.on("show.bs.modal",function (e) {
			 //$("html,body").css({overflow:"hidden"});
		});
		modalUI.on("shown.bs.modal",function (e) {
			 if(messageObj.onShown){
			 	messageObj.onShown(modalUI);
			 }
		});
		modalUI.modal();

	}
};
$(document).ready(function(e){
	var onResizing=function(e){
		var currentSlide=app.slider.getCurrentSlide();
		if(currentSlide){
			var height=currentSlide.ui.div.find(".content-wrapper").outerHeight(true);
			//height=height+parseInt(currentSlide.ui.div.find(".content").css("paddingTop").replace("px"));
			//height=height+parseInt(currentSlide.ui.div.find(".content").css("paddingBottom").replace("px"));
			var winHeight=($(window).height())-$(".footer").outerHeight(true)-$(".global_header").outerHeight(true);
			if(height<winHeight){
				height=winHeight;
			}
			if($(".navigation-small").css("display").toLowerCase()=="block"){
				height=height+$(".navigation-small").outerHeight(true);
				height=height+currentSlide.ui.div.find(".slide-linder-footer").outerHeight(true);
			}
			var content=currentSlide.ui.div.find(".content");
			height=height+(content.outerHeight(true)-content.height());
			//height=height-$(".global_header").height();
			 
			$(".top-container").css({height:height});
			window.parent.postMessage(JSON.stringify({t:{type:"height",height:height}}),"*");	
		}
		//console.log(height+"	"+$(".top-container").height());
		
	}
	$(window).bind("resize",onResizing);
 	setInterval(onResizing,500);
 	//onResizing();
});