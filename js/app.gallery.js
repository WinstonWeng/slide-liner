app.gallery={
	videoPlayer:null,
	watchInterval:null,
	videoGetPlayStateInterval:null,
	viewDetail:function(gallery){
		if(gallery.type=="video"){
			gallery.items=app.gallerydata.video;
		}
		var message=$(".template-gallery-view").val();
		app.ui.modal({
		className:"gallery-player gallery-"+gallery.id,
		message:message,
		modalSize:'modal-lg',
		title:" ",
		onHidden:function(){
			$("html,body").css({overflow:""});
			if(app.gallery.videoPlayer)
			{
				app.gallery.videoPlayer.destroy();
			}
			app.gallery.videoPlayer=null;
		}, 
		onShown:function(ui){
			$("html,body").css({overflow:"hidden"});
			if(gallery.title){
				ui.find(".ablum-title-container").html(gallery.title).show();
			}
			if(gallery.location){
				ui.find(".ablum-location-container").html(gallery.location).show();	
			};
			ui.addClass(gallery.type);
			var 
				thumCarousel,
				mainCarousel,
				showItemInfo=function(item){
					if(item.title){
						ui.find(".main-info .title-container").html(item.title).show();
					}
					if(item.description && item.description!=="null"){
						ui.find(".main-info .description-container").html(item.description).show();	
					}
					$(".gallery-view .thumbnails .owl-item.active").removeClass("active");
					item.ui.div.parent().addClass("active");
					if(mainCarousel)
					{
						thumCarousel.goTo(mainCarousel.currentItem);
					}
					ui.find(".play-item-index").html((gallery.type == "video" ? "video" : "photo") +" "+(item.index+1)+" of "+gallery.items.length);
				},
				onItemClick=function(item){
					if(item.type=="video")
					{
						app.gallery.playerVideo(item);
						showItemInfo(item);
						if(item.title){
							ui.find(".ablum-title-container").html(item.title).show();
						}
						if(item.location){
							ui.find(".ablum-location-container").html(item.location).show();	
						};
					}
					else
					{
						mainCarousel.goTo(item.index);
					}
				};
			var thumbSetting={
				   items : 5, //10 items above 1000px browser width
			       itemsDesktop : [1000,4], //5 items between 1000px and 901px
			       itemsDesktopSmall : [900,4], // betweem 900px and 601px
			       itemsTablet: [600,3], //2 items between 600 and 0
			       itemsMobile : [400,2],
				   navigation : true,
				   pagination:false,
				   navigationText:['&nbsp;','&nbsp;']
			};
			thumCarousel=app.gallery.loadInGallery(gallery.items,".gallery-view .thumbnails .carousel",onItemClick,thumbSetting);
			mainCarousel=null;
			var twittershareurl,facebookshareurl;
			if(gallery.type==="album")
			{
				twittershareurl  = "window.open(app.twitter_share_url,'','width=500,height=580')";
				facebookshareurl = "window.open(app.facebook_share_url,'','width=500,height=580')";
				mainCarousel=app.gallery.loadMainGallery(gallery.items,".gallery-view .main-container .carousel");
			}
			else
			{
				var faceShare="https://www.facebook.com/sharer.php?app_id=257838867740570&u=http://youtu.be/"+gallery.videoUrl+"&display=popup";
				var twittersharer ="<%=twitterShareUrl%>";
				facebookshareurl   = "window.open('"+faceShare+"','','width=500,height=580')";
				twittershareVideo = twittersharer.split("&");
				twittershareVideo[twittershareVideo.length-1]="url=http://youtu.be/"+gallery.videoUrl;
				twittershareurl  = "window.open('"+twittershareVideo.join('&')+"','','width=500,height=580')";
				app.gallery.playerVideo(gallery);
				showItemInfo(gallery);
			}
			ui.find(".share-icon.facebook a").attr({onclick:facebookshareurl});
			ui.find(".share-icon.twitter a").attr({onclick:twittershareurl});
			clearInterval(app.gallery.watchInterval);
			var currentItem;
			if(mainCarousel)
			{
				app.gallery.watchInterval=setInterval(function(){
					if(mainCarousel)
					{
						if(currentItem!=mainCarousel.currentItem)
						{
							currentItem=mainCarousel.currentItem;
							showItemInfo(gallery.items[mainCarousel.currentItem]);
						}
					}
				},200);
			}
			 
		}});
	},
	playerVideo:function(sender,ui){
		if(app.gallery.videoPlayer==null){
			if(ui){
				ui=ui[0];
			}
			else
			{
				ui=$(".gallery-view .main-container .carousel")[0];
			}
			var playerState = null;
			var onStateChange = function(e){
				console.log('State is:', e.data);
			}
			app.gallery.videoPlayer = new YT.Player(ui, {
		          height: '100%',
		          width: '100%',
		          videoId: sender.videoUrl,
		          playerVars: { 'autoplay': 0 },
		          events: {
		           	onReady: function(e){
		          	},
		          	onStateChange:function(e){
		          		if(e.data == YT.PlayerState.ENDED){
		          			if(sender.playlist){
		          				var next = sender.getNext(sender.videoUrl);
		          				if(next){
		          					sender.videoUrl=next;
		          					app.gallery.playerVideo(sender,ui);
		          				}
		          			}
		          		}
		          	}
		          }
	        });
		}
		else{
			app.gallery.videoPlayer.loadVideoById(sender.videoUrl);
		}
	},
	loadMainGallery:function(items,selector){
		var mainViewSetting={
		    navigation : true, // Show next and prev buttons
		    slideSpeed : 300,
		    lazyLoad : true,
		    paginationSpeed : 400,
		    singleItem:true,
		    navigationText:false,
		    pagination:false
		};
		var owlContainer=$(selector);
		$(items).each(function(i,item){
			var mdiv=$("<div>").addClass("item");
			var mimg,responsiveDiv,responsiveDivInner;
			item.index=i;
			if(item.type && item.type=="video"){
				/*
				var template=$($(".template.gallery-video-template").val());
				responsiveDiv=template;
				responsiveDivInner=responsiveDiv.find(".inner");
				mimg=responsiveDiv.find("img");
				mimg.addClass("lazyOwl").attr({'data-src':item.url});
				var btnPlay=template.find(".play-button");
				btnPlay.attr({videoid:item.id});
				item.ui=template.find(".video-play");
				btnPlay.click(function(e){
					galleryPlayVideo(item);
				});*/
			}
			else
			{
 				responsiveDiv=$("<div>").addClass("image-container");
				responsiveDivInner=$("<div>").addClass("inner");
				responsiveDiv.append(responsiveDivInner);
				mimg=$("<img>").addClass("lazyOwl").attr({'data-src':item.url});
				mimg.appendTo(responsiveDivInner);
				responsiveDiv.appendTo(mdiv);

			}
			mdiv.appendTo(owlContainer);
		});
		return owlContainer.owlCarousel(mainViewSetting).data('owlCarousel');	
	},

	loadInGallery:function(items,selector,onItemClick,thumbSettingCUST){
		var thumbSetting={
			   items : 4, //10 items above 1000px browser width
		       itemsDesktop : [1200,3], //5 items between 1000px and 901px
		       itemsDesktopSmall : [950,3], // betweem 900px and 601px
		       itemsTablet: [600,3], //2 items between 600 and 0
		       itemsMobile : [400,2],
			   navigation : true,
			   pagination:false,
			   navigationText:['&nbsp;','&nbsp;']
		};
		if(thumbSettingCUST){
			thumbSetting=thumbSettingCUST;
		}
		var owlContainer=$(selector);
		var templates=$(".template-gallery-thumb").val();
		$(items).each(function(i,item){
			var div=$(templates);
			div.addClass(item.type);
			div.attr({galleryitemid:item.id});
			var imgSrc=item.thumburl;
			if(item.type=="album"){
				imgSrc=item.cover;
			}
			item.index=i;
			var img=div.find("img").addClass("lazyOwl").load(function(e){
			}).attr({'src':imgSrc});
			div.appendTo(owlContainer);
			for(var f in item){
				var ui=div.find("."+f+"-container");
				if(ui.length>0){
					if(item[f] && item[f]!=="null")
					{
						ui.attr({title:item[f]});
						ui.html(item[f]);
					}
				}
			}
			div.find("a").attr({href:"javascript:void(0)"}).click(function(e){
				if(onItemClick){
					onItemClick(item);
				}
				else
				{
					app.gallery.viewDetail(item);
				}
			});
			item.ui={div:div};
			if(item.type=="album"){
				div.find(".counter").html(item.items.length);
			}
		});
		return owlContainer.owlCarousel(thumbSetting).data('owlCarousel');

	},
	installYouTube:function(){
		 /*if(typeof(YT)==="undefined"){
			 
			  window.onYouTubeIframeAPIReady = function(){
			  	console.log("onYouTubeIframeAPIReady");
			  };
			  var tag = document.createElement('script');
		      tag.src = "https//www.youtube.com/iframe_api";
		      var firstScriptTag = document.getElementsByTagName('script')[0];
		      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

		}*/
	},
	start:function (){
		var photos=app.gallerydata.photo;
		var photoObj=this.loadInGallery(photos,".content.gallery .photo-gallery .carousel");
		var videos=app.gallerydata.video;
		var videoObject=this.loadInGallery(videos,".content.gallery .video-gallery .carousel");
		this.installYouTube();
	}
}