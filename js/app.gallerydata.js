app.gallerydata={
		photo:[
			{
				id:0,
				title:"Bledisloe Cup - Qantas Wallabies v All Blacks",
				location:"ANZ Stadium, Sydney 16/08/14",
				description:"null",
				type:"album",
				cover:"assets/gallery/photos/slideliner-anz/cover.jpg",
				items:[
					{
						title:"_BHP3927",
						itemID:"9",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP3927.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP3927.jpg"
					},

					{
						title:"_BHP3985",
						itemID:"10",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP3985.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP3985.jpg"
					},
					{
						title:"_BHP4001",
						itemID:"11",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP4001.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP4001.jpg"
					},

					{
						title:"_BHP4024",
						itemID:"12",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP4024.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP4024.jpg"
					},


					{
						title:"_BHP2295",
						itemID:"0",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP2295.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP2295.jpg"
					},

					{
						title:"_BHP2313",
						itemID:"1",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP2313.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP2313.jpg"
					},


					{
						title:"_BHP4048",
						itemID:"13",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP4048.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP4048.jpg"
					},

					{
						title:"_BHP4064",
						itemID:"14",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP4064.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP4064.jpg"
					},
					{
						title:"_BHP4100",
						itemID:"16",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP4100.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP4100.jpg"
					},
					{
						title:"_BHP2322",
						itemID:"2",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP2322.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP2322.jpg"
					},


					{
						title:"_BHP4073",
						itemID:"15",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP4073.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP4073.jpg"
					},


					{
						title:"_BHP4297",
						itemID:"17",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP4297.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP4297.jpg"
					},

					{
						title:"_BHP4313",
						itemID:"18",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP4313.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP4313.jpg"
					},

					{
						title:"_BHP4323",
						itemID:"19",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP4323.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP4323.jpg"
					},



					{
						title:"_BHP2475",
						itemID:"3",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP2475.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP2475.jpg"
					},

					{
						title:"_BHP2478",
						itemID:"4",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP2478.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP2478.jpg"
					},

					{
						title:"_BHP2495",
						itemID:"5",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP2495.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP2495.jpg"
					},

					{
						title:"_BHP2500",
						itemID:"6",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP2500.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP2500.jpg"
					},

					{
						title:"_BHP2535",
						itemID:"7",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP2535.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP2535.jpg"
					},

					{
						title:"_BHP2559",
						itemID:"8",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP2559.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP2559.jpg"
					},
					{
						title:"_BHP4443",
						itemID:"20",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP4443.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP4443.jpg"
					},

					{
						title:"_BHP4455",
						itemID:"21",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP4455.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP4455.jpg"
					},

					{
						title:"_BHP4487",
						itemID:"22",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP4487.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP4487.jpg"
					},

					{
						title:"_BHP4517",
						itemID:"23",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP4517.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP4517.jpg"
					},

					{
						title:"_BHP4568",
						itemID:"24",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-anz/images/_BHP4568.jpg",
						thumburl:"assets/gallery/photos/slideliner-anz/thumb/_BHP4568.jpg"
					}
				]
			},
			{
				id:1,
				title:"Wallabies VS Springboks",
				description:"null",
				type:"photo",
				location:"Patersons Stadium, Perth 06/09/14",
				cover:"assets/gallery/photos/slideliner-patersons/cover.jpg",
				items:[
					{
						title:"AA5E1295",
						itemID:"0",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/AA5E1295.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/AA5E1295.jpg"
					},

					{
						title:"AA5E1353",
						itemID:"1",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/AA5E1353.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/AA5E1353.jpg"
					},

					{
						title:"AA5E1389",
						itemID:"2",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/AA5E1389.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/AA5E1389.jpg"
					},

					{
						title:"AA5E1402",
						itemID:"3",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/AA5E1402.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/AA5E1402.jpg"
					},

					{
						title:"AA5E1414",
						itemID:"4",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/AA5E1414.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/AA5E1414.jpg"
					},

					{
						title:"AA5E1463",
						itemID:"5",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/AA5E1463.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/AA5E1463.jpg"
					},

					{
						title:"AA5E1575",
						itemID:"6",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/AA5E1575.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/AA5E1575.jpg"
					},

					{
						title:"IZ6C6618",
						itemID:"7",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/IZ6C6618.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/IZ6C6618.jpg"
					},

					{
						title:"IZ6C6745",
						itemID:"8",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/IZ6C6745.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/IZ6C6745.jpg"
					},

					{
						title:"IZ6C6796",
						itemID:"9",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/IZ6C6796.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/IZ6C6796.jpg"
					},

					{
						title:"IZ6C6808",
						itemID:"10",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/IZ6C6808.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/IZ6C6808.jpg"
					},

					{
						title:"IZ6C6817",
						itemID:"11",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/IZ6C6817.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/IZ6C6817.jpg"
					},

					{
						title:"IZ6C6828",
						itemID:"12",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/IZ6C6828.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/IZ6C6828.jpg"
					},

					{
						title:"IZ6C6855",
						itemID:"13",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/IZ6C6855.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/IZ6C6855.jpg"
					},

					{
						title:"IZ6C6861",
						itemID:"14",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/IZ6C6861.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/IZ6C6861.jpg"
					},

					{
						title:"IZ6C6911",
						itemID:"15",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/IZ6C6911.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/IZ6C6911.jpg"
					},

					{
						title:"IZ6C6940",
						itemID:"16",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/IZ6C6940.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/IZ6C6940.jpg"
					},

					{
						title:"IZ6C6961",
						itemID:"17",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/IZ6C6961.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/IZ6C6961.jpg"
					},

					{
						title:"IZ6C7067",
						itemID:"18",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/IZ6C7067.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/IZ6C7067.jpg"
					},

					{
						title:"IZ6C7334",
						itemID:"19",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/IZ6C7334.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/IZ6C7334.jpg"
					},

					{
						title:"IZ6C7342",
						itemID:"20",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/IZ6C7342.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/IZ6C7342.jpg"
					},

					{
						title:"IZ6C7419",
						itemID:"21",
						description:"null",
						credits:"null",
						url:"assets/gallery/photos/slideliner-patersons/images/IZ6C7419.jpg",
						thumburl:"assets/gallery/photos/slideliner-patersons/thumb/IZ6C7419.jpg"
					}
				]
			}
		],	 
		video:[
			{
				id:406,
				title:"Samsung SlideLiner - Slide with the action",
				description:null,
				credits:null,
				url:"assets/gallery/video/slideliner/LfoR32cp-90.jpg",
				videoUrl:"LfoR32cp-90",
				thumburl:"assets/gallery/video/slideliner/LfoR32cp-90.jpg",
				cover:"assets/gallery/video/slideliner/LfoR32cp-90.jpg",
				location:"ANZ Stadium, Sydney 16.08.14",
				type:"video"
			},
			{
				id:405,
				title:"Samsung SlideLiner Consumer Activations",
				description:null,
				credits:null,
				url:"assets/gallery/video/slideliner/EZc55-y1Usw.jpg",
				videoUrl:"EZc55-y1Usw",
				thumburl:"assets/gallery/video/slideliner/EZc55-y1Usw.jpg",
				cover:"assets/gallery/video/slideliner/EZc55-y1Usw.jpg",
				location:"First Fleet Park, Circular Quay, Sydney 24.08.14",
				type:"video"
			},
			{
				id:404,
				title:"Samsung SlideLiner Consumer Activation Reel 5",
				description:null,
				credits:null,
				url:"assets/gallery/video/slideliner/v1ehTh9Yd_I.jpg",
				videoUrl:"v1ehTh9Yd_I",
				thumburl:"assets/gallery/video/slideliner/v1ehTh9Yd_I.jpg",
				cover:"assets/gallery/video/slideliner/v1ehTh9Yd_I.jpg",
				location:"First Fleet Park, Circular Quay, Sydney 24.08.14",
				type:"video"
			},
			{
				id:403,
				title:"Samsung SlideLiner Consumer Activation Reel 4",
				description:null,
				credits:null,
				url:"assets/gallery/video/slideliner/O70rCFYuqs0.jpg",
				videoUrl:"O70rCFYuqs0",
				thumburl:"assets/gallery/video/slideliner/O70rCFYuqs0.jpg",
				cover:"assets/gallery/video/slideliner/O70rCFYuqs0.jpg",
				location:"First Fleet Park, Circular Quay, Sydney 24.08.14",
				type:"video"
			},
			{
				id:402,
				title:"Samsung SlideLiner Consumer Activation Reel 3",
				description:null,
				credits:null,
				url:"assets/gallery/video/slideliner/nY1kdKkZlcY.jpg",
				videoUrl:"nY1kdKkZlcY",
				thumburl:"assets/gallery/video/slideliner/nY1kdKkZlcY.jpg",
				cover:"assets/gallery/video/slideliner/nY1kdKkZlcY.jpg",
				location:"First Fleet Park, Circular Quay, Sydney 24.08.14",
				type:"video"
			},
			{
				id:401,
				title:"Samsung SlideLiner Consumer Activation Reel 2",
				description:null,
				credits:null,
				url:"assets/gallery/video/slideliner/myNgq5Nz5EE.jpg",
				videoUrl:"myNgq5Nz5EE",
				thumburl:"assets/gallery/video/slideliner/myNgq5Nz5EE.jpg",
				cover:"assets/gallery/video/slideliner/myNgq5Nz5EE.jpg",
				location:"First Fleet Park, Circular Quay, Sydney 24.08.14",
				type:"video"
			}, 
			{
				id:400,
				title:"Samsung SlideLiner Consumer Activation Reel 1",
				description: "",
				credits:null,
				url:"assets/gallery/video/slideliner/Npl7IWjKiBk.jpg",
				videoUrl:"Npl7IWjKiBk",
				thumburl:"assets/gallery/video/slideliner/Npl7IWjKiBk.jpg",
				cover:"assets/gallery/video/slideliner/Npl7IWjKiBk.jpg",
				type:"video",
				location:"First Fleet Park, Circular Quay, Sydney 24.08.14",
				share:true
			}
		]
}