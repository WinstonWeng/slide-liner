<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class User extends AppModel {

	function register($data) {
		if(!empty($data['facebookId']) && !empty($data['twitterId'])) {
			return $this->validateBoth($data);
		} elseif (empty($data['facebookId']) || empty($data['twitterId'])) {
			return $this->validateAny($data);
		} else {
			return array('status' => 'success', 'message' => 'Successfully registered participant', 'data' => $this->Participant->validationErrors);
		}

		$this->create();
		if ($this->save($data)) {
			//return $this->flash(__('The user has been saved.'), array('action' => 'index'));

		}
	}

	function validateBoth($data) {

	}

	function validateAny($data) {

	}

}
