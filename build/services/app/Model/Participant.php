<?php
App::uses('AppModel', 'Model');
/**
 * Purchase Model
 *
 */
class Participant extends AppModel {

/**
 * Use database config
 *
 * @var string
 */
	//public $useDbConfig = 'local';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'fname' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'First name cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'lname' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Last name cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'phone' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Phone cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'email' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Email cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'address' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Address cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'suburb' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Suburb cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'postcode' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Postcode cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'state' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'State cannot be blank',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'product' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Purchased product cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'purchaseDate' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Purchase date cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'backlog' => array(
			'checkBacklog' => array(
				'rule' => array('checkBacklog'),
				'message' => 'Serial number cannot be empty',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'serialNo' => array(
			'custom' => array(
				'rule' => array('checkUnique'),
				'message' => 'The serial number is already registerd for this contest',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			)
		),
		'receiptCopy' => array(
			'uploadError' => array(
				'rule' => 'uploadError',
				'message' => 'Something went wrong with the file upload'
			),
			'extension' => array(
		        'rule'    => array('extension', array('gif', 'jpeg', 'png', 'jpg', 'doc', 'docx', 'pdf')),
		        'message' => 'Please upload a valid file. Only gif, png, jpg, doc/docx &amp; pdf are allowed to upload'
			),
			'fileSize' => array(
		        'rule' => array('fileSize', '<=', '10MB'),
		        'message' => 'Image must be less than 10MB'
		    )
	    ),
		'comment' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Comment cannot be blank',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'custom' => array(
				'rule' => array('wordLimit'),
				'message' => 'Comment must be less than 25 words',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			)
		),
		'tnc' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'You must accept the terms &amp; conditions',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	public function beforeSave($options = array()) {
		parent::beforeSave();
	}
	public function checkBacklog() {
		if(empty($this->data['Participant']['backlog']) && empty($this->data['Participant']['serialNo'])) return false;
		else return true;
	}
	public function checkUnique() {
		$count = $this->find('count',
			array(
				'conditions' => array('serialNo' => $this->data['Participant']['serialNo'])
			)
		);

		if(!empty($this->data['Participant']['serialNo']) && $count > 0) return false;
		else return true;
	}
	public function wordLimit($data) {
		return count(explode(' ', trim($data['comment']))) <= 25;
	}

	/*
	 * Upload file
	 */
	public function uploadFile($fileObj, $fileName) {

		if (!empty($_SERVER['SERVER_NAME'])) {
			if($_SERVER['SERVER_NAME'] == 'localhost' || $_SERVER['SERVER_NAME'] == 'samsung.api') {
				$filePath = "./uploads/". $fileName;
			} else {
				$filePath = $_SERVER['DOCUMENT_ROOT'] . '\samsungslideliner\uploads\\'. $fileName;
			}
		}

		try {
			move_uploaded_file($fileObj['tmp_name'], $filePath);
			return true;
		} catch(Exception $e) {
			return false;
		}
	}
}
