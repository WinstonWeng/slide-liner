<?php
App::uses('AppModel', 'Model');
/**
 * Event Model
 *
 */
class Event extends AppModel {

	function hasActiveEvent($type) {

		$eventCount =  $this->find(
					'count',array(
						'recursive' => -1,
						'conditions' => array(
							'isCurrent' => 1,
							'socials LIKE ' => '%'.$type.'%',
							'closing >=' => date('Y-m-d H:i:s')
						)
				));
		return $eventCount;
	}

}
