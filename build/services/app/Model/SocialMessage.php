<?php
App::uses('AppModel', 'Model');
/**
 * SocialMessage Model
 *
 */
class SocialMessage extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'socialType' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'profileId' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),

		/**
		 * FOR UPLOAD
		 */
		'fname' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'First name cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'lname' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Last name cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'phone' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Phone cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'email' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Email cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'postcode' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Postcode cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'state' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'State cannot be blank',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'userImage' => array(
			'extension' => array(
		        'rule'    => array('extension', array('gif', 'jpeg', 'png', 'jpg')),
		        'message' => 'Please upload a valid file. Only gif, png &amp jpg are allowed to upload'
			),
			'fileSize' => array(
		        'rule' => array('fileSize', '<=', '1MB'),
		        'message' => 'Image must be less than 1MB'
		    )
	    ),
		'comment' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'Comment cannot be blank',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			)
		),
		'tnc' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'You must accept the terms &amp; conditions',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),

	);
	public $status		 = array('approved' => '1', 'rejected' => '2');
	public $hashtags	 = array("Slideliner","SlideLiner", "slideliner", "SLIDELINER");
	public $hashtag		= 'slideliner';

	public $socialConfig = array(
		'Facebook'	=> array(
			'app_id'	=> '545930208772539',
			'app_secret'=> '79e84ba7fc6464db4cb9af39cf5bb0a2'
		),
		'Twitter'	=> array(
			'consumer_key'		=> 'EQNi09M55CIGekeKqjpvSnp6J',
			'consumer_secret'	=> 'YJeTSv7oRNBuoPFBdtpkBhgpdPL7wXhDHnhwjeqaC0WCyfuDGG',
			'oauth_token'		=> '1271195101-vUwmnaUlOW3396P2XJjETq98dJOCzyLybASm2g8',
			'oauth_token_secret' => '6DYFT9hbmIpDbAy4zt2EoQmkrDWESdBGytpCqU8fnSdxT',
			'output_format' => 'json'
		),
		'Instagram'	=> array(
			'client_id' => '73e6d765e7204934b964da62050b1b0b' )
		);
	/**
	 * reset everything if uploaded
	 */
	public function beforeSave($options = array()) {
		date_default_timezone_set("Australia/Sydney");
		parent::beforeSave();
		if (!empty($this->data['SocialMessage']['fname']) && !empty($this->data['SocialMessage']['lname'])) {
			$uploadData = $this->data['SocialMessage'];
			$this->data['SocialMessage'] = array(
				'socialType' => 'up',
				'profileName' => $this->data['SocialMessage']['fname'] . '.' . $this->data['SocialMessage']['lname'][0],
				'status'	 => 2,
				'postMessage'=> $this->data['SocialMessage']['comment'],
				'uploadData' => json_encode($uploadData),
				'posted'	 => date('Y-m-d H:i:s'),
				'event'		 => '',
				'created'	 => date('Y-m-d H:i:s'),
			);
		}
		return true;
	}

	public function uploadFile($fileObj, $fileName) {

		if (!empty($_SERVER['SERVER_NAME'])) {
			if($_SERVER['SERVER_NAME'] == 'localhost' || $_SERVER['SERVER_NAME'] == 'samsung.api') {
				$filePath = "./uploads/events/". $fileName;
			} else {
				$filePath = $_SERVER['DOCUMENT_ROOT'] . '\samsungsound\uploads\events\\'. $fileName;
			}
		}

		try {
			$source = imagecreatefromstring(file_get_contents($fileObj['tmp_name']));
			$thumbSource = $this->resizeImage($source);
			imagejpeg($thumbSource, $filePath . '-thumb.jpg', 100);

			$imageSource = $this->resizeImage($source, 'large');
			imagejpeg($imageSource, $filePath . '.jpg', 100);

			return array(
				'thumb' => $fileName . '-thumb.jpg',
				'large' => $fileName . '.jpg',
			);
		} catch(Exception $e) {
			return false;
		}
	}

	/**
	 * --------------------- SOCIAL AGGREGATION METHODS ------------------------------
	 */


	public function getLastPulledData($type = 'fb') {
		$contents = $this->find('first', array(
			'fields'		=> array('posted'),
			'conditions'	=>array(
				'socialType'	=> $type
			),
			'order'			=> array('posted DESC')
		));
		return (!empty($contents['SocialMessage']['posted']) ? $contents['SocialMessage']['posted'] : null);
	}


	/**
	 * =====================================================================
	 * ----------------------------- FACEBOOK -----------------------------
	 * =====================================================================
	 */
	public function importFacebookData() {

		$hasActiveEvent = ClassRegistry::init('Event')->hasActiveEvent('fb');
		$socialData = array();

		if(!empty($hasActiveEvent)) {
			$NextUrl = ClassRegistry::init('NextUrl');
			$nxtData = $NextUrl->find('first');

			$accessToken = '268598636489272|RFJmXUTaKK-PYiRmsZnbN_Sgbm0';
			if (!empty( $accessToken )) {
				$params = array(
					'q' => '#'. $this->hashtag,
					'limit' => 50,
					'access_token' => $accessToken
				);
				if(!empty($nxtData['NextUrl']['facebook'])) {
					$params['until'] = $nxtData['NextUrl']['facebook'];
				}

				$graphUrl = 'https://graph.facebook.com/';
				$data = $this->cURLRequest($graphUrl . 'search/', $params,false);
				$data = json_decode($data);
				if(!empty($data) && empty($data->error)) {
					if(!empty($data->paging->next)) {
						//parse next_result url and get the max_id
						$nextFacebookURL = parse_url($data->paging->next);
						parse_str($nextFacebookURL['query'], $nextFacebookURL);
						if(!empty($nextFacebookURL['until'])) {
							$nxtMaxId = $nextFacebookURL['until'];
							$NextUrl->id = $nxtData['NextUrl']['id'];
							$NextUrl->saveField('facebook', $nxtMaxId);
						}
					}

					$socialData = $this->saveFacebookData($data, array(
						'url' => $graphUrl,
						'params' => array('access_token' => $accessToken)
					));

				}

			}
			return $socialData;
		}
	}

	public function saveFacebookData($messages, $fbParams = null) {
		ini_set('max_execution_time', 300);

		if(!empty($messages)) {

			$socialData = array();
			$dataChecked = $alreadySaved = false;

			foreach($messages->data as $msg) {

				if(!empty($msg->picture) && $msg->type == 'photo' ) {
					if(!empty($msg->caption) && strpos($msg->caption, $this->hashtag) !== false) {
						if($this->checkPostExists('fb', $msg->id) > 0) {
							break;
						}
						$profileImage	= $data = json_decode($this->cURLRequest($fbParams['url']. $msg->from->id .'/picture', $fbParams['params']));
						$profileData	= $data = json_decode($this->cURLRequest($fbParams['url']. $msg->from->id, $fbParams['params']));

						$socialData[] = array(
							'socialType'	=> 'fb',
							'profileId'		=> $msg->from->id,
							'profileName'	=> $msg->from->name,
							'profileImage'	=> !empty($profileImage->data->url) ? $profileImage->data->url : '',
							'profileLink'	=> !empty($profileData->link) ? $profileData->link : 'http://facebook.com/' . $msg->from->id,
							'postId'		=> $msg->id,
							'postImageLarge'=> !empty($msg->picture) ? str_replace(array('_s.','_t.'), '_b.', $msg->picture) : '',
							'postImageSmall'=> $msg->picture,
							'postMessage'	=> !empty($msg->caption) ? $msg->caption : '',
							'postUrl'		=> $msg->link,
							'likes'			=> !empty($msg->likes->data) ?sizeof($msg->likes->data) : 0,
							'shares'		=> !empty($msg->shares->count) ? $msg->shares->count : 0,
							'comments'		=> '',
							'status'		=> 2,
							'posted'		=> date('Y-m-d H:i:s', strtotime($msg->created_time))
						);
					}
				}
			}
			if(!empty($socialData)) {
				$this->saveMany($socialData);
			}
			return $socialData;
		}
	}

	/**
	 * =====================================================================
	 * ----------------------------- TWITTER -----------------------------
	 * =====================================================================
	 */

	 public function importTwitterData() {
	 	$hasActiveEvent = ClassRegistry::init('Event')->hasActiveEvent('tw');
		$socialData = array();
		if(!empty($hasActiveEvent)) {
			$NextUrl = ClassRegistry::init('NextUrl');
			$nxtData = $NextUrl->find('first');

		 	App::import('vendor', 'Twitter/TwitterOAuth');
			$params	= $this->socialConfig['Twitter'];

			$tw = new TwitterOAuth($params);

			$request = array(
				'q' => '#' . $this->hashtag . "+OR+#" . implode('+OR+#', $this->hashtags),
				'include_entities' => true,
            	'lang' => 'en',
            	'count' => 100
			);
			/**
			 * Get next data
			 */
			if(!empty($nxtData['NextUrl']['twitter'])) {
				$params['max_id'] = $nxtData['NextUrl']['twitter'];
				$params['include_entities'] = 1;
			}

			$data = $tw->get('search/tweets', $request);
			//print($data);
			$data = json_decode($data, true);

			$nxtMaxId = '';
			if(!empty($data['search_metadata']['next_results'])) {
				//parse next_result url and get the max_id
				$nextTwitterURL = parse_url($data['search_metadata']['next_results']);
				parse_str($nextTwitterURL['query'], $nextTwitterURL);
				if(!empty($nextTwitterURL['max_id'])) {
					$nxtMaxId = $nextTwitterURL['max_id'];
				}
			}
			$NextUrl->id = $nxtData['NextUrl']['id'];
			$NextUrl->saveField('twitter', $nxtMaxId);

			$socialData = $this->saveTwitterData($data);

			return $socialData;
		}

	 }
	/**
	 * Save Twitter data
	 */
	public function saveTwitterData($data, $params = null) {

		ini_set('max_execution_time', 300);

		if(!empty($data)) {
			$socialData = array();
			$dataChecked = false;

			foreach($data['statuses'] as $msg) {

				if(!empty($msg['text'])) {

					if($this->checkPostExists('tw', $msg['id_str']) == 0) {

						$profileImage	= $msg['user']['profile_image_url'];
						$profileLink	= 'http://twitter.com/' . $msg['user']['screen_name'];

						$socialData[] = array(
							'socialType'	=> 'tw',
							'profileId'		=> $msg['user']['id_str'],
							'profileName'	=> $msg['user']['screen_name'],
							//'location'		=>  $msg['user']['location'], //Australia
							'profileImage'	=> $profileImage,
							'profileLink'	=> $profileLink,
							'postId'		=> $msg['id_str'],
							'postImageLarge'=> (!empty($msg['entities']['media']) ? $msg['entities']['media'][0]['media_url'] : ''),
							'postImageSmall'=> (!empty($msg['entities']['media']) ? $msg['entities']['media'][0]['media_url'] . ':thumb' : ''),//twitter has 4 sizes - thumb, small, large & medium
							'postMessage'	=> !empty($msg['text']) ? $msg['text'] : '',
							'postUrl'		=> (!empty($msg['entities']['media']) ?
									(strpos($msg['entities']['media'][0]['display_url'], 'http') === false ? 'http://' : '') . $msg['entities']['media'][0]['display_url'] : ''),
							'likes'			=> $msg['favorite_count'],
							'shares'		=> $msg['retweet_count'],
							'comments'		=> 0,
							'status'		=> 2,
							'posted'		=> date('Y-m-d H:i:s', strtotime($msg['created_at']))
						);
					}
				}
			}

			if(!empty($socialData)) {
				$this->saveMany($socialData);
			}
			return $socialData;
		}
	}

	/**
	 * =====================================================================
	 * ----------------------------- INSTAGRAM -----------------------------
	 * =====================================================================
	 */
	public function importInstagramData()
	{

		$hasActiveEvent = ClassRegistry::init('Event')->hasActiveEvent('ig');
		$socialData = array();
		//print($hasActiveEvent);
		if(!empty($hasActiveEvent)) {

			$NextUrl = ClassRegistry::init('NextUrl');
			$nxtData = $NextUrl->find('first');
			$hastagCombinations = explode(',', $this->hashtag . ',' . implode(',', $this->hashtags));

			$params = $this->socialConfig['Instagram'];
			$maxTagId = $hashtagMaxId = null;
			if(!empty($nxtData['NextUrl']['instagram'])) {
				$params['max_tag_id'] = $nxtData['NextUrl']['instagram'];
			}
			$instaParams = $params;
			$currentHashtag = '';

			foreach($hastagCombinations as $hashtag) {
				if($currentHashtag != $hashtag) {
					$instaParams = $params;
					$currentHashtag = $hashtag;
				}
				do {
					$nxtMaxId = $instagramData = '';
					$instagramURL = 'https://api.instagram.com/v1/tags/'. $hashtag .'/media/recent';
					$data = $this->cURLRequest($instagramURL, $instaParams, false);
					$data = json_decode($data, true);
					$instagramData = $this->saveInstagramData($data);
					if(!empty($instagramData)) {
						$socialData[] = $instagramData;
					}
					if(!empty($data['pagination']['next_max_tag_id'])){
						$nxtMaxId = $data['pagination']['next_max_tag_id'];
						$instaParams['max_tag_id'] = $nxtMaxId;

						/**
						 * Setting the max next id to $maxTagId if that is lower than the previous one
						 */
						 if($hashtagMaxId < $nxtMaxId) {
							$hashtagMaxId = $nxtMaxId;
						 }
					};
				} while(!empty($nxtMaxId));
			}

			$NextUrl->id = $nxtData['NextUrl']['id'];
			$NextUrl->saveField('instagram', $hashtagMaxId);
		}

		return $socialData;
	}

	public function saveInstagramData($data, $params = null) {
		ini_set('max_execution_time', 300);

		if(!empty($data)) {
			$socialData = array();
			$dataChecked = $alreadySaved = false;
			foreach($data['data'] as $msg) {
				if(!empty($msg['images'])) {
					if(!empty($msg['caption']['text']) && strpos(strtolower($msg['caption']['text']), strtolower($this->hashtag)) !== false) {
						if($this->checkPostExists('ig', $msg['id']) > 0) {
							break;
						}
						$profileImage	= $msg['user']['profile_picture'];
						$profileLink	= 'http://instagram.com/' . $msg['user']['username'];
						$feedtype 		= $msg['type'];
						$videoUrl       = null;
						if($feedtype === "video")
						{
							if(!empty($msg['videos']['low_resolution']['url']))
							{
								$videoUrl=  $msg['videos']['low_resolution']['url'];
							}
						}
						$socialData[] = array(
							'socialType'	=> 'ig',
							'profileId'		=> $msg['user']['id'],
							'profileName'	=> $msg['user']['username'],
							'profileImage'	=> $profileImage,
							'profileLink'	=> $profileLink,
							'postId'		=> $msg['id'],
							'postImageLarge'=> $msg['images']['standard_resolution']['url'],
							'postImageSmall'=> $msg['images']['low_resolution']['url'],
							'postMessage'	=> !empty($msg['caption']['text']) ? $msg['caption']['text'] : '',
							'postUrl'		=> $msg['link'],
							'likes'			=> $msg['likes']['count'],
							'shares'		=> 0,
							'comments'		=> $msg['comments']['count'],
							'status'		=> 2,
							'posted'		=> date('Y-m-d H:i:s', $msg['created_time']),
							'videoUrl'   	=> $videoUrl
						);
					}
				}
			}

			if(!empty($socialData)) {
				$this->saveMany($socialData);
			}
			return $socialData;
		}
	}

	public function checkPostExists($type, $postId) {
		return $this->find('count', array(
			'conditions' => array(
				'socialType'=> $type,
				'postId'	=> $postId
			)
		));

	}

}
