<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

	public $imageSizes = array(
		'max-width' => '750',
		'min-width' => '750',
		'thumb-max-width' => '200',
		'thumb-min-width' => '200'
	);

	public function cURLRequest($url, $params = array(), $isPost = false, $postJSON = false) {
		try {
			if (empty($isPost) && !empty($params)) {
				$url .= '?' . http_build_query($params);
			}
			$curl = curl_init($url);

			if (!empty($isPost)) {
				curl_setopt($curl, CURLOPT_POST, true);
				if (!empty($params))
					curl_setopt($curl, CURLOPT_POSTFIELDS, array_merge($params));
			}
			if (!empty($postJSON)) {
				curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
			}
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

			if (!($result = curl_exec($curl))) {
				//echo curl_error($curl);
			} else {
				$result = $result;
			}
			curl_close($curl);

		} catch (exception $e) {
			return false;
		}

		return $result;
	}

	/**
	 * Processing Multi cURL to fetch images in batches
	 */
	public function cURLMultiRequests($links = array()) {//id, url
		try {
			if (!empty($links)) {
				$cmh = curl_multi_init();
				$curls = $results = array();

				foreach ($links as $link) {
					$id = $link['Link']['id'];
					$url = $link['Link']['source_url'];
					$curls[$id] = curl_init($url) or die('cannot curl init');

					curl_setopt($curls[$id], CURLOPT_URL, $url);
					curl_setopt($curls[$id], CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curls[$id], CURLOPT_SSL_VERIFYPEER, false);

					curl_multi_add_handle($cmh, $curls[$id]);
				}

				do {
					curl_multi_select($cmh);
					// non-busy (!) wait for state change
					$this->fullCurlMultiExec($cmh, $active);
					// get new state
					while ($info = curl_multi_info_read($cmh)) {
						pr($info);
						// process completed request (e.g. curl_multi_getcontent($info['handle']))
					}
				} while ($active > 0);

				foreach ($curls as $id => $request) {
					$results[$id] = curl_multi_getcontent($request);
					curl_multi_remove_handle($cmh, $request);
					//assuming we're being responsible about our resource management
					curl_close($request);
					//being responsible again.  THIS MUST GO AFTER curl_multi_getcontent();
				}
				curl_multi_close($cmh);
				return $results;
			}
		} catch (exception $e) {
			echo $e -> getMessage();
			exit ;
		}
		return $result;
	}

	/**
	 * cURL Multi continues till ends
	 */
	public function fullCurlMultiExec($mh, &$still_running) {
		do {
			$rv = curl_multi_exec($mh, $still_running);
		} while ($rv == CURLM_CALL_MULTI_PERFORM);
		return $rv;
	}

	/**
	 * Resize image
	 * will take the sourceImage & the percent to scale
	 */
	 public function resizeImage($sourceImage, $type = 'thumb', $resizedWidth = 0, $resizedHeight = 0) {
		$sourceWidth 	= imagesx($sourceImage);
		$sourceHeight	= imagesy($sourceImage);
		$maxWidth		= ($type !='thumb') ? $this->imageSizes['max-width'] : $this->imageSizes['thumb-max-width'];
		$minWidth		= ($type !='thumb') ? $this->imageSizes['min-width'] : $this->imageSizes['thumb-min-width'];

		$resizedWidth = empty($resizedWidth) ?
					(($maxWidth < $sourceWidth) ? $maxWidth : $sourceWidth)
					: (($minWidth < $resizedWidth) ? $minWidth : $resizedWidth);
		/**
		 * Set $x $y from the middle if they are not set
		 */
		//$resizedWidth = $sourceWidth * $percent;
		//$resizedHeight = $sourceHeight * $percent;

		if(empty($resizedHeight)) {
			$resizedHeight = ($sourceHeight * $resizedWidth) / $sourceWidth;
		}

		$resizedImage	= imagecreatetruecolor($resizedWidth, $resizedHeight);

		// Copy
		//imagecopy($croppedImage, $sourceImage, 0, 0, $x, $y, $croppedWidth, $croppedHeight);
		//using copyresampled for better output
		imagecopyresampled($resizedImage, $sourceImage, 0, 0, 0, 0, $resizedWidth, $resizedHeight, $sourceWidth, $sourceHeight);
		/*
		 * header('Content-Type: image/jpeg');
		 * imagejpeg($resizedImage);
		 */
		//imagedestroy($resizedImage);
		//imagedestroy($sourceImage);

		return $resizedImage;
	}

	public function beforeSave($options = array()) {
		$this->data = $this->sanitize($this->data);
	}
	public function sanitize(array $data){
	    $sanitized = array();
	    foreach($data as $key => $value){
	        if(is_array($value)){
	            $sanitized[$key] = $this->sanitize($value);
	        } else {
	            $sanitized[$key] = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
	        }
	    }

	    return $sanitized;
	}
}
