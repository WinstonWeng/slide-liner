<?php
App::uses('AppModel', 'Model');
/**
 * Edm Model
 *
 */
class Edm extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'fname' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'First name cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'lname' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Last name cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'email' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Email cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'email' => array(
				'rule' => array('email'),
				'message' => 'Invalid email address',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'postcode' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Postcode cannot be empty',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	public function subscribe($data = null) {
		$existingSubscriber = $this->findByEmail($data['Edm']['email']);
		if(empty($existingSubscriber['Edm']['id'])) {
			$this->create();
		} else {
			$data['Edm']['id'] = $existingSubscriber['Edm']['id'];
		}

		if ($this->save($data)) {
			$result = array('status' => 'success', 'message' => 'Successfully subscribed for EDM', 'data' => $this->validationErrors);
		} else {
			$result = array('status' => 'fail', 'message' => 'Errors', 'data' => $this->validationErrors);
		}
		return $result;
	}

	public function beforeSave($options = array()) {
		parent::beforeSave();
		if(!empty($this->data['Edm']['optInFor'])) {
			$this->data['Edm']['optInFor'] = json_encode($this->data['Edm']['optInFor'] );
		}
	}
}
