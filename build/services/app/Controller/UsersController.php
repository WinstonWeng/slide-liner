<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_login() {
	
		if($this->Auth->loggedIn() ||
				($this->request->is(array('post', 'put')) &&
				$this->Auth->login())) {
			return $this->redirect(array('controller' => 'Participants', 'action' => 'index'));
		}
	}

	public function admin_logout() {
		return $this->redirect($this->Auth->logout());
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_change_password() {
		$user = $this->Auth->user();
		if ($this->request->is(array('post', 'put'))) {
			if($this->request->data['User']['password'] == $this->request->data['User']['confirm_password']) {
				$this->User->id = $user['id'];
				$newPassword = Security::hash($this->request->data['User']['password'], null, true);
				if ($this->User->saveField('password', $newPassword)) {
					return $this->redirect(array('controller' => 'participants', 'action' => 'admin_index'));
				}
			}

		}
	}
}
