<?php
App::uses('AppController', 'Controller');
/**
 * Participants Controller
 *
 * @property Participant $Participant
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ParticipantsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public function register() {
		if ($this->request->is('post')) {
			$this->Participant->create();
			if(!empty($this->request->data['Participant']['uploadType'])) {
				$uploadType = $this->request->data['Participant']['uploadType'];
				unset($this->request->data['Participant']['uploadType']);
			}
			if(!empty($this->request->data['Participant']['optIn']) && $this->request->data['Participant']['optIn'] == 'on') {
				$this->request->data['Participant']['optIn'] = 1;
			}
			if(!empty($this->request->data['Participant']['tnc']) && $this->request->data['Participant']['tnc'] == 'on') {
				$this->request->data['Participant']['tnc'] = 1;
			}
			if ($this->Participant->save($this->request->data)) {
				//uploadImage
				if(!empty($this->request->data['Participant']['receiptCopy'])) {
					$receiptCopy	= $this->request->data['Participant']['receiptCopy'];
					$fname			= preg_replace('/[^a-z]/i','',$this->request->data['Participant']['fname']);
					$ext			= pathinfo($receiptCopy['name'], PATHINFO_EXTENSION);

					$fileName = strtoupper($fname[0]) . $this->Participant->id . '.' .$ext;
					$uploaded = $this->Participant->uploadFile($receiptCopy, $fileName);
					if($uploaded) {
						$this->Participant->saveField('receipt', $fileName);
					} else {
						$this->Participant->validationErrors['FileUpload'] = array('failed');
					}
				}

				if(!empty($this->request->data['Participant']['optIn'])) {
					$Edm = array(
						'Edm' => array(
							'fname' => $this->request->data['Participant']['fname'],
							'lname' => $this->request->data['Participant']['lname'],
							'email' => $this->request->data['Participant']['email'],
							'postcode' => $this->request->data['Participant']['postcode']
						)
					);
					$edmSubscribe = ClassRegistry::init('Edm')->subscribe($Edm);
				}
				$data = array('status' => 'success', 'message' => 'Successfully registered participant', 'data' => $this->Participant->validationErrors);
			} else {
				$data = array('status' => 'fail', 'message' => $this->formatErrors($this->Participant->validationErrors), 'data' => $this->Participant->validationErrors);
			}

			if(!empty($uploadType) && $uploadType == 'iframe') {
				header('content-type text/html charset=utf-8');
				header('Access-Control-Allow-Origin: *');
				echo "<html>
						<body>
						<script language=\"javascript\">
							var response='{\"action\":\"register\",\"result\":" . json_encode($data) . "}';
							window.parent.postMessage(response,'*');
						</script>
						</body>
					</html>";
				exit;
			} else {
				$this->set('data', $data);
			}

		}
	}
/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Participant->recursive = 0;
		$this->Paginator->settings = array(
	        'order'		=> array('created' => 'DESC')
	    );
		$this->set('participants', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Participant->exists($id)) {
			throw new NotFoundException(__('Invalid Participant'));
		}
		$options = array('conditions' => array('Participant.' . $this->Participant->primaryKey => $id));
		$this->set('participant', $this->Participant->find('first', $options));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Participant->exists($id)) {
			throw new NotFoundException(__('Invalid Participant'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Participant->save($this->request->data)) {
				$this->Session->setFlash(__('The Participant has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Participant could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Participant.' . $this->Participant->primaryKey => $id));
			$this->request->data = $this->Participant->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Participant->id = $id;
		if (!$this->Participant->exists()) {
			throw new NotFoundException(__('Invalid Participant'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Participant->delete()) {
			$this->Session->setFlash(__('The Participant has been deleted.'));
		} else {
			$this->Session->setFlash(__('The Participant could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_export() {
		$data = $this->Participant->find('all', array(
			'recursive' => -1,
			'order'		=> array('Participant.created DESC')
		));
		$part = $_headers = array();
		foreach ($data as $row) {
			if(empty($part)) {
				$part[] = array_combine(array_keys($row['Participant']), array_keys($row['Participant']));
			}
			if(!empty($row['Participant']['receipt'])) {
				$row['Participant']['receipt'] = str_ireplace('/services', '/uploads/', $this->getServiceUrl(true) . $this->base) . $row['Participant']['receipt'];
			}
			$part[] = $row['Participant'];

		}
		$data = $part;
		$_serialize = 'data';
		$excludePaths = array('Participant.id'); // Exclude all id fields
		$this->response->download('Slideliner-Participants-'. date('Y-m-d') .'.csv'); // <= setting the file name
		$this->viewClass = 'CsvView.Csv';
		$this->set(compact('data', '_serialize'));
	}
}
