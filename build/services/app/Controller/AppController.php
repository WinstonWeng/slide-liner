<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::import('Sanitize');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $components = array(
		'Paginator', 'Session', 'Security', //'Auth'
	);
	public $servicePath;
	public $serviceUrl;
	public $loggedIn = false;

	public function beforeFilter() {
		$this->Security->blackHoleCallback = 'blackhole';
		if(Router::getParam('prefix', true) == 'admin') {

			if($_SERVER['SERVER_NAME'] == 'au.stg-campaign.samsung.com' || $_SERVER['SERVER_NAME'] == 'samsung.com.au' || $_SERVER['SERVER_NAME'] == 'www.samsung.com.au') {
				$adminPorts = array('9000', '9008');
			} else {
				$adminPorts = array('80');
			}

			if(!empty($_SERVER['SERVER_PORT']) && in_array($_SERVER['SERVER_PORT'], $adminPorts)) {
				$this->Auth = $this->Components->load('Auth');
				$this->Auth->initialize($this); // this needs to be added otherwise it won't trigger

				$this->Auth->loginAction = array('controller' => 'users', 'action' => 'admin_login', 'plugin' => false);
				if(!$this->Auth->loggedIn()) {
					$this->Auth->login();
				} else {
					$this->loggedIn = true;
				}

			} else {
				header("Location: " . $this->getServiceUrl(true) . '/samsungslideliner');
				exit;
			}

		} else {
			$this->setAjaxLayout();
		}
	}

	public function setAjaxLayout() {
		$this -> layout = 'ajax';
		$this -> view = '../Elements/ajax';

		if (!empty($this -> request -> query)) {
			if(!empty($this -> request -> query['callback'])) {
				$this -> set('callback', $this -> request -> query['callback']);
			}
			if(!empty($this -> request -> query['data'])) {
				$this -> request -> data = $this -> request -> query['data'];
			}

		}
	}
	public function blackhole($type) {

	}
	public function beforeRender() {
		$url = str_ireplace('phase2/', '', $this->getServiceUrl(true).$this->base);
		$this->set('serviceUrl', $url);
		$this->set('loggedIn', $this->loggedIn);
	}

	public function formatErrors($errors = null) {
		$str = '';
		foreach($errors as $error) {
			$str = $error[0];
			break;
		}
		return $str;
	}

	public function getUrl() {
		return sprintf(
		    "%s://%s%s",
		    isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
		    $_SERVER['SERVER_NAME'],
		    $_SERVER['REQUEST_URI']
  		);
	}

	public function getServiceUrl($full = false) {
		if($full) {
			return str_ireplace($this->here, '', $this->getUrl());
		} else {
			return $this->base;
		}
	}


	/**
	 *
	 * ---------------------- CURL Codes ----------------------
	 *
	 */
	public function cURLRequest($url, $params = array(), $isPost = false, $postJSON = false) {
		try {
			if (empty($isPost) && !empty($params)) {
				$url .= '?' . http_build_query($params);
			}
			$curl = curl_init($url);

			if (!empty($isPost)) {
				curl_setopt($curl, CURLOPT_POST, true);
				if (!empty($params)) {
					curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
				}
			}
			if (!empty($postJSON)) {
				curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
			}
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

			if (!($result = curl_exec($curl))) {
				//echo curl_error($curl);
			} else {
				$result = $result;
			}
			curl_close($curl);

		} catch (exception $e) {
			return false;
		}

		return $result;
	}

	/**
	 * Processing Multi cURL to fetch images in batches
	 */
	public function cURLMultiRequests($links = array()) {//id, url
		try {
			if (!empty($links)) {
				$cmh = curl_multi_init();
				$curls = $results = array();

				foreach ($links as $link) {
					$id = $link['Link']['id'];
					$url = $link['Link']['source_url'];
					$curls[$id] = curl_init($url) or die('cannot curl init');

					curl_setopt($curls[$id], CURLOPT_URL, $url);
					curl_setopt($curls[$id], CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curls[$id], CURLOPT_SSL_VERIFYPEER, false);

					curl_multi_add_handle($cmh, $curls[$id]);
				}

				do {
					curl_multi_select($cmh);
					// non-busy (!) wait for state change
					$this->fullCurlMultiExec($cmh, $active);
					// get new state
					while ($info = curl_multi_info_read($cmh)) {
						pr($info);
						// process completed request (e.g. curl_multi_getcontent($info['handle']))
					}
				} while ($active > 0);

				foreach ($curls as $id => $request) {
					$results[$id] = curl_multi_getcontent($request);
					curl_multi_remove_handle($cmh, $request);
					//assuming we're being responsible about our resource management
					curl_close($request);
					//being responsible again.  THIS MUST GO AFTER curl_multi_getcontent();
				}
				curl_multi_close($cmh);
				return $results;
			}
		} catch (exception $e) {
			echo $e -> getMessage();
			exit ;
		}
		return $result;
	}

	/**
	 * cURL Multi continues till ends
	 */
	public function fullCurlMultiExec($mh, &$still_running) {
		do {
			$rv = curl_multi_exec($mh, $still_running);
		} while ($rv == CURLM_CALL_MULTI_PERFORM);
		return $rv;
	}
}
