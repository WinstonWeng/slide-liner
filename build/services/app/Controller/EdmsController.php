<?php
App::uses('AppController', 'Controller');
/**
 * Edms Controller
 *
 * @property Edm $Edm
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class EdmsController extends AppController {

/**
 * add method
 *
 * @return void
 */
	public function subscribe() {
		if ($this->request->is('post')) {
			$data = $this->Edm->subscribe($this->request->data);
			$this->set('data', $data);
		}
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Edm->recursive = 0;
		$this->Paginator->settings = array(
	        'order'		=> array('created' => 'DESC')
	    );
		$this->set('edms', $this->Paginator->paginate());
	}
	public function admin_export() {
		$data = $this->Edm->find('all', array(
			'recursive' => -1,
			//'order'		=> array('Edms.created DESC')
		));
		$part = $_headers = array();
		foreach ($data as $row) {
			if(empty($part)) { //Adding Header
				$part[] = array_combine(array_keys($row['Edm']), array_keys($row['Edm']));
			}
			$part[] = $row['Edm'];

		}
		$data = $part;
		$_serialize = 'data';
		$this->response->download('Slideliner-Edms-'. date('Y-m-d') .'.csv'); // <= setting the file name
		$this->viewClass = 'CsvView.Csv';
		$this->set(compact('data', '_serialize'));
	}
}
