<?php
App::uses('AppController', 'Controller');
class CronController extends AppController {

	public function beforeFilter() {
	    parent::beforeFilter();
	    $this->layout = null;
		$this->autoRender = false;

		// Check the action is being invoked by the cron dispatcher
		if (!defined('CRON_DISPATCHER')) {
			//$this->redirect('/');
			//exit;
		}
	}
	/**
	 * to run this from command line run this:
	 * php -q app/cron.php cron/test
	 */
	public function test() {
		//no view
		$this->autoRender = false;

		for($i = 1; $i <= 10; $i++) {
			echo "{$i}. hello\r\n";
		}
		exit;
		return;
	}
	/**
	 * Get Scial Data
	 */
	public function getFacebookData() {
		$SocialMessage = ClassRegistry::init('SocialMessage');
		$socialData = $SocialMessage->importFacebookData();
		exit;
	}
	 public function getInstagramData() {
	 	$SocialMessage = ClassRegistry::init('SocialMessage');
		$socialData = $SocialMessage->importInstagramData();
		exit;
	}
	 public function getTwitterData() {
	 	$SocialMessage = ClassRegistry::init('SocialMessage');
		$socialData = $SocialMessage->importTwitterData();
		exit;
	}
}
?>