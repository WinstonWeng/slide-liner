<?php
App::uses('AppController', 'Controller');
/**
 * SocialMessages Controller
 *
 * @property SocialMessage $SocialMessage
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SocialMessagesController extends AppController {

	public function getImages($page = 1) {
		$limit	= 20;
		$offset	= $limit * ($page - 1);		 
		$data	= $this->SocialMessage->find('all' , array(
			'conditions' => array('status' => 1 , 'postImageLarge !=' => ''), //only active ones
			'offset'	=> $offset,
			'limit' 	=> $limit,
			'order'		=> array('posted' => 'DESC')
		));
		//print(json_encode($data));
		if(!empty($data)) {
			$data = array('status' => 'Success',
					'message' => 'Social Contents',
					'data'	=> $data
				);
		} else {
			$data = array('status' => 'fail',
					'message' => 'No approved Social content found',
					'data'	=> null
				);
		}
		$this->set('data', $data);
	}

	public function upload() {
		if ($this->request->is('post')) {
			$this->SocialMessage->create();
			if ($this->SocialMessage->save($this->request->data)) {
				//uploadImage
				$uploadedImage	= $this->request->data['SocialMessage']['userImage'];
				$fname			= preg_replace('/[^a-z]/i','',trim($this->request->data['SocialMessage']['fname']));
				//$ext			= pathinfo($uploadedImage['name'], PATHINFO_EXTENSION);

				$fileName = strtoupper($fname[0]) . $this->SocialMessage->id;//' .$ext;
				$uploaded = $this->SocialMessage->uploadFile($uploadedImage, $fileName);
				if($uploaded) {
					$this->SocialMessage->saveField('postImageLarge', '/samsungsound/uploads/events/' . $uploaded['large']);
					$this->SocialMessage->saveField('postImageSmall', '/samsungsound/uploads/events/' . $uploaded['thumb']);
				} else {
					$this->SocialMessage->validationErrors['FileUpload'] = array('failed');
				}

				$data = array('status' => 'success', 'message' => 'Successfully uploaded', 'data' => $this->SocialMessage->validationErrors);
			} else {
				$data = array('status' => 'fail', 'message' => $this->formatErrors($this->SocialMessage->validationErrors), 'data' => $this->SocialMessage->validationErrors);
			}

			$this->set('data', $data);
		}
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($type = null) {

		$this->SocialMessage->recursive = 0;
		$status = $this->SocialMessage->status;

		if(empty($type)) {
			$type = 'approved';
		}
		$this->Paginator->settings = array(
	        'conditions' => array('status' => "{$status[$type]}"),
	        'order'		=> array('created' => 'DESC')
	    );
		$socialMessages = $this->Paginator->paginate();

		$this->set(compact('socialMessages', 'type', 'status'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->SocialMessage->exists($id)) {
			throw new NotFoundException(__('Invalid social message'));
		}
		$options = array('conditions' => array('SocialMessage.' . $this->SocialMessage->primaryKey => $id));
		$this->set('socialMessage', $this->SocialMessage->find('first', $options));
	}

/**
 * admin_status method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_status($id = null, $status = 'rejected') {
		$this->SocialMessage->id = $id;
		/*
		 * Update status
		 */

		if ($this->SocialMessage->saveField('status', $this->SocialMessage->status[$status])) {
			$this->Session->setFlash(__('The post has been ' . $status));
			return $this->redirect(array('action' => 'index'));
		} else {
			$this->Session->setFlash(__('Failed to change status'));
		}
	}

	/**
	 * ------------------------------ SOCIAL DATA IMPORT ----------------------------
	 */
	public function admin_getFacebookData() {
		$this -> layout = 'ajax';
		$this -> view = '../Elements/ajax';

		$socialData = $this->SocialMessage->importFacebookData();
		$this->set('data', $socialData);
	}
	 public function admin_getInstagramData() {
	 	$this -> layout = 'ajax';
		$this -> view = '../Elements/ajax';

		$socialData = $this->SocialMessage->importInstagramData();
		$this->set('data', $socialData);
	}
	 public function admin_getTwitterData() {
	 	$this -> layout = 'ajax';
		$this -> view = '../Elements/ajax';

		$socialData = $this->SocialMessage->importTwitterData();
		$this->set('data', $socialData);
	}


	public function admin_weeklyPosts($event_id = null) {

		$this->SocialMessage->recursive = 0;
		$condition	= empty($event) ? array('isCurrent' => 1) : array('id' => $event_id);
		$event		= ClassRegistry::init('Event')->find('first', array('conditions' => $condition));

		if(!empty($this->request->params['named']['start']) && !empty($this->request->params['named']['end'])) {
			$dates = array(
				$this->request->params['named']['start'],
				$this->request->params['named']['end']
			);
		} else {
			$dates = array(
				$event['Event']['opening'],
				$event['Event']['closing']
			);
		}

		$this->Paginator->settings = array(
	        'conditions' =>  array(
				array('SocialMessage.posted BETWEEN ? AND ?' => $dates)
			),
			//'limit' => 5,
	        'order'		=> array('created' => 'DESC')
	    );
		$socialMessages = $this->Paginator->paginate();

		$weeks = $this->getWeekStartEnds($event['Event']['opening'], $event['Event']['closing']);
		$this->set(compact('socialMessages', 'weeks', 'dates', 'event', 'event_id' ));
	}

	private function getWeekStartEnds($start_date, $end_date) {

		$start_date = date('Y-m-d', strtotime($start_date));
		$end_date = date('Y-m-d', strtotime($end_date));
		$end_date1 = date('Y-m-d', strtotime($end_date. '+ 6 days'));

		$weeks = array();

		for($date = $start_date; $date <= $end_date1; $date = date('Y-m-d', strtotime($date. ' + 7 days'))) {
			$week =  date('W', strtotime($date));
		    $year =  date('Y', strtotime($date));
		    $from = date("Y-m-d", strtotime("{$year}-W{$week}+1")); //Returns the date of monday in week

		    if(strtotime($from) < strtotime($start_date)) {
		    	$from = $start_date;
			}

		    $to = date("Y-m-d", strtotime("{$year}-W{$week}-7"));   //Returns the date of sunday in week

		    if($to > $end_date) {
		    	$to = $end_date;
			}

			$weeks[] = array('start' => $from, 'end' =>$to);
		}
		return $weeks;
	}

	public function admin_export() {
		$params = '';
		if(!empty($this->request->params['named']['start']) && !empty($this->request->params['named']['end'])) {
			$params = array(
				'conditions' => array('SocialMessage.posted BETWEEN ? AND ?' => array(
					$this->request->params['named']['start'], $this->request->params['named']['end'])
				)
			);
		}
		$params['recursive']= -1;
		$params['order']	= array('SocialMessage.created DESC');

		$data = $this->SocialMessage->find('all', $params);

		$part = $_headers = array();
		foreach ($data as $row) {
			if(empty($part)) {

				if(!empty($row['SocialMessage']['uploadData'])) {
					$row['SocialMessage']['postImageSmall'] = $this->getServiceUrl(true).$this->base . "/../uploads/events/" . str_replace('samsungsound/uploads/events/', '', $row['SocialMessage']['postImageSmall']);
					$row['SocialMessage']['postImageLarge'] = $this->getServiceUrl(true).$this->base . "/../uploads/events/" . str_replace('samsungsound/uploads/events/', '', $row['SocialMessage']['postImageLarge']);
				}

				$part[] = array_combine(array_keys($row['SocialMessage']), array_keys($row['SocialMessage']));
			}
			$part[] = $row['SocialMessage'];
		}

		$data = $part;
		$_serialize = 'data';
		$excludePaths = array('SocialMessage.id', 'SocialMessage.location'); // Exclude all id fields
		$this->response->download('SamsungSound-' . date('Y-m-d') .'.csv'); // <= setting the file name
		$this->viewClass = 'CsvView.Csv';
		$this->set(compact('data', '_serialize'));


	}
}
