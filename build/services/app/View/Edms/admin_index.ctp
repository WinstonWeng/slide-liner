<div class="edms index">
	<h2><?php echo __('Subcribers'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('fname'); ?></th>
			<th><?php echo $this->Paginator->sort('lname'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('postcode'); ?></th>
			<th><?php echo $this->Paginator->sort('optInFor'); ?></th>
			<th><?php echo $this->Paginator->sort('optInOther'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('updated'); ?></th>
	</tr>
	<?php foreach ($edms as $edm): ?>
	<tr>
		<td><?php echo h($edm['Edm']['id']); ?>&nbsp;</td>
		<td><?php echo h($edm['Edm']['fname']); ?>&nbsp;</td>
		<td><?php echo h($edm['Edm']['lname']); ?>&nbsp;</td>
		<td><?php echo h($edm['Edm']['email']); ?>&nbsp;</td>
		<td><?php echo h($edm['Edm']['postcode']); ?>&nbsp;</td>
		<td><?php echo h($edm['Edm']['optInFor']); ?>&nbsp;</td>
		<td><?php echo h($edm['Edm']['optInOther']); ?>&nbsp;</td>
		<td><?php echo h($edm['Edm']['created']); ?>&nbsp;</td>
		<td><?php echo h($edm['Edm']['updated']); ?>&nbsp;</td>

	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
	<div>

	</div>
</div>
<div class="actions">
	<?php echo $this->element('menu', array(
		'extraMenu' => array(
			array(
			'Text' => 'Download CSV',
			'Controller' => 'Edms',
			'Action' => 'admin_export')
		))
	); ?>
</div>
