<div class="events index">
	<h2><?php echo __('Events'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('opening'); ?></th>
			<th><?php echo $this->Paginator->sort('closing'); ?></th>
			<th><?php echo $this->Paginator->sort('Current'); ?></th>
			<th><?php echo $this->Paginator->sort('Socials'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($events as $event): ?>
	<tr>
		<td><?php echo h($event['Event']['id']); ?>&nbsp;</td>
		<td><?php echo h($event['Event']['name']); ?>&nbsp;</td>
		<td><?php echo h($event['Event']['opening']); ?>&nbsp;</td>
		<td><?php echo h($event['Event']['closing']); ?>&nbsp;</td>
		<td><?php echo h($event['Event']['isCurrent']); ?>&nbsp;</td>
		<td><?php echo h($event['Event']['socials']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'manage', $event['Event']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>
	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
	<div class="action"><?php echo $this->Html->link(__('Add Event'), array('action' => 'manage')); ?></div>
</div>
<div class="actions">
	<?php echo $this->element('menu'); ?>
</div>
