<div class="events form">
<?php echo $this->Form->create('Event'); ?>
	<fieldset>
		<legend><?php echo __('Event Management'); ?></legend>
	<?php

		if(!empty($this->data)) {
			echo $this->Form->input('id');
		}

		echo $this->Form->input('code');
		echo $this->Form->input('name');
		echo $this->Form->input('opening', array(
			'type' => 'date',
			'dateFormat' => 'DMY',
		    'minYear' => date('Y'),
		    'maxYear' => date('Y')
		));
		echo $this->Form->input('closing', array(
			'type' => 'date',
			'dateFormat' => 'DMY',
		    'minYear' => date('Y'),
		    'maxYear' => date('Y')
		));
		echo $this->Form->input('isCurrent', array('type' => 'checkbox', 'label' => 'Current Event'));

		$selected = !empty($this->data['Event']['socials']) ? json_decode($this->data['Event']['socials']) : array();
		echo $this->Form->input('socials', array(
		   // 'label' => '<p style="font-weight:bold;">Select Duty Employee</p>',
		    'type' => 'select',
		    'multiple' => 'checkbox',
		    'options' => $socialOptions,
		    'selected' => $selected
		  ));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Save')); ?>
</div>
<div class="actions">
	<?php echo $this->element('menu'); ?>
</div>
