<?php

$menus = array(
	array('Text' => 'Participants','Controller' => 'Participants', 'Action' => 'admin_index'),
	array('Text' => 'Subcribers','Controller' => 'Edms', 'Action' => 'admin_index'),
	array('Text' => 'Social','Controller' => 'SocialMessages', 'Action' => 'admin_index'),
	array()
);
if(!empty($extraMenu)) {
	$menus = array_merge($menus, $extraMenu);
}

if($loggedIn) {

echo '<h3>' . __('Admin Menus') . '</h3>
<ul>';
	foreach($menus as $menu) {
		if(!empty($menu)) {
			echo '<li>' . $this->Html->link(__($menu['Text']),
				array('controller' => $menu['Controller'],
				'action' => $menu['Action'])
			) . '</li>';
		} else {
			echo '<li>-----------------------</li>';
		}
	}

	if(!empty($extraMenu)) {
		echo '<li>-----------------------</li>';
	}
	echo '<li>' . $this->Html->link('Change Password',
				array('controller' => 'Users',
				'action' => 'admin_change_password'))
		. '</li>';
	echo '<li>' . $this->Html->link('Logout',
				array('controller' => 'Users',
				'action' => 'admin_logout'))
		. '</li>';

echo '</ul>';

}