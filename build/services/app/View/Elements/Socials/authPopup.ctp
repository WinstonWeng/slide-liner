<?php
	$windowWidth = !empty($windowWidth) ? $windowWidth : 500;
	$windowHeight = !empty($windowHeight) ? $windowHeight : 500;

	$content =  'Please follow the prompts to authorise access to ##SOCIAL## (if required).
				This window will close automatically after the authorisation';

?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Connecting to <?php echo $socialSource; ?>...</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="/css/popup.css">

        <script>
			function resizeWindow() {
				var width = <?php echo $windowWidth; ?>;
				var height = <?php echo $windowHeight; ?>;
				self.resizeTo(width, height);
				self.moveTo(((screen.width - width) / 2), ((screen.height - height) / 2));
			}

			window.onload = function () {
				resizeWindow();
				setInterval(location.replace("<?php echo $redirectUrl; ?>") , 500);
			}
			window.onunload = function () {
				window.opener.location.reload();
			}
		</script>
    </head>
    <body class="animated fadeIn slow">
        <div class="header">Samsung Sound</div>
        <div class="connecting">
            <h2><strong>Connecting to</strong> <em><?php echo $socialSource; ?></em>...</h2>
            <p><?php echo str_replace('##SOCIAL##', $socialSource, $content); ?></p>
        </div>
    </body>
</html>