<div class="socialMessages index">
	<h2><?php echo __('Social Messages'); ?></h2>
	<?php
		$stats = array_flip($status);
		foreach ($stats as $stat) {
			echo "<div class='tabs" . (($stat == $type) ? ' selected' : '') . "'>" . $this->Html->link(__($stat), array('action' => 'index', $stat)) ."</div>";
		}
	?>

	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('Social Type'); ?></th>
		<th><?php echo $this->Paginator->sort('Post'); ?></th>
		<th><?php echo $this->Paginator->sort('Profile'); ?></th>
		<th><?php echo $this->Paginator->sort('Message'); ?></th>
		<th><?php echo $this->Paginator->sort('Status'); ?></th>
		<th><?php echo $this->Paginator->sort('Activities'); ?></th>
		<th><?php echo $this->Paginator->sort('posted'); ?></th>
		<!--<th><?php echo $this->Paginator->sort('created'); ?></th>-->
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($socialMessages as $socialMessage): ?>
	<tr>
		<td><?php echo h($socialMessage['SocialMessage']['socialType']); ?>&nbsp;</td>
		<td>Post ID:
			<div class='ellipsis'>
				<?php echo $this->Html->link(
					h($socialMessage['SocialMessage']['postId']),
					h($socialMessage['SocialMessage']['postUrl'])
				);?>
			</div>
		<br>
			<?php
			if(($socialMessage['SocialMessage']['postImageLarge'])) :
				if(!empty($socialMessage['SocialMessage']['uploadData'])) :
					$thumb = $serviceUrl . "/../uploads/events/" . str_replace('samsungsound/uploads/events/', '', h($socialMessage['SocialMessage']['postImageSmall']));
					$large = $serviceUrl . "/../uploads/events/" . str_replace('samsungsound/uploads/events/', '', h($socialMessage['SocialMessage']['postImageLarge']));
				else:
					$thumb = h($socialMessage['SocialMessage']['postImageSmall']);
					$large = h($socialMessage['SocialMessage']['postImageLarge']);
				endif;
				echo $this->Html->link(
						$this->Html->image ($thumb), $large,array('escape' => false, 'target'=>'_blank')
					);
			endif;
			?>
		</td>
		<td>
			ID: <?php echo h($socialMessage['SocialMessage']['profileId']); ?><br>
			Profile Name:
			<?php
			if(!empty($socialMessage['SocialMessage']['uploadData'])) :
				h($socialMessage['SocialMessage']['profileName']);
			else:
				echo $this->Html->link(
					h($socialMessage['SocialMessage']['profileName']),
					h($socialMessage['SocialMessage']['profileLink']),
					array('target'=>'_blank')
				);
			endif;
			?>
		</td>
		<td><?php echo h($socialMessage['SocialMessage']['postMessage']); ?>&nbsp;</td>

		<td><?php echo h($stats[$socialMessage['SocialMessage']['status']]); ?>&nbsp;</td>
		<td>
			<?php
				if(!empty($socialMessage['SocialMessage']['uploadData'])) :
					$uploadData = json_decode($socialMessage['SocialMessage']['uploadData']);
					echo '<ul style="min-width: 220px;">';
					unset($uploadData->userImage);
					foreach ($uploadData as $k => $v) {
						echo '<li>' . h($k) . ' : ' . h($v) . '</li>';
					}
					echo '</ul>';
				else :
					echo 'Likes ' . h($socialMessage['SocialMessage']['likes']) . '<br>' .
						 'Shares: '. h($socialMessage['SocialMessage']['shares']);
				endif;
			?>
		</td>
		<td><?php echo h($socialMessage['SocialMessage']['posted']); ?>&nbsp;</td>
		<!--<td><?php echo h($socialMessage['SocialMessage']['created']); ?>&nbsp;</td>-->
		<td class="actions">
			<?php
				if($type != 'approved') {
					echo $this->Html->link(
					    'Approve',
					    array('action' => 'status', $socialMessage['SocialMessage']['id'], 'approved'),
					    array(),
					    "Are you sure you want approve this?"
					);
				}
				if($type != 'rejected') {
					echo $this->Html->link(
					    'Reject',
					    array('action' => 'status', $socialMessage['SocialMessage']['id'], 'rejected'),
					    array(),
					    "Are you sure you want to reject this?"
					);
				}

			?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<?php echo $this->element('menu'); ?>
</div>
