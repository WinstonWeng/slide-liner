<div class="socialMessages index">
	<h2><?php echo __('Weekly Social Aggregation'); ?></h2>
	<?php

		if(!empty($weeks)) {
			$selected = ($dates[0] == $event['Event']['opening'] && $dates[1] == $event['Event']['closing']) ? ' selected' : '';
			$i = 0;
			echo "<div class='weekBox $selected'>" . $this->Html->link('All', array(
				'controller'=> 'SocialMessages',
				'action'	=> 'weeklyPosts',
				'start' 	=> $event['Event']['opening'],
				'end'		=> $event['Event']['closing'] //next sunday
			)) . '</div>';
			$start = $end = '';
			foreach ($weeks AS $week) {
				$i++;
				$selected = '';
				if($week['start'] == $dates[0] && $week['end'] == $dates[1]) {
					$selected	= 'selected';
					$start		= $week['start'];
					$end		= $week['end'];
				}
				echo "<div class='weekBox $selected'>" . $this->Html->link('Week-' . $i, array(
					'controller'=> 'SocialMessages',
					'action'	=> 'weeklyPosts',
					'start' 	=> $week['start'],
					'end'		=> $week['end'] //next sunday
				)) . '</div>';

			}
			if(!empty($socialMessages)) :
				echo "<div>
					<div class='weekBox selected'>" . $this->Html->link('Export Data',
					array(
						'controller'=> 'SocialMessages',
						'action'	=> 'export',
						'start' 	=> $start,
						'end'		=> $end
					)) . '</div>
				</div>';
			endif;
		}
	?>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('Social Type'); ?></th>
		<th><?php echo $this->Paginator->sort('Post'); ?></th>
		<th><?php echo $this->Paginator->sort('Profile'); ?></th>
		<th><?php echo $this->Paginator->sort('Message'); ?></th>
		<th><?php echo $this->Paginator->sort('Activities'); ?></th>
		<th><?php echo $this->Paginator->sort('posted'); ?></th>

	</tr>
	<?php foreach ($socialMessages as $socialMessage): ?>
	<tr>
		<td><?php echo h($socialMessage['SocialMessage']['socialType']); ?>&nbsp;</td>
		<td>Post ID:
			<div class='ellipsis'>
				<?php echo $this->Html->link(
					h($socialMessage['SocialMessage']['postId']),
					h($socialMessage['SocialMessage']['postUrl'])
				);?>
			</div>
		<br>
			<?php
			if(($socialMessage['SocialMessage']['postImageLarge'])) :
				if(!empty($socialMessage['SocialMessage']['uploadData'])) :
					$thumb = $serviceUrl . "/../uploads/events/" . str_replace('samsungsound/uploads/events/', '', h($socialMessage['SocialMessage']['postImageSmall']));
					$large = $serviceUrl . "/../uploads/events/" . str_replace('samsungsound/uploads/events/', '', h($socialMessage['SocialMessage']['postImageLarge']));
				else:
					$thumb = h($socialMessage['SocialMessage']['postImageSmall']);
					$large = h($socialMessage['SocialMessage']['postImageLarge']);
				endif;
				echo $this->Html->link(
						$this->Html->image ($thumb), $large,array('escape' => false, 'target'=>'_blank')
					);
			endif;
			?>
		</td>
		<td>
			ID: <?php echo h($socialMessage['SocialMessage']['profileId']); ?><br>
			Profile Name:
			<?php
			if(!empty($socialMessage['SocialMessage']['uploadData'])) :
				h($socialMessage['SocialMessage']['profileName']);
			else:
				echo $this->Html->link(
					h($socialMessage['SocialMessage']['profileName']),
					h($socialMessage['SocialMessage']['profileLink']),
					array('target'=>'_blank')
				);
			endif;
			?>
		</td>
		<td><?php echo h($socialMessage['SocialMessage']['postMessage']); ?>&nbsp;</td>
		<td>
			<?php
				if(!empty($socialMessage['SocialMessage']['uploadData'])) :
					$uploadData = json_decode($socialMessage['SocialMessage']['uploadData']);
					echo '<ul style="min-width: 220px;">';
					unset($uploadData->userImage);
					foreach ($uploadData as $k => $v) {
						echo '<li>' . h($k) . ' : ' . h($v) . '</li>';
					}
					echo '</ul>';
				else :
					echo 'Likes ' . h($socialMessage['SocialMessage']['likes']) . '<br>' .
						 'Shares: '. h($socialMessage['SocialMessage']['shares']);
				endif;
			?>
		</td>
		<td><?php echo h($socialMessage['SocialMessage']['posted']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<?php echo $this->element('menu'); ?>
</div>
