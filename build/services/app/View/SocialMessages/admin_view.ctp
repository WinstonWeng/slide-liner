<div class="socialMessages view">
<h2><?php echo __('Social Message'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($socialMessage['SocialMessage']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('SocialType'); ?></dt>
		<dd>
			<?php echo h($socialMessage['SocialMessage']['socialType']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ProfileId'); ?></dt>
		<dd>
			<?php echo h($socialMessage['SocialMessage']['profileId']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ProfileName'); ?></dt>
		<dd>
			<?php echo h($socialMessage['SocialMessage']['profileName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ProfileImage'); ?></dt>
		<dd>
			<?php echo h($socialMessage['SocialMessage']['profileImage']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ProfileLink'); ?></dt>
		<dd>
			<?php echo h($socialMessage['SocialMessage']['profileLink']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PostId'); ?></dt>
		<dd>
			<?php echo h($socialMessage['SocialMessage']['postId']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PostImageLarge'); ?></dt>
		<dd>
			<?php echo h($socialMessage['SocialMessage']['postImageLarge']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PostImageSmall'); ?></dt>
		<dd>
			<?php echo h($socialMessage['SocialMessage']['postImageSmall']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PostMessage'); ?></dt>
		<dd>
			<?php echo h($socialMessage['SocialMessage']['postMessage']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PostUrl'); ?></dt>
		<dd>
			<?php echo h($socialMessage['SocialMessage']['postUrl']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($socialMessage['SocialMessage']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Likes'); ?></dt>
		<dd>
			<?php echo h($socialMessage['SocialMessage']['likes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Shares'); ?></dt>
		<dd>
			<?php echo h($socialMessage['SocialMessage']['shares']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Comments'); ?></dt>
		<dd>
			<?php echo h($socialMessage['SocialMessage']['comments']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Posted'); ?></dt>
		<dd>
			<?php echo h($socialMessage['SocialMessage']['posted']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($socialMessage['SocialMessage']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<?php echo $this->element('menu'); ?>
</div>
