<div class="Participants index">
	<h2><?php echo __('Participants'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('Name'); ?></th>
		<th><?php echo $this->Paginator->sort('Contact'); ?></th>
		<th><?php echo $this->Paginator->sort('receiptNo'); ?></th>
		<th><?php echo $this->Paginator->sort('purchaseDate'); ?></th>
		<th><?php echo $this->Paginator->sort('receipt'); ?></th>
		<th><?php echo $this->Paginator->sort('Product'); ?></th>
		<th><?php echo $this->Paginator->sort('backlog'); ?></th>
		<th><?php echo $this->Paginator->sort('serialNo'); ?></th>
		<th><?php echo $this->Paginator->sort('comment'); ?></th>
		<th><?php echo $this->Paginator->sort('tnc'); ?></th>
		<th><?php echo $this->Paginator->sort('optIn'); ?></th>
	</tr>
	<?php foreach ($participants as $participant): ?>
	<tr>
		<td><?php echo h($participant['Participant']['fname']) . h($participant['Participant']['lname']); ?>&nbsp;</td>
		<td>Phone:<?php echo h($participant['Participant']['phone']); ?><br>
			Email: <?php echo h($participant['Participant']['email']); ?><br>
			Address: <?php echo h($participant['Participant']['address']); ?><br>
			Suburb: <?php echo h($participant['Participant']['suburb']); ?><br>
			State: <?php echo h($participant['Participant']['state']); ?><br>
			Postcode: <?php echo  h($participant['Participant']['postcode']); ?></td>
		<td><?php echo h($participant['Participant']['receiptNo']); ?>&nbsp;</td>
		<td><?php echo h($participant['Participant']['purchaseDate']); ?>&nbsp;</td>
		<td><?php echo (!empty($participant['Participant']['receipt'])) ?
					$this->Html->link(h($participant['Participant']['receipt']), $serviceUrl. "/../uploads/" . h($participant['Participant']['receipt']),
						array('escape' => false, 'target' => '_blank')) : '&nbsp;'; ?></td>

		<td><?php echo h($participant['Participant']['product']); ?>&nbsp;</td>
		<td><?php echo h($participant['Participant']['backlog']); ?>&nbsp;</td>
		<td><?php echo h($participant['Participant']['serialNo']); ?>&nbsp;</td>
		<td><?php echo h($participant['Participant']['comment']); ?>&nbsp;</td>
		<td><?php echo h($participant['Participant']['tnc']); ?>&nbsp;</td>
		<td><?php echo h($participant['Participant']['optIn']); ?>&nbsp;</td>

	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<?php echo $this->element('menu', array(
		'extraMenu' => array(
			array(
			'Text' => 'Download CSV',
			'Controller' => 'Participants',
			'Action' => 'admin_export')
		))
	); ?>
</div>
