<?php

interface ServiceInterface {
	public function authenticate();
	public function authorize();
	public function response();
	public function profile();
	public function logout();
}
