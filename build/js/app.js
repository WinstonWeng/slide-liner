
var app={};

app.facebook_share_url = 'https://www.facebook.com/sharer.php?app_id=257838867740570&u=https%3A%2F%2Fwww.samsung.com.au%2Fsamsungslideliner&display=popup';
app.twitter_share_url = 'https://twitter.com/intent/tweet?original_referer=http://www.samsung.com.au/samsungslideliner&text=It%E2%80%99s%20on%20for%20you%20%26%203%20mates%20-%20win%20seats%20on%20the%20Samsung%20%23SlideLiner%20plus%20a%20VIP%20day%20with%20Nathan%20Sharpe%20%26%20more.&tw_p=tweetbutton&url=http://bit.ly/1qMiv8T';
var Modernizr=(typeof(Modernizr)==="undefined" ? {touch:("ontouchstart" in window)} : Modernizr);
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement, fromIndex) {
      if ( this === undefined || this === null ) {
        throw new TypeError( '"this" is null or not defined' );
      }

      var length = this.length >>> 0; // Hack to convert object.length to a UInt32

      fromIndex = +fromIndex || 0;

      if (Math.abs(fromIndex) === Infinity) {
        fromIndex = 0;
      }

      if (fromIndex < 0) {
        fromIndex += length;
        if (fromIndex < 0) {
          fromIndex = 0;
        }
      }

      for (;fromIndex < length; fromIndex++) {
        if (this[fromIndex] === searchElement) {
          return fromIndex;
        }
      }

      return -1;
    };
};
Number.prototype.padLeft = function (n,str) {
    return (this < 0 ? '-' : '') + 
            Array(n-String(Math.abs(this)).length+1)
             .join(str||'0') + 
           (Math.abs(this));
};
app.control={
	checkbox:function (argument) {
		$(".checkbox-label input").change(function(e){
			var sender=$(e.target);
			if(sender[0].checked){
				sender.parent().addClass("checked");
			}
			else
			{
				sender.parent().removeClass("checked");	
			}
		});
		$(".checkbox-label input").change();
	},
	uploadWrapper:function(){
		$(".upload-wrapper").each(function(i,item){
			$(item).find("input[type='file']").change(function(e){
				var file=this.value,
					allowed="pdf,jpeg,jpg,png,gif,bmp",
					parts=file.split("."),
					ext=parts[parts.length-1],
					fileName=null;
				if(this.files)
				{
					fileName=this.files[0].name;
				}
				else
				{
					var fileNames=file.split("\\");
					fileName=fileNames[fileNames.length-1];
				}
				if(allowed.split(",").indexOf(ext.toLowerCase())>-1)
				{
					$(item).find(".file-display").val(fileName);	
				}
				else
				{
					this.val("");
					$(item).find(".file-display").val("");
				}
				
			});
		});
	},
	start:function(){
		//app.control.checkbox();
		app.control.uploadWrapper();
	}
};

app.formUpload ={
	onMessage: null,
	 uploader: function(options) {
		var me=this;
		var ui=options.events;
		this.upload=function(fileObject){
			

			var formData=new FormData();
			for(var f in fileObject){
				var fd=fileObject[f];
				formData.append(f,fd.value);
			}
			 var xhr = new XMLHttpRequest();
			 xhr.open("POST",options.url);
			 var upload = xhr.upload;
			 xhr.onreadystatechange=function()
			 {
				  if (xhr.readyState==4 && xhr.status==200)
				  {
				    if (ui.uploadReady) {
		                ui.uploadReady($.parseJSON(xhr.responseText));
		            }
				  }
				  else
				  {
				  	if (xhr.readyState==4 && ui.uploadReady) {
		                ui.uploadReady({status:'failed',message:'http error'});
		            }
				  }
			};
			xhr.onerror =function(e){
				var k=0;
			}
			xhr.onload =function(e){
				var k=0;
			}
			upload.addEventListener("progress", function (ev) {
	            if (ev.lengthComputable) {
	                ui.uploadProgress(parseInt(((ev.loaded / ev.total) * 100)));
	            }
	        }, false);
	        upload.addEventListener("load", function (ev) {
	            ui.uploadProcessCompleted();
	            
	        }, false);
	        upload.addEventListener("error", function (ev) {
	            ui.uploadError();

	        }, false);
	        xhr.send(formData);
		};

	   this.uploadIframe=function(fileObject){
	   		if(window.upload && options.iframe){
	   			var iframeSrc=$(options.iframe).attr("src");	   			
	   			app.formUpload.onMessage=function(e){
	   					var  data=null;
						if(e && e.data){
							data=e.data;
						}
						else if(e.originalEvent){
							data=e.originalEvent.data;
						}
						else if(window.event){
							data=window.event.data;
						}
						if(data){
							var obj=$.parseJSON(data);
							if(obj.action==="register" || obj.action==="ssupload"){
								$(window).unbind("message",app.formUpload.onMessage);
					            for(var f in fileObject){
					            	var fb=fileObject[f];
					            	if(fb.type=="file"){
					            		try
					            		{
					            			fb.value.detach()	
					            		}
					            		catch(error){

					            		}
					            		try
					            		{
					            			fb.value.appendTo(fb.parentCon);
					            		}
					            		catch(error){}
					            	}
					            }
								options.iframe.attr({src:iframeSrc});
								if (ui.uploadReady) {
					                ui.uploadReady(obj.result);
					            }
					            
							}
						};
	   			};
	   			$(window).bind("message",app.formUpload.onMessage);	
	   			window.upload(options.url,fileObject);
	   		}
	   		else{
	   			if(ui.uploadReady){
	   				ui.uploadReady({status:'failed',message:'upload not supported'});
	   			}
	   		}
	   };


		this.readData = function(){
			var form,datas,files,uploadObject,hasFormData;
				 
				form 		 = options.form;
				datas		 = form.serializeArray();
				files   	 = form.find("input[type='file']");
				hasFormData	 = ("FormData" in window);
				uploadObject = {};
			for(var i=0;i<datas.length;i++){
				uploadObject[datas[i].name]={type:"text",value:datas[i].value};
			}
	 		for(var i=0;i<files.length;i++){
	 			if(hasFormData)
	 			{
	 				var f=$(files[i]);
	 				uploadObject[f.attr("name")]={type:"file",value:f[0].files[0]};	
	 			}
	 			else
	 			{
	 				uploadObject[$(files[i]).attr("name")]={type:"file",value:$(files[i]),parentCon:$(files[i]).parent()};		
	 				$(files[i]).detach();
	 			}
	 		}
	 		if(hasFormData)
	 		{
	 			me.upload(uploadObject);
	 		}
	 		else
	 		{
	 			me.uploadIframe(uploadObject);	
	 		}
		}
		me.readData();
	}
};

app.control.datetimeSelector=function(elm){
		var 
			selMonth = elm.find(".select-month"),
			selDay   = elm.find(".select-day");
		var allowedDay={
			"07":[14,31],
			"08":[1,31],
			"09":[1,30]
		};
		selMonth.change(function(e){
			var days   = allowedDay[$(e.target).val()],
			    start  = days[0],
			    end    = days[1],
			    dayVal = selDay.val();
			selDay.children().remove();
			var selectDayOpt=$("<option>").attr({value:""}).text("--");
			var existingValue=false;
			selectDayOpt.appendTo(selDay);
			for(var i=start;i<=end;i++){
				var val=i.padLeft(2,"0");

				var opt=$("<option>").attr({value:val}).text(val);
				if(dayVal == val){
					opt.attr({selected:"selected"});
				}
				opt.appendTo(selDay);
			}

		});
};

 

$.fn.ssformUpload=function(options){
	 options.form=this;
	 options.url=this.attr("action");
	 var upload=new app.formUpload.uploader(options);
};
app.touchUtil=function(elm,setting){
	var 
        //
        hasTouch        = Modernizr.touch,
        touchstart      = "mousedown",
        touchend        = "mouseup",
        touchmove   	= "mousemove",
        startEvt		= null,
        lastMoveEvt 	= null,		
        root			= null,
        swipeDistance   = 50,
        moveDistance    = ((setting && setting.moveDistance) ? setting.moveDistance : 15),
        formatEvent = function(e){
        	if(hasTouch)
        	{	
        		if(e.originalEvent){
        			e=e.originalEvent;
        		}
        		if(e.touches && e.touches.length>0)
        		{	 
        			var touch = e.touches[0];
            		e.clientY = touch.pageY;
            		e.clientX = touch.pageX;
            	}
            	else if(lastMoveEvt)
            	{
            		e.clientX = lastMoveEvt.clientX;
            		e.clientY = lastMoveEvt.clientY;
            	}
            	else
            	{
            		e.clientX=0;
            		e.clientY=0;
            	}
        	}
            return e;
        },
        onTouchCancel  = function (e){
            console.log("onTouchCancel:"+e.touches)
        },

        onTouchLeave = function(e){
            console.log("onTouchLeave:"+e.touches);
        },
        /*
        */
        onTouchStart    = function (e) {
        	e=formatEvent(e);
        	startEvt=e;
            startEvt.startTime=new Date();
            lastMoveEvt=null;
            console.log("onTouchStart");
            $(document.body).addClass("on-drag");
            if(setting.onStartOnly==undefined)
            {
                elm.on(touchend, onTouchEnd);
            //elm.on("touchcancel",onTouchCancel);
            //elm.on("touchleave",onTouchLeave);
                elm.on(touchmove, onTouchMove);
            }
            root.trigger({type:"ontouchstart",e:startEvt});  
             if(setting){
                if(setting.preventDefault)
                {
                    e.preventDefault();
                }
                if(setting.stopPropagation){
                    e.stopPropagation();
                }
            } 
        },

        onTouchEnd		= function (e){
           $(document.body).removeClass("on-drag");
            elm.off(touchend, onTouchEnd);
            elm.off(touchmove, onTouchMove);
            console.log("onTouchEnd");
            var endTime=new Date();
            if(setting){
                if(setting.preventDefault)
                {
                    e.preventDefault();
                }
                if(setting.stopPropagation){
                    e.stopPropagation();
                }
            }
            
            e=formatEvent(e);
            var startX      = startEvt.clientX,
                startY      = startEvt.clientY,
                endX        = e.clientX,
                endY        = e.clientY,
                showIndex   = 0;
                diffX       = startX - endX,
                diffY       = startY-endY,
                data        ={
                    info:{
                        startY: startY,
                        startX: startX,
                        diffX : diffX,
                        diffY : diffY,
                        speedX :Math.abs(diffX)/((endTime.valueOf()-startEvt.startTime.valueOf())/1000),
                        speedY :Math.abs(diffY)/((endTime.valueOf()-startEvt.startTime.valueOf())/1000)
                    },
                    type  : "ontouchend"
                };
            
            if(Math.abs(diffX) >  Math.abs(diffY))
            {  
                 if(diffX>swipeDistance){
                 	data.type="swipeleft";
                    data.info.dir="left";
                    root.trigger(data);
                 }
                 else if(diffX<-swipeDistance)
                 {
                    data.info.dir="right";
                    data.type="swiperight";
                    root.trigger(data);  
                 }
                 
            }
            else
            {
                if(diffY>swipeDistance){
                    data.info.dir="down";
                    data.type="swipedown";
                    root.trigger(data);   
                }
                else if(diffY<-swipeDistance){
                    data.info.dir="up";
                    data.type="swipeup";
                    root.trigger(data);     
                }
                
            }
                //console.log(diffX+"     "+diffY);
            
         	startEvt = null;
            lastMoveEvt=null;
         	root.trigger({type:"ontouchend",info:data.info});   
        },

        onTouchMove   = function (e){
            console.log("onTouchMove:"+e.touches);
            if(setting){
                if(setting.preventDefault)
                {
                    e.preventDefault();
                }
                if(setting.stopPropagation){
                    e.stopPropagation();
                }
            }
            if(startEvt)
            {

                e=formatEvent(e);
                if(e.clientX && e.clientY)
                {
                	lastMoveEvt = e;
                    //console.log("startEvt:"+startEvt.clientX+" "+startEvt.clientY)
                    var startX      = startEvt.clientX,
                        startY      = startEvt.clientY,
                        endX        = e.clientX,
                        endY        = e.clientY,
                        showIndex   = 0;
                        diffX       = startX - endX,
                        diffY       = startY-endY,
                        data        ={
                            info:{
                                startY: startY,
                                startX: startX,
                                diffX : diffX,
                                diffY : diffY
                            },
                            type  : "ontouchmove"
                        };
                     //console.log(diffX);
                    if(!isNaN(diffX))
                    {
	                    if(Math.abs(diffX) >  Math.abs(diffY))
	                    {  
	                         if(diffX>moveDistance){
                                e.stopPropagation();
                                e.preventDefault();
	                            data.info.dir="left";
	                            data.type="moveleft";
	                            root.trigger(data);
	                         }
	                         else if(diffX<-moveDistance)
	                         {
	                            data.info.dir="right";
	                            data.type="moveright";
	                            root.trigger(data);  
	                         }
                             e.stopPropagation();
                             e.preventDefault();
	                    }
	                    else
	                    {
	                        if(diffY>moveDistance){
	                            data.info.dir="down";
	                            data.type="movedown";
	                            root.trigger(data);   
	                        }
	                        else if(diffY<-moveDistance){
	                            data.info.dir="up";
	                            data.type="moveup";
	                            root.trigger(data);     
	                        }
	                    }
                	}
                	else{
                		var k=0;
                	}
            	}
                //console.log(diffX+"     "+diffY);
            };
        	 
        },
        /*
        */
        addTouchEvent   = function (target) {
        	root=$(target);
            target.on(touchstart, onTouchStart);            
            $(document).on("dragstart",function(e){
                e.preventDefault();
                return false;
            });
        };
    if (hasTouch) {
        touchstart = "touchstart";
        touchend   = "touchend";
        touchmove  = "touchmove";
    };
	addTouchEvent(elm);
}
app.slidingNav=function(){
  	 var 
	 		slides=[
	 			{title:'home',view:'home',route:'home',template:"slides/_home.php",versaTag:"http://www.samsung.com.au/samsungslideliner"},
	 			{title:'Samsung slideliner',view:'slideliner',route:'slideliner',template:"slides/_slideliner.html",versaTag:"http://www.samsung.com.au/samsungslideliner/whatsslideliner"},
	 			{title:'Samsung SMART TVs',view:'smarttv',route:'smarttv',template:"slides/_smarttv.html",versaTag:"http://www.samsung.com.au/samsungslideliner/tvrange"},
	 			//{title:'Prizes',view:'prizes',route:'prizes',template:"slides/_prizes.html",versaTag:"http://www.samsung.com.au/samsungslideliner/prizes"},
	 			//{title:'Enter now',view:'winseat',route:'winseat',template:"slides/_winseat.html",versaTag:"http://www.samsung.com.au/samsungslideliner/enternow"},
	 			//{title:'Game schedule',view:'game',route:'game',template:"slides/_game.html",versaTag:"http://www.samsung.com.au/samsungslideliner/schedule"},
	 			{
	 				title:'Gallery',
	 				view:'gallery',
	 				route:'gallery',
	 				template:"slides/_gallery.html",
	 				versaTag:"http://www.samsung.com.au/samsungslideliner/gallery"
	 			},
	 			{title:'Social',view:'social',route:'social',template:"slides/_social.html",versaTag:"http://www.samsung.com.au/samsungslideliner/social"}
	 		],
	 		slideContainer   =		$(".main-content"),
	 		navContainer	 =		$(".navbar ul"),
	 		footer			 = 	    $(".footer"),
	 		root 			 =      $(document.body),
	 		me 				 =		this,
	 		isMobile		 =      function(){return $(".navigation-small").css("display").toLowerCase()!=="none";},
	 		toucher			 = 		null,
	 		isHashChangeFromCode = false;
	 		current 		 =      {
	 			 slideContainer:{
	 			 	currentSlideIndex:0,
	 			 	translate:0,
	 			 	isSliding:false
	 			 }
	 		};
	this.getTransformCSS=function(x,y,second){
		var css= {
				'-webkit-transform':'translate('+x+','+y+')',
				'transform':'translate('+x+','+y+')'
			}
		if(second){
			css['-webkit-transition']='-webkit-transform '+second+'ms ease-out';
			css['transition']='transform '+second+'ms ease-out';
			css['-ms-transition']='transform '+second+'ms ease-out';
		}
		else{
			css['-webkit-transition']='none';
			css['transition']='none';
		}
		if(isIE8){
			css={
				left:x
			}
		}
		return css;
	};
	this.loadSlide=function(slide,callback){
		//var loader=$("<div>").addClass("loader");
		//loader.appendTo(slide.ui.div);
		 
		slide.ui.div.load(slide.template,function(e){
			//loader.remove();
			slide.loaded=true;
			slide.ui.div.removeClass("loader").removeClass("center-no-width");
			if(callback){
				callback(slide);
			}
		});
		
	}
	this.toggleSwipe=function(allowSwipe){
		if(allowSwipe)
		{
			slideContainer.on('swipeleft',me.onTouchEnd);
			slideContainer.on('swiperight',me.onTouchEnd);
			slideContainer.on("moveleft",me.onTouchMove);
			slideContainer.on("moveright",me.onTouchMove);
		}
		else
		{
			slideContainer.off('swipeleft',me.onTouchEnd);
			slideContainer.off('swiperight',me.onTouchEnd);	
			slideContainer.off("moveleft",me.onTouchMove);
			slideContainer.off("moveright",me.onTouchMove);
		}
	};
	this.goToSlideByRoute= function(route){
		for(var i=0;i<slides.length;i++){
			if(slides[i].route===route){
				if(i!==current.slideContainer.currentSlideIndex){
					me.goToSlide(slides[i]);
				}
				break;
			}
		}
	};
	this.goToSlide=function(slide){
		if(!current.slideContainer.isMobile)
		{
			//console.log("goToSlide:"+slide.index);
			//me.toggleNavBar(false);
			var x=slide.index*-100;
			var timing=800;
			var cIndex=current.slideContainer.currentSlideIndex;
			console.log("goToSlide:"+slide.index);
			current.slideContainer.isSliding=true;
			$(document.body).removeClass(slides[current.slideContainer.currentSlideIndex].route);
			$(document.body).addClass(slide.route);
			slide.ui.div.removeClass("exiting").addClass("entering");
			slides[current.slideContainer.currentSlideIndex].ui.div.addClass("exiting");
			var currentView=slides[current.slideContainer.currentSlideIndex].view;
			if(typeof(window[currentView+"Slide"]) !== "undefined"){
				if(window[currentView+"Slide"].onExitStart)
				{

					window[currentView+"Slide"].onExitStart(slide);
				}
			};
			$(document.body).addClass("sliding");
			slideContainer.addClass("sliding-working").css(me.getTransformCSS(x+"%",0,timing));
			console.log("goToSlide:"+slide.index+" 	translate:"+x);
			navContainer.find("li.active").removeClass("active");
			current.slideContainer.translate=x;
			current.slideContainer.currentSlideIndex=slide.index;
			
			var onSlideLoaded = function(slide){
				if(typeof(window[slide.view+"Slide"]) !== "undefined"){
					window[slide.view+"Slide"].onEnter(slide);
				};
			}
			setTimeout(function(){
				$(document.body).removeClass("sliding");
				current.slideContainer.isSliding=false;
				onSlideLoaded(slide);
				if(slide.index>0){
					
					var previousSlide=null;
					previousSlide=slides[cIndex];					 
					
					previousSlide.ui.div.removeClass("exiting");
					if(typeof(window[previousSlide.view+"Slide"]) !== "undefined"){
						window[previousSlide.view+"Slide"].onExit(previousSlide);
					}
				}
				slide.ui.div.removeClass("entering");
				slideContainer.removeClass("sliding-working");
			},timing+400);
			if(!slide.loaded){
				me.loadSlide(slide,onSlideLoaded);
			}
			//window.location.hash=slide.route;
			window.parent.postMessage(JSON.stringify({t:{type:"hashchange",hash:slide.route}}),"*");
			isHashChangeFromCode=true;
			slide.ui.li.addClass("active");
			setTimeout(function(){
				if(isMobile())
				{
					me.toggleNavBar(false);
				}
			},0);
		}
	};
	this.toggleNavBar=function(show){
		if(show)
		{
			$(".navbar-outer").removeClass("out");
			$(".navigation-small").addClass("out");
			footer.removeClass("out");
		}
		else
		{
			$(".navbar-outer").addClass("out");		
			$(".navigation-small").removeClass("out");
			footer.addClass("out");
		}
	};
	this.getCurrentSlide=function(){
		return slides[current.slideContainer.currentSlideIndex];
	};
	this.onTouchEnd  = function(e){
		console.log($(e.target).attr("class"));
		if($(e.target).hasClass("frames")){
			return;
		}
		if($(e.target).hasClass("track-outer")){
			return;
		}
		if(e.info && $(".modal-backdrop").length==0)
		{

			//console.log("sliding onTouchEnd:"+e.info);
			var slideIndex=current.slideContainer.currentSlideIndex;
			var isLeftAndRight=false;
			var minMove=$(window).width()*0.3;
			if(minMove>300){
				minMove=300;
			}
			if(Math.abs(e.info.diffX)>minMove)
			{
				switch(e.info.dir){
					case "left":
						slideIndex+=1;
						isLeftAndRight=true;
						break;
					case "right":
						slideIndex--;
						isLeftAndRight=true;
						break;
				}
			}
			console.log("sliding onTouchEnd:"+e.info.diffX+"	"+minMove+"		"+slideIndex+"		"+current.slideContainer.currentSlideIndex+"	"+isLeftAndRight);
			//e.preventDefault();
			/*if(slideIndex==current.slideContainer.currentSlideIndex){
				//return;
			}*/
			if(slideIndex>=0 && slideIndex<slides.length){
				me.goToSlide(slides[slideIndex]);
				//window.location.hash=slides[slideIndex].route;
				console.log("touchend:goToSlide:"+slideIndex);
			}
			else{
				console.log("touchend:goToSlide:"+current.slideContainer.currentSlideIndex);
				//window.location.hash=slides[current.slideContainer.currentSlideIndex].route;
				me.goToSlide(slides[current.slideContainer.currentSlideIndex]);	
			}
		}
		 
	}; 
	this.onTouchMove = function(e){
		if($(e.target).hasClass("track-outer")){
			return;
		}
		if(e.info)
		{
			console.log("sliding:onTouchMove"+e.info);
			if(e.info.dir=="left" || e.info.dir=="right")
			{ 
				slideContainer.addClass("sliding-working")
				var mvx=e.info.diffX;
				var percent=(mvx/($(window).width()))*100;
				var x=current.slideContainer.translate-percent;
				//current.slideContainer.translate=x;
				slideContainer.css(me.getTransformCSS(x+"%",0,null));
			}
		}
	};
	this.start=function ()  {
		$(slides).each(function(i,slide){
		  	var 
		  	  	  li 			 =		$("<li>"),
		  	  	  a  			 =		$("<a>").attr({"href":"javascript:void(0)"}).html(slide.title),
		  	  	  div			 =		$("<div>").addClass("slide").attr({'viewUrl':slide.template}).addClass(slide.view),
		  	  	  x				 =       100*(i);
		  	 
		  	 a.css({paddingRight:i*(2+i/5)+35});
		  	 
		  	 a.click(function(e){
		  	 	if(isOkToLive)
		  	 	{
			  	 	var prop5 = "au:campaign:seau:samsungslideliner:"+slide.route;
			  	 	var hier1  = "au>campaign>seau>samsungslideliner>"+slide.route;
			  	 	if(window.s_gi)
			  	 	{
				  	 	var s=s_gi(s_account);
				  	 	s.pageName="au:campaign:seau:samsungslideliner:"+slide.route;
				  	 	s.prop5 = prop5;
				  	 	s.hier1=hier1;
				  	 	s.linkTrackVars='eVar33,events';
						s.linkTrackEvents='event45';
						s.eVar33="au:campaign:seau:samsungslideliner:"+slide.route;
						s.events='event45';
						s.tl(this,"o",s.eVar33);
					}
					_gaq.push(['_trackEvent', "Menu", 'Click', slide.title]);
			  	 	if(slide.versaTag){
						window.sendVersaTag(slide.versaTag,true);
					}
					me.goToSlide(slide);
					document.location.hash=slide.route;
		  	 	}
		  	 });
		  	 div.addClass("loader center-no-width").css(me.getTransformCSS((x)+'%', 0));
		  	 a.appendTo(li);
		  	 li.appendTo(navContainer);
		  	 div.appendTo(slideContainer);
		  	 slide.index=i;
		  	 slide.loaded=false;
		  	 slide.ui={
		  	 	li:li,
		  	 	a:a,
		  	 	div:div
		  	 }
		});
		var hashSlide=window.location.hash;
		if(hashSlide){
			hashSlide=hashSlide.split("/?")[0];
			console.log(hashSlide);
			hashSlide=hashSlide.replace("#","");
			var i=0,isCorrectHash=false;
			for(var j=0;j<slides.length;j++){
				if(hashSlide===slides[j].route){	
					isCorrectHash=true;
					i=j;
					break;
				}
			}
			if(isCorrectHash){
				//slides[i].ui.a.click();
				me.goToSlide(slides[i]);
			}
			else
			{
				//slides[0].ui.a.click();
				me.goToSlide(slides[0]);
			}
			
		}
		else
		{
			//slides[0].ui.a.click();
			me.goToSlide(slides[0]);	
		}
		if(!isMobile())
		{
			me.toggleNavBar(true);
		}
		//slideContainer.bind("touchend",me.onTouchEnd);
		 
		//toucher=new app.touchUtil(slideContainer,{moveDistance:20});
		me.toggleSwipe(true);
		var allTryTimes=0;
		slideContainer.on('_xxxxxmousewheel', function(event) {
			 
			console.log("mousewheel:slideContainer");
			if(!isMobile())
			{
				console.log("event.deltaY="+event.deltaY);				
				var slideIndex=current.slideContainer.currentSlideIndex+(event.deltaY>0 ? -1 : 1);
				var currentSlideIndexItem=slides[current.slideContainer.currentSlideIndex];
				var elm=currentSlideIndexItem.ui.div.find(".content")[0];

				if(elm.scrollTop == 0 || (elm.scrollTop == (elm.scrollHeight - elm.offsetHeight))) { 
					if(slideIndex>=0 && slideIndex<slides.length)
					{
						allTryTimes++;
						var canContinue=true;
						elm=$(elm);
						//if(elm.HasScrollBar()){
						if(true){
							var dir=(event.deltaY>0 ? "up" :"down");
							if(dir=="up"){
								elm.removeAttr("scrolltrydown");
								allTryTimes=0;		
							}
							else
							{
								elm.removeAttr("scrolltryup");
								allTryTimes=0;
							}
							var attrName="scrolltry"+dir;
							var tryTimes=elm.attr(attrName);
							if(tryTimes && parseInt(tryTimes)==20){
								canContinue=true;
							}
							else
							{
								canContinue=false;
								var tt={};
								if(tryTimes){
									tt[attrName]=parseInt(tryTimes)+1;
									elm.attr(tt);					
								}
								else
								{
									tt[attrName]=1;
									elm.attr(tt);	
								}
							}
						}
						if(allTryTimes>5){
							canContinue=true;
						}
						if(canContinue &&  !current.slideContainer.isSliding)
						{
							allTryTimes=0;
							elm.removeAttr("scrolltryup").removeAttr("scrolltrydown");
	    					me.goToSlide(slides[slideIndex]);
	    				}
	    			}
				}
			}
			
		});
		$(window).on("resize",function(e){
			if(isMobile()){
				me.toggleNavBar(false);
			}
			else
			{
				me.toggleNavBar(true);
			}
		}).on("hashchange",function(e){
			me.goToSlideByRoute(window.location.hash.replace("#",""));
		});

		$(".navigation-small .mobile-button-container").click(function(e){
			$(".navbar-outer, .navigation-small").toggleClass("out");
		});
	}
};

app.validation={
	preconfig:{
		firstname:{rules:{required:true},messages:{required:'Please enter your first name'}},
		surname:{rules:{required:true},messages:{required:'Please enter your surname'}},
		serialnumber:{rules:{required:true,isSerialNumber:true},
			messages:{
				required:'Please enter product serial number',
				isSerialNumber:"Serial number must be a 14/15 character alphanumeric code"
			}
		},
		phonenumber:{rules:{required:true,digits:true,phoneCheck:true},messages:{required:'Please enter your phone number',digits:"Please enter numbers only",phoneCheck:"Please enter valid Australian phone number"}},
		email:{rules:{required:true,isEmail:true},messages:{required:'Please enter your email',isEmail:"Invalid email address"}},
		emailIfAny:{rules:{required:false, isEmailIfAny:true},messages:{isEmailIfAny:"Invalid email address"}},
		address:{rules:{required:true},messages:{required:'Please enter address'}},
		suburb:{rules:{required:true},messages:{required:'Please enter suburb'}},
		postcode:{rules:{required:true,digits:true,isAuPostcode:true},messages:{required:'Please enter postcode',digits:"Please enter numbers only",isAuPostcode:"Please enter valid Australian postcode"}},
		state:{rules:{required:true},messages:{required:'Please select state'}},
		receiptnumber:{rules:{required:true},messages:{required:'Please enter receipt number'}},
		productpurchased:{rules:{required:true},messages:{required:'Please select product purchased'}},
		fileinput:{rules:{required:true},messages:{required:'Please attach your receipt'}},
		selectmonth:{rules:{required:true},messages:{required:'Please select month'}},
		selectday:{rules:{required:true},messages:{required:'Please select day'}},
		selectyear:{rules:{required:true},messages:{required:'Please select year'}},
		comment:{rules:{required:true},messages:{required:'Please enter your experience'}},
		termcondition:{rules:{required:true},messages:{required:'Please agree with our terms and conditions'}}
	},
	utility:{
		isEmail:function(email){
			var pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
					// pattern=new
					// RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
			pattern=/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;
			return pattern.test(email);
		},
		isHomePhoneNumber: function (value) {
	        var patterns = [/^(\+?61|0)\d{9}$/, /^\D*0(\D*\d){9}\D*$/, /^\d{8}$/];
	        var result = false;
	        while (!result && patterns.length > 0) {
	            var p = (patterns[0]);
	            result = p.test(value.replace(/\s+/g, ""));
	            patterns.splice(0, 1);
	        }
	        return result;
	    },
	    isMobileNumber: function (value) {
	        var pattern = /^(\+?61|0)4\d{8}$/;
	        return pattern.test(value.replace(/\s+/g, ""));
	    },
	    isPhoneNumber: function (value) {
	        if (this.isHomePhoneNumber(value)) {
	            return true;
	        }
	        if (this.isMobileNumber(value)) {
	            return true;
	        }
	        return false;
	    },
	    isAuPostcode: function(value){
	    	var pattern=/^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$/;
	    	return pattern.test(value.replace(/\s+/g, ""));
	    },
	    isAlphaNumeric:function(value){
	    	var pattern=/^[a-zA-Z0-9]+$/;
	    	return pattern.test(value.replace(/\s+/g, ""));	
	    }
	},
	commonSetting:{
			getParentUI:function(element){
				element=$(element);
				var ui=null;
				 
				switch($(element).prop("tagName").toLowerCase()){
					case "select":
						ui=element.parent();					 
						break;
					case "input":
						if(
							element.parent().hasClass("email-wrapper") 
							|| 
							element.parent().hasClass("datetimepicker-wrapper")
						  )
						{
							ui=element.parent();
						}

						break;

				}
				return ui;
			},
			success:function(label,element){
				label.remove();
			},
			highlight:function(element, errorClass, validClass) {
				element=$(element);
				var ui=element;
	 			var parentUI=app.validation.commonSetting.getParentUI(element);
				if(parentUI){
					ui=parentUI;
				}
				ui.addClass(errorClass);
			},
			unhighlight:function(element, errorClass, validClass) {
				element=$(element);
				var ui=element;
				var parentUI=app.validation.commonSetting.getParentUI(element);
				if(parentUI){
					ui=parentUI;
				}
				ui.removeClass(errorClass);
			},
			errorPlacement:function(error,element){
				var ui=element;
				var parentUI=app.validation.commonSetting.getParentUI(element);
				if(parentUI){
					ui=parentUI;
				}
				error.insertAfter(ui);
			}
	},
	getSetting:function(root,formID){
			var setting={
				rules:{},
				messages:{}
			}
			var formID=root.attr("formID");			 
			var nameStart="fieldValidation_";
			if(formID){
				nameStart=formID+"_";
			}
			root.find("[validation]").each(function(i,control){
				var c=$(control);
				var ruleDEF=c.attr("validation");
				var name=c.attr("name");
				if(name===null || name===undefined){
					name=nameStart+i;
					c.attr({name:name});
				}
				var depends=c.attr("depends");
				var equalTo=c.attr("validationEqualTo");
				switch(ruleDEF){
					case "companyName":
						setting.rules[name]={
								required:function(element){return root.find("#chkOnBehalfBusiness")[0].checked;}
						};
						break;
					default:
						setting.rules[name]=$.extend({},app.validation.preconfig[ruleDEF].rules);
						if(depends){
							for(var f in setting.rules[name]){
								setting.rules[name][f]={
									depends:function(element){
										return !$(depends)[0].checked;
									}
								}	
							}
							
						}
						if(equalTo){
							setting.rules[name].equalTo=root.find("[name='"+equalTo+"']")[0];
						}
						break;	
				}

				setting.messages[name]=app.validation.preconfig[ruleDEF].messages;
			});
			var ignore=[];
			root.find("[validationIngore='true']").each(function(i,item){
				var name=$(item).attr("name");
				ignore.push(item);
			});
			if(ignore.length>0){
				setting.ignore=ignore;
			}
			return setting;
	},
	validate:function(form,errorPlacement){
		if(form.length>0){
			var setting=app.validation.getSetting(form);
			$.extend(setting,app.validation.commonSetting);
			if(errorPlacement){
				setting.errorPlacement=errorPlacement;
			}
			else
			{
				setting.errorPlacement=function(error,element){

					var name=$(element).attr("name");
					var ui=form.find("[errorcontainerfor='"+name+"']");
					if(ui.length>0){
						error.appendTo(ui);
						return;
					}
					ui=form.find("div[for='error-container']");
					if(ui.length>0){
						error.appendTo(ui);
						return;
					}			
					var parentUI=app.validation.commonSetting.getParentUI(element);
					if(parentUI){
						error.insertAfter(parentUI);
						return;
					}
					error.insertAfter(element);
				}
			}
			form.validate(setting);	
		}
	},
	start:function(){
		$("form[formvalidation='common']").each(
			function(i,form){
				var validation=new app.validation.validate($(form));
			}
		);
	}
}
$(document).ready(function(e){
	$.validator.addMethod('isEmail', function (value, element) {
	  	return app.validation.utility.isEmail(value);
	}, '');

	$.validator.addMethod("phoneCheck", function (value, element) {
        var valid;
        valid = app.validation.utility.isPhoneNumber(value);
        return valid;
    }, "");

	$.validator.addMethod("isAuPostcode", function (value, element) {
        var valid;
        valid = app.validation.utility.isAuPostcode(value);
        return valid;
    }, "");
    
    $.validator.addMethod("isAlphaNumeric", function (value, element) {
        var valid;
        valid = app.validation.utility.isAlphaNumeric(value);
        return valid;
    }, "");

     $.validator.addMethod("isSerialNumber", function (value, element) {
        var valid=false;
        if(value.length==14 || value.length==15)
        {
        	valid = app.validation.utility.isAlphaNumeric(value);	
        }
        return valid;
    }, "");
	
	app.validation.start();
});
app.gallery={
	videoPlayer:null,
	watchInterval:null,
	videoGetPlayStateInterval:null,
	viewDetail:function(gallery){
		if(gallery.type=="video"){
			gallery.items=app.gallerydata.video;
		}
		var message=$(".template-gallery-view").val();
		app.ui.modal({
		className:"gallery-player gallery-"+gallery.id,
		message:message,
		modalSize:'modal-lg',
		title:" ",
		onHidden:function(){
			$("html,body").css({overflow:""});
			if(app.gallery.videoPlayer)
			{
				app.gallery.videoPlayer.destroy();
			}
			app.gallery.videoPlayer=null;
		}, 
		onShown:function(ui){
			$("html,body").css({overflow:"hidden"});
			if(gallery.title){
				ui.find(".ablum-title-container").html(gallery.title).show();
			}
			if(gallery.location){
				ui.find(".ablum-location-container").html(gallery.location).show();	
			};
			ui.addClass(gallery.type);
			var 
				thumCarousel,
				mainCarousel,
				showItemInfo=function(item){
					if(item.title){
						ui.find(".main-info .title-container").html(item.title).show();
					}
					if(item.description && item.description!=="null"){
						ui.find(".main-info .description-container").html(item.description).show();	
					}
					$(".gallery-view .thumbnails .owl-item.active").removeClass("active");
					item.ui.div.parent().addClass("active");
					if(mainCarousel)
					{
						thumCarousel.goTo(mainCarousel.currentItem);
					}
					ui.find(".play-item-index").html((gallery.type == "video" ? "video" : "photo") +" "+(item.index+1)+" of "+gallery.items.length);
				},
				onItemClick=function(item){
					if(item.type=="video")
					{
						app.gallery.playerVideo(item);
						showItemInfo(item);
						if(item.title){
							ui.find(".ablum-title-container").html(item.title).show();
						}
						if(item.location){
							ui.find(".ablum-location-container").html(item.location).show();	
						};
					}
					else
					{
						mainCarousel.goTo(item.index);
					}
				};
			var thumbSetting={
				   items : 5, //10 items above 1000px browser width
			       itemsDesktop : [1000,4], //5 items between 1000px and 901px
			       itemsDesktopSmall : [900,4], // betweem 900px and 601px
			       itemsTablet: [600,3], //2 items between 600 and 0
			       itemsMobile : [400,2],
				   navigation : true,
				   pagination:false,
				   navigationText:['&nbsp;','&nbsp;']
			};
			thumCarousel=app.gallery.loadInGallery(gallery.items,".gallery-view .thumbnails .carousel",onItemClick,thumbSetting);
			mainCarousel=null;
			var twittershareurl,facebookshareurl;
			if(gallery.type==="album")
			{
				twittershareurl  = "window.open(app.twitter_share_url,'','width=500,height=580')";
				facebookshareurl = "window.open(app.facebook_share_url,'','width=500,height=580')";
				mainCarousel=app.gallery.loadMainGallery(gallery.items,".gallery-view .main-container .carousel");
			}
			else
			{
				var faceShare="https://www.facebook.com/sharer.php?app_id=257838867740570&u=http://youtu.be/"+gallery.videoUrl+"&display=popup";
				var twittersharer ="https://twitter.com/intent/tweet?original_referer=http://www.samsung.com.au/samsungslideliner&text=It%E2%80%99s%20on%20for%20you%20%26%203%20mates%20-%20win%20seats%20on%20the%20Samsung%20%23SlideLiner%20plus%20a%20VIP%20day%20with%20Nathan%20Sharpe%20%26%20more.&tw_p=tweetbutton&url=http://bit.ly/1qMiv8T";
				facebookshareurl   = "window.open('"+faceShare+"','','width=500,height=580')";
				twittershareVideo = twittersharer.split("&");
				twittershareVideo[twittershareVideo.length-1]="url=http://youtu.be/"+gallery.videoUrl;
				twittershareurl  = "window.open('"+twittershareVideo.join('&')+"','','width=500,height=580')";
				app.gallery.playerVideo(gallery);
				showItemInfo(gallery);
			}
			ui.find(".share-icon.facebook a").attr({onclick:facebookshareurl});
			ui.find(".share-icon.twitter a").attr({onclick:twittershareurl});
			clearInterval(app.gallery.watchInterval);
			var currentItem;
			if(mainCarousel)
			{
				app.gallery.watchInterval=setInterval(function(){
					if(mainCarousel)
					{
						if(currentItem!=mainCarousel.currentItem)
						{
							currentItem=mainCarousel.currentItem;
							showItemInfo(gallery.items[mainCarousel.currentItem]);
						}
					}
				},200);
			}
			 
		}});
	},
	playerVideo:function(sender,ui){
		if(app.gallery.videoPlayer==null){
			if(ui){
				ui=ui[0];
			}
			else
			{
				ui=$(".gallery-view .main-container .carousel")[0];
			}
			var playerState = null;
			var onStateChange = function(e){
				console.log('State is:', e.data);
			}
			app.gallery.videoPlayer = new YT.Player(ui, {
		          height: '100%',
		          width: '100%',
		          videoId: sender.videoUrl,
		          playerVars: { 'autoplay': 0 },
		          events: {
		           	onReady: function(e){
		          	},
		          	onStateChange:function(e){
		          		if(e.data == YT.PlayerState.ENDED){
		          			if(sender.playlist){
		          				var next = sender.getNext(sender.videoUrl);
		          				if(next){
		          					sender.videoUrl=next;
		          					app.gallery.playerVideo(sender,ui);
		          				}
		          			}
		          		}
		          	}
		          }
	        });
		}
		else{
			app.gallery.videoPlayer.loadVideoById(sender.videoUrl);
		}
	},
	loadMainGallery:function(items,selector){
		var mainViewSetting={
		    navigation : true, // Show next and prev buttons
		    slideSpeed : 300,
		    lazyLoad : true,
		    paginationSpeed : 400,
		    singleItem:true,
		    navigationText:false,
		    pagination:false
		};
		var owlContainer=$(selector);
		$(items).each(function(i,item){
			var mdiv=$("<div>").addClass("item");
			var mimg,responsiveDiv,responsiveDivInner;
			item.index=i;
			if(item.type && item.type=="video"){
				/*
				var template=$($(".template.gallery-video-template").val());
				responsiveDiv=template;
				responsiveDivInner=responsiveDiv.find(".inner");
				mimg=responsiveDiv.find("img");
				mimg.addClass("lazyOwl").attr({'data-src':item.url});
				var btnPlay=template.find(".play-button");
				btnPlay.attr({videoid:item.id});
				item.ui=template.find(".video-play");
				btnPlay.click(function(e){
					galleryPlayVideo(item);
				});*/
			}
			else
			{
 				responsiveDiv=$("<div>").addClass("image-container");
				responsiveDivInner=$("<div>").addClass("inner");
				responsiveDiv.append(responsiveDivInner);
				mimg=$("<img>").addClass("lazyOwl").attr({'data-src':item.url});
				mimg.appendTo(responsiveDivInner);
				responsiveDiv.appendTo(mdiv);

			}
			mdiv.appendTo(owlContainer);
		});
		return owlContainer.owlCarousel(mainViewSetting).data('owlCarousel');	
	},

	loadInGallery:function(items,selector,onItemClick,thumbSettingCUST){
		var thumbSetting={
			   items : 4, //10 items above 1000px browser width
		       itemsDesktop : [1200,3], //5 items between 1000px and 901px
		       itemsDesktopSmall : [950,3], // betweem 900px and 601px
		       itemsTablet: [600,3], //2 items between 600 and 0
		       itemsMobile : [400,2],
			   navigation : true,
			   pagination:false,
			   navigationText:['&nbsp;','&nbsp;']
		};
		if(thumbSettingCUST){
			thumbSetting=thumbSettingCUST;
		}
		var owlContainer=$(selector);
		var templates=$(".template-gallery-thumb").val();
		$(items).each(function(i,item){
			var div=$(templates);
			div.addClass(item.type);
			div.attr({galleryitemid:item.id});
			var imgSrc=item.thumburl;
			if(item.type=="album"){
				imgSrc=item.cover;
			}
			item.index=i;
			var img=div.find("img").addClass("lazyOwl").load(function(e){
			}).attr({'src':imgSrc});
			div.appendTo(owlContainer);
			for(var f in item){
				var ui=div.find("."+f+"-container");
				if(ui.length>0){
					if(item[f] && item[f]!=="null")
					{
						ui.attr({title:item[f]});
						ui.html(item[f]);
					}
				}
			}
			div.find("a").attr({href:"javascript:void(0)"}).click(function(e){
				if(onItemClick){
					onItemClick(item);
				}
				else
				{
					app.gallery.viewDetail(item);
				}
			});
			item.ui={div:div};
			if(item.type=="album"){
				div.find(".counter").html(item.items.length);
			}
		});
		return owlContainer.owlCarousel(thumbSetting).data('owlCarousel');

	},
	installYouTube:function(){
		 /*if(typeof(YT)==="undefined"){
			 
			  window.onYouTubeIframeAPIReady = function(){
			  	console.log("onYouTubeIframeAPIReady");
			  };
			  var tag = document.createElement('script');
		      tag.src = "https//www.youtube.com/iframe_api";
		      var firstScriptTag = document.getElementsByTagName('script')[0];
		      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

		}*/
	},
	start:function (){
		var photos=app.gallerydata.photo;
		var photoObj=this.loadInGallery(photos,".content.gallery .photo-gallery .carousel");
		var videos=app.gallerydata.video;
		var videoObject=this.loadInGallery(videos,".content.gallery .video-gallery .carousel");
		this.installYouTube();
	}
}
app.gallerynew = {};
(function(g){
	g.api = null;
	g.setting ={
		container:null,
		template : null,
		data:null
	};
	g.getAllGalleryItems = function(){
		var allItems = [];
		$(app.gallerydata.photo).each(function(i,item){
			$(item.items).each(function(j,photo){
				photo.type = "photo";
				photo.location= item.location;
				allItems.push(photo);
			});
		});

		$(app.gallerydata.video).each(function(i,video){
				video.type = "video";
			 	allItems.push(video);
		});
		//allItems.sort(function() {return 0.5 - Math.random()});
		return allItems;
	},
	g.getAllGalleryItemInVideoAndPhoto = function(){
		var photoes=[];
		for(var i=app.gallerydata.photo.length-1;i>-1;i--){
			var p=app.gallerydata.photo[i];
			for(var j=p.items.length-1;j>-1;j--){
				var photo=p.items[j];
				photo.type = "photo";
				photo.thumburl=photo.thumburl.replace("/thumb/","/medium/");
				photo.location= p.location;
				photoes.push(photo);
			}
		};

		var videos=[]
		$(app.gallerydata.video).each(function(i,video){
				video.type = "video";
			 	videos.push(video);
		});	
		return {photo:photoes,video:videos};
	},
	g.galleryView= function(setting){
		var 
			loadedImage=[],
			loader=$('<div class="loader">'),
			mapi = null,
			startMasonry = function(){
				if(mapi == null)
				{
					setting.container.masonry({
						itemSelector: setting.itemSelector,
						columnWidth: setting.width ? setting.width : 320,
						isFitWidth:true
					});
					mapi = setting.container.data('masonry');
				}
			},
			checkAllLoaded = function (length,index,elems){
				loadedImage.push(index);
				console.log(length+"  "+index+"  "+loadedImage.length);
				if(loadedImage.length === length){
					//console.log("allLoaded");

					setting.container.removeClass("loading");
					loadedImage = [];
					if(mapi === null)
					{
						if(isIE8)
						{
							setTimeout(function(){startMasonry();},700);
						}
						else
						{
							setTimeout(function(){startMasonry();},200);
						}
					}
					else
					{
						setting.container.masonry('appended',elems);
					}
					loader.detach();
				}
			},
			loadMainGallery = function(items,selector){
		 		var mainViewSetting={
				    center: true,
				    items:2,
				    loop:true,
				    margin:10,
				    lazyLoad:true,
				    nav:true,
				    dots:false,
				    navText:["",""],
				    responsive:{
				        600:{
				            items:1
				        }
				    }
				};
				var owlContainer=$(selector);
				$(items).each(function(i,item){
					var mdiv=$("<div>").addClass("item");
					var mimg,responsiveDiv,responsiveDivInner;
					item.index=i;
	 				responsiveDiv=$("<div>").addClass("image-container");
					responsiveDivInner=$("<div>").addClass("inner");
					responsiveDiv.append(responsiveDivInner);
					mimg=$("<img>").addClass("owl-lazy").attr({'data-src':item.url});
					mimg.appendTo(responsiveDivInner);
					responsiveDiv.appendTo(mdiv);
					mdiv.appendTo(owlContainer);
				});
				return owlContainer.owlCarousel(mainViewSetting).data('owlCarousel');	
			},
			viewDetailSingle = function(gallery){
				var message=$(".template-gallery-view").val();
				app.ui.modal({
					className:"gallery-player gallery-"+gallery.id,
					message:message,
					modalSize:'modal-lg',
					title:" ",
					onHidden:function(){
						//$("html,body").css({overflow:""});
						if(app.gallery.videoPlayer)
						{
							app.gallery.videoPlayer.destroy();
						}
						app.gallery.videoPlayer=null;
					}, 
					onShown:function(ui){
						var container = ui.find(".carousel .image-container .inner");
						loader.detach().appendTo(container);
						ui.addClass(gallery.type);
						if(gallery.type=="video")
						{
							ui.find(".title-container").html(gallery.title).parent().show();
						}
						else
						{
							ui.find(".title-container").parent().hide();
						}
						if(gallery.location){
							ui.find(".location-container").html(gallery.location);
						}
						//$("html,body").css({overflow:"hidden"});
						addToSocialShare(ui,gallery);
						if(gallery.type=="photo"){
							$("<img>").load(function(e){
								loader.remove();
							}).error(function(e){
								loader.remove();
							}).attr({
								src:gallery.url
							}).appendTo(container);
						}
						else{
							$("<img>").load(function(e){
								loader.remove();
							}).error(function(e){
								loader.remove();
							}).attr({
								src:gallery.url,
								width:"100%"
							}).appendTo(container);
							var div=$("<div>").addClass("video-container").appendTo(container);
							app.gallery.playerVideo({videoUrl:gallery.videoUrl},div);
						}
					}
				});
			},
			addToSocialShare = function(ui,item){
				var urlToShare = siteUrl+"/fbshare.php?gallery="+item.thumburl;
				var shareOnTwitter="https://twitter.com/intent/tweet?url=[source_url]&text=Check%20out%20the%20Samsung%20SlideLiner%20in%20action&hashtags=SlideLiner";
				if(item.type=="video"){
					urlToShare="http://youtu.be/"+item.videoUrl;
				}
				var faceShare="https://www.facebook.com/sharer.php?app_id=257838867740570&u="+urlToShare+"&display=popup";
				var facebookshareurl   = "window.open('"+faceShare+"','','width=500,height=580')";
				shareOnTwitter = shareOnTwitter.replace("[source_url]",urlToShare);
				var twittershareurl = "window.open('"+shareOnTwitter+"','','width=500,height=580')";
				ui.find(".fb-share").attr({onclick:facebookshareurl});
				ui.find(".twitter-share").attr({onclick:twittershareurl});
			},
			populateGallery = function(data){
				var vp  = g.getAllGalleryItemInVideoAndPhoto();
				var rows = Math.ceil(data.length/5);
				var cols = 2;
				var count =0;
				for(var i=0;i<rows;i++){
					for(var j=0;j<cols;j++){
						var div=$("<div>").addClass("gallery-group-item waiting").appendTo(setting.container);
						$("<div>").addClass("gallery-group-item-inner").appendTo(div);
						 if(j==0)
						 {
							 if(count%4===0){
							 	div.addClass("single");
							 }
							 else
							 {
							 	div.addClass("four-item");
							 }
						}
						else 
						{
							if(count%4===3){
							 	div.addClass("single")
							}
							else
							{
								div.addClass("four-item");
							}
						}
						count++;
					}
				};

				var photoCounter = 0;
				var videoCounter = 0;

				var addImageToContainer = function(selector){
					$(setting.container.find(selector)).each(function(i,ui){
						var item=null;
						console.log(i);
						if((i%2)===0){
							if(photoCounter<vp.photo.length){
								item=vp.photo[photoCounter];
								photoCounter++;
							}
							else if(videoCounter<vp.video.length){
								item=vp.video[videoCounter];
								videoCounter++;
							}
						}
						else
						{
							if(videoCounter<vp.video.length){
								item=vp.video[videoCounter];
								videoCounter++;
							}
							else if(photoCounter<vp.photo.length){
								item=vp.photo[photoCounter];
								photoCounter++;
							}
						}
						addImage(data,$(ui).find(".gallery-group-item-inner"),item,[]);
					});
				};
				addImageFourToContainer = function(selector){

					$(setting.container.find(selector)).each(function(j,ui){
						for(var i=0; i<4;i++){
							var item = null
							
							if(i===0 || i==3){
								if(photoCounter<vp.photo.length){
									item=vp.photo[photoCounter];
									photoCounter++;
								}
								else if(videoCounter<vp.video.length){
									item=vp.video[videoCounter];
									videoCounter++;
								}
							}
							else
							{
								if(videoCounter<vp.video.length){
									item=vp.video[videoCounter];
									videoCounter++;
								}
								else if(photoCounter<vp.photo.length){
									item=vp.photo[photoCounter];
									photoCounter++;
								}
							}
							addImage(data,$(ui).find(".gallery-group-item-inner"),item,[]);
						}
					});
				};
				addImageToContainer(".gallery-group-item.single");
				addImageFourToContainer(".gallery-group-item.four-item");
			},
			populateGalleryV1 = function(data){
				var elems=[];	
				var group=Math.ceil(data.length / 2);
				var nextSingle=true;
				var goBy2=0;
				var count=0;
				for(var i=0;(i<=group && (count<data.length));i++){
					var div=$("<div>").addClass("gallery-group-item").appendTo(setting.container);	
					if(i==0){
						nextSingle = true;
					}
					if(nextSingle)
					{
						div.addClass("single");
						addImage(data,div,data[count],elems);
						count++;
						if(i==0){
							nextSingle =false
						}
						else
						{
							goBy2++;
							if(goBy2==2){
								nextSingle=false;
								goBy2=0;
							}
						}
					}
					else
					{
						for(var j=0;j<4;j++){
							addImage(data,div,data[count],elems);
							count++;
						}
						goBy2++;
						if(goBy2==2){
							nextSingle=true;
							goBy2=0;
						}
					}
					console.log("i="+i+" count="+count);
				}
				
			},
			addImage = function(data,container,item,elems){
				if(item)
				{
					var div = $(setting.template);
					div.addClass(item.type);
					var img = $("<img>").load(function(e){
						checkAllLoaded(data.length,i,elems);
					}).error(function(e){
						checkAllLoaded(data.length,i,elems);
					}).attr({
						src:item.thumburl
					}).appendTo(div.find(".media-container"));
					img.click(function(e){
						viewDetailSingle(item);
					});
					div.find(".btn-play").click(function(e){
						viewDetailSingle(item);
					});
					div.find(".title").html(item.location);
					addToSocialShare(div,item);
					div.appendTo(container);
				}
			},
			populateGallery_new  = function(data){
				
				$(data).each(function(i,item){
					item.items=data;
					var div = $(setting.template);
					div.addClass(item.type);
					var img = $("<img>").load(function(e){
						checkAllLoaded(data.length,i,elems);
					}).error(function(e){
						checkAllLoaded(data.length,i,elems);
					}).attr({
						src:item.thumburl
					}).appendTo(div.find(".media-container"));
					img.click(function(e){
						viewDetailSingle(item);
					});
					div.find(".btn-play").click(function(e){
						viewDetailSingle(item);
					});
					div.find(".title").html(item.location);
					addToSocialShare(div,item);
					div.appendTo(setting.container);
				});
			};
			populateGallery(g.setting.data);

	},
	g.start = function(setting){
		app.gallery.installYouTube();
		g.setting = $.extend(g.setting,setting);
		g.setting.data = g.getAllGalleryItems();

		if(g.api == null){
			g.api =new g.galleryView(g.setting);
		}
	}
})
(app.gallerynew)
app.winseat={
	root:null,
	step2Send:false,
	step3Send:false,
	startUpload:function (e) {
		if(window.FormData){
			$("input[name='data[Participant][uploadType]']").val("ajax");
		}
		else
		{
			$("input[name='data[Participant][uploadType]']").val("iframe");
		}
		var form1=$("#winTicketAccordion .form-1").serializeArray();
		var form2=$("#winTicketAccordion .form-2").serializeArray();
		var form3=$("#winTicketAccordion .form-3").serializeArray();

		var allData=[],allObject={};
		$.merge(allData,form1);
		$.merge(allData,form2);
		$.merge(allData,form3);
		for(var i=0;i<allData.length;i++){
			allObject[allData[i].name]=allData[i].value;
		}
		var files=$("#winTicketAccordion input[type='file']");
		var form=$("<form>");
		for(var f in allObject){
			var input=$("<input>").attr({type:'text',name:f}).val(allObject[f]);
			input.appendTo(form);
		}
		if(allObject['data[Participant][send_txt]']===undefined || allObject['data[Participant][send_txt]'] == "0")
		{
			$(files).each(function(i,f){
				var f=$(f);
				var aCopy=f.clone();
				allObject[f.attr("name")] = {value:f,type:"file",parentCon:f.parent(),fcopy:aCopy};
				f.detach().appendTo(form);
			});
		}
		var loader=$($(".template-loader").val());
		var sender=$(e.target);
		sender.hide().parent().append(loader);
		form.attr({action:"../upload.aspx"});
		$(form).ssformUpload({
			iframe:$("iframe[name='upload-iframe']"),
			events:{
				uploadReady:function(e){
					 loader.remove();
					 for(var f in allObject){
					 	if(allObject[f].type==="file"){
					 		 try
					 		 {
					 		 	allObject[f].value.appendTo(allObject[f].parentCon);
					 		 }
					 		 catch(error){
								allObject[f].fcopy.appendTo(allObject[f].parentCon);
					 		 }
					 	}
					 }
					 sender.show();
					 var resetForm=function(){
			 			$("#winTicketAccordion .form-1")[0].reset();
			 			$("#winTicketAccordion .form-2")[0].reset();
			 			$("#winTicketAccordion .form-3")[0].reset();
			 			//$('a[href="#winSeatStepOne"]').click();
			 			$('a[href="#winSeatStepTwo"]').attr({disabled:"disabled"});
			 			$('a[href="#winSeatStepThree"]').attr({disabled:"disabled"});
			 			app.winseat.step2Send=false;
					 	app.winseat.step3Send=false;
					 	
					 };
					 var onHidden=function()
					 {
					 	resetForm();
					 }
					 if(e.status==="success"){
					 	var message=$(".template-entry-success").val();
					 	var onShown=function(modalUI){
					 		window.sendVersaTag("http://www.samsung.com.au/samsungslideliner/enternow/thankyou");
					 		app.ui.versaTagStart(modalUI);
					 		modalUI.find(".modal-body").find(".enter-new-receipt").click(function(e){
					 			modalUI.modal("hide");
					 			resetForm();
					 			$('a[href="#winSeatStepOne"]').click();
					 			$('.content.winseat a[data-versa-once="true"]').each(function(i,item){
					 				item=$(item);
					 				var versatagurl=item.attr("_xxxdata-versa");
					 				item.attr({'data-versa':versatagurl,'_xxxdata-versa':null});
					 			});
					 			window.sendVersaTag("http://www.samsung.com.au/samsungslideliner/enternow/step1"); 

					 		});
					 	}
					 	app.ui.modal({message:message,modalSize:"modal-md",title:"&nbsp",className:"black-no-footer",onShown:onShown,onHidden:onHidden});
					 }
					 else
					 {
					 	app.ui.modal({message:e.message,modalSize:"modal-md",title:"&nbsp",className:"black-no-footer with-border error",onHidden:onHidden});
					 }
				},
				uploadProgress:function(percent){
					loader.addClass("with-percent");
					loader.find(".percent .inner").css({width:percent+"%"}).html(percent+"%");
				},
				uploadProcessCompleted:function(e){console.log("uploadProcessCompleted:"+e)},
				uploadError:function(e){loader.remove();sender.show();}
			}
		});

	},
	start:function(){
		app.ui.versaTagStart($(".content.winseat"));
		$(".content.winseat #winTicketAccordion").on("show.bs.collapse",function(e){
			e.stopPropagation();
			var id=($(e.target).attr("id"));
			var heading=$("a[href='#"+id+"']");
			var disabled=heading.attr("disabled");
			if(disabled){
				e.preventDefault();
				return;
			}
			$(".panel-title.active").removeClass("active");
			$("a[href='#"+id+"']").parent().addClass("active");
			 
		});
		/*
		$('.content.winseat .nav.nav-tabs a[data-toggle="tab"]').on("shown.bs.tab",function(e){
			$("input[name='data[Participant][send_txt]']")[0].checked=parseInt(($(e.target).parent().attr("value")));
		});
		*/
		app.control.datetimeSelector($(".content.winseat .date-picker"));
		app.control.uploadWrapper();
		app.validation.start();
		$("a[ss-data-url]").click(function(e){
			e.preventDefault();
			app.ui.modal({dataUrl:$(e.target).attr("ss-data-url"),modalSize:"modal-md",title:"&nbsp",className:"black-no-footer with-border winseat-tvs"})
		});
		$("form .action-row .button").click(function(e){
			var form=$(e.target).attr("form");
			var next=$(e.target).attr("next");
			e.stopPropagation();
			e.preventDefault();
			if(form){
				var aForm=$(".steps ."+form);
				if(aForm.valid()){
					switch(next){
						case "step-two":
							if(!app.winseat.step2Send)
							{
								app.winseat.step2Send=true;
								window.sendVersaTag("http://www.samsung.com.au/samsungslideliner/enternow/step2/"); 
							}
						 break;
						case "step-three":
							if(!app.winseat.step3Send)
							{
								app.winseat.step3Send=true; 
								window.sendVersaTag("http://www.samsung.com.au/samsungslideliner/enternow/step3"); 
							}
						 break;
					}
					$("."+next+" .panel-title a").removeAttr("disabled").click();
					if(aForm.hasClass("form-3")){
						//window.sendVersaTag("http://www.samsung.com.au/samsungslideliner/enternow/step3/button"); 
						app.winseat.startUpload(e);
					}
				}
			}
		});	
		$(".content.winseat a[href='#winSeatStepOne']").click();
		window.sendVersaTag("http://www.samsung.com.au/samsungslideliner/enternow/step1");
		var isShow=false;
		var template=$(".template-where-is-serial").val();
		$('.content.winseat .where-is-serial').popover({selector:document.body,placement:"top",html:true, trigger:"hover",template:template,delay: { show: 500, hide: 100 }}).on("shown.bs.popover",function(e){isShow=true;}).on("hidden.bs.popover",function(e){isShow=false;});
		//var toucher=new app.touchUtil($(".content.winseat"),{moveDistance:20,onStartOnly:true});
		/*
		$(".content.winseat").on("ontouchstart",function(e){
				if(isShow){
					$('.content.winseat .where-is-serial').popover("hide")
				}
		});
		*/
	}
}
app.control.spin3d=function(options){
    var 
        //
        hasTouch    =  Modernizr.touch,
        toucher     =  null,
        slider      =  null,
        startEvt    =  null,   
        movePercent =  1,
        loadedTotal =  0,
        currentIndex=  0,
        items       =  [],
        lastMove    =  null,
        addEventTimer = null,
        defaults    =  {
                            length:31,
                            imgBaseSrc:null,
                            canvas:null,
                            indexPadding:null,
                            startIndex:0,
                            addEvent:true,
                            from:null
                       },

        _nextFrame   = function(){
            var next=currentIndex+1;
            if(next>defaults.length-1){
                next=0;
            }
            _showFrame(next);
        },

        _previousFrame = function(){
            var pre=currentIndex-1;
            if(pre<0){
                pre=defaults.length-1;
            }
            _showFrame(pre);
        },
        _showFrame   = function(showIndex){
            console.log("spin3d:playframe>"+showIndex);
            currentIndex = showIndex;
            defaults.canvas.find("img.show").removeClass("show");
            items[showIndex].addClass("show");
            if(options.showIndexChanged){
                options.showIndexChanged(showIndex);
            }
        },
        /*
         *
         */ 
        spinner     = function() {
            var i=0;
            if(defaults.from){
                i=defaults.from;
            }
            defaults.canvas.addClass("loading");
            var loader=defaults.canvas.find(".loader");
            for(i=0; i<defaults.length; i++){
                var img=$("<img>");
                var index=i;
                 var newFilename=index;
                if(defaults.getFilename){
                    newFilename=defaults.getFilename(index);
                }
                if(defaults.indexPadding!==null){
                    if(newFilename.toString().length===1){
                        newFilename=defaults.indexPadding+""+newFilename;
                    }
                }

                img.load(function(e){
                    loadedTotal++;
                    if(loader.length>0){
                        var percent=parseInt(Math.round((loadedTotal/defaults.length) * 100));
                        loader.find(".percent .inner").css({width:percent+"%"}).html(percent+"%");
                    }
                    if(loadedTotal==defaults.length){
                        defaults.canvas.removeClass("loading"); 
                        setTimeout(function(){ 
                            _showFrame(currentIndex);
                        },500);
                    }
                }).attr({src:defaults.imgBaseSrc.replace("[index]",newFilename)});
                if(i===defaults.startIndex){
                    currentIndex = i;
                }
                items.push(img);
                $(defaults.canvas).append(img);
            };
            movePercent = defaults.length/defaults.canvas.clientWidth;
            if(defaults.addEvent)
            {
                addTouchEvent();
            }
        },

        onTouchMove     =  function(e){
            e.stopPropagation();
            e.preventDefault();
            if(e.originalEvent){
                e.originalEvent.stopPropagation();
                e.originalEvent.preventDefault();
            }

            var width    = defaults.canvas.width(),
                newIndex = currentIndex;
                interval = width/defaults.length;
            console.log("spin3d:onTouchMove:"+e.info.diffX+"  "+interval);
            if(lastMove==null){
                if(Math.abs(e.info.diffX)>=(0)){
                    lastMove=e;
                    switch(e.info.dir){
                        case "left":
                            newIndex--;
                            break;
                        case "right":
                            newIndex++;
                            break;
                    }
                }
            }
            else
            {
                if((Math.abs(e.info.diffX)-Math.abs(lastMove.info.diffX))>=(0)) {
                     lastMove=e;
                     switch(e.info.dir){
                        case "left":
                            newIndex--;
                            break;
                        case "right":
                            newIndex++;
                            break;
                    }
                }
            }
            if(newIndex>(defaults.length-1)){
                newIndex=0;
            }
            if(newIndex<0){
                newIndex=defaults.length-1;
            }
            _showFrame(newIndex);
        },
        /*
         *
         */        
        addTouchEvent = function() {
        toucher=new app.touchUtil(defaults.canvas,{moveDistance:1,preventDefault:true,stopPropagation:true});
            var c=defaults.canvas;           
            c.on("moveleft",function(e){
                onTouchMove(e);}
            ).on("moveright",function(e){
                onTouchMove(e);
            });

            c.on("ontouchend",function(e){
                if(addEventTimer){
                    clearTimeout(addEventTimer);
                }
                //addEventTimer=setTimeout(function(){app.slider.toggleSwipe(true);},10);
                e.stopPropagation();
                e.preventDefault();
                if(e.originalEvent){
                    e.originalEvent.stopPropagation();
                    e.originalEvent.preventDefault();
                }
                console.log("touchend");
                lastMove=null;
                /*
                if(e.info.speedX>1000){
                    var o={index:currentIndex};
                    $(o).animate({index:defaults.length-1},{
                        duration:500,
                        step:function(value){
                            value=parseInt(value);
                            
                           console.log(value);
                           _showFrame(value);
                        }
                    });
                };
                */
                //console.log("speedX:"+e.info.speedX);
            });
            defaults.canvas.on('mousewheel', function(event) {
                 console.log("mousewheel:spin3d canvas");
                 event.preventDefault();
                 event.stopPropagation();
                 var newIndex=currentIndex+event.deltaY;
                 if(newIndex>(defaults.length-1)){
                    newIndex=0;
                 }
                 if(newIndex<0){
                    newIndex=defaults.length-1;
                 }
                 _showFrame(newIndex);
            });
        },

        /*
         *
         */
        _destory      = function() {
            //yazApp.removeAllElement(defaults.elm);
        };

        /*
         *
         */
        this.destory             = _destory;
        this.showFrame           = _showFrame;
        this.nextFrame           = _nextFrame;
        this.previousFrame       = _previousFrame;
        this.getCurrentFrameIndex= function(){return currentIndex};
        $.extend(defaults,options);
        spinner();
};
app.slideliner={
	spiner:null,
	frameFetures:{
		"1":{extend:[2,3,4,5],extendIncreaseRight:5, point:{bottom:"40%",right:"50%"},text:"Max Speed: 20km/H<p>Almost as fast as a flying Izzy Folau.</p>",info:{bottom:"80%",right:"50%"}},
		/*"70":{extend:[71,72,73,74],extendIncreaseRight:0,point:{bottom:"50%",right:"40%"},text:"Samsung SlideLiner weight: 340kgs<p>Less than half the weight of the Wallabies forward pack.</p>",info:{bottom:"80%",right:"30%"}},*/
		"38":{extend:[39,40,41,42],extendIncreaseRight:15,"42":{point:{bottom:"8%",right:"0"}},point:{bottom:"4%",right:"48%"},text:"Track Length: 80m<p>Taking you from try line to try line.</p>",info:{bottom:"80%",right:"4%"}},
		"11":{extend:[12,13,14,15],extendIncreaseRight:1,point:{bottom:"49%",right:"39%"},text:"Custom Racing-Style Seats<p>Designed for comfortable viewing</p>",info:{bottom:"78%",right:"20%"}},
		"28":{extend:[29,30,31,32],extendIncreaseRight:2,point:{bottom:"49%",right:"61%"},text:"4-point harness system<p>Strap yourself in and watch the action.</p>",info:{bottom:"80%",right:"50%"}},
		"56":{extend:[57,58,59,60,61,62],extendIncreaseRight:0,point:{bottom:"58%",right:"74%"},text:"Integrated video cameras<p>4 individual cameras streaming live action.</p>",info:{bottom:"80%",right:"57%"}},
		"20":{extend:[21,22,23,24],extendIncreaseRight:0,point:{bottom:"49%",right:"40%"},text:"Built-in Samsung tablets<p>Stream, capture and share to social media, straight from the SlideLiner.</p>",info:{bottom:"85%",right:"34%"}}
	},
	getRotate:function(sidea,sideb){
	   var sidec,tanx,atanx,anglex;
	    sidec = Math.pow(sidea,2)+Math.pow(sideb,2);
		sidec = Math.sqrt(sidec);
		tanx = sidea / sideb;
	   	atanx = Math.atan(tanx);
	    anglex = atanx * 180 / Math.PI;
	    return anglex;
	},

	 getTransformCSS:function(x,y,deg,second){
		var css= {
				'-webkit-transform':'translate('+x+','+y+') rotate('+deg+'deg)',
				'transform':'translate('+x+','+y+')  rotate('+deg+'deg)'
			}
		if(second){
			css['-webkit-transition']='-webkit-transform '+second+'ms ease-out';
			css['transition']='transform '+second+'ms ease-out';
			css['-ms-transition']='transform '+second+'ms ease-out';
		}
		else{
			//css['-webkit-transition']='none';
			//css['transition']='none';
		}
		return css;
	},
	showFeature:function(index){
		var outerCon=$(".content.slideliner .frames");
		var con=$(".content.slideliner .frames .feature-bubble");
		var centerPoint = {x:con.width()/2,y:con.height()-50};
		var ff=app.slideliner.frameFetures[index];
		if(ff){
			outerCon.addClass("show-feature");
			/*var sidea = centerPoint.x-ff.right,
				sideb = centerPoint.y-ff.bottom,
				angle = app.slideliner.getRotate(sidea,sideb),*/
			var
				point = con.find(".point"),
				line  = con.find(".line"),
				info  = con.find(".info-container");
			info.html(ff.text);
			point.css(ff.point);
			info.css(ff.info).css({marginLeft:-info.width()/2,marginTop:-info.height()/2});
			
			var p1=point.position();
			var p2=info.position();
			var dir=-1;
			if((p1.left-p2.left)<0){
				dir=1;
			}
			var sidea = Math.abs(p2.left- p1.left);
			var sideb = Math.abs(p2.top - p1.top);
			var angle = app.slideliner.getRotate(sidea,sideb);
			var sidec = Math.pow(sidea,2)+Math.pow(sideb,2);
				sidec = Math.sqrt(sidec);
			//console.log("sidea:"+sidea+"	sideb:"+sideb+"	 angle:"+angle);
			var transformCSS=app.slideliner.getTransformCSS(0,0,angle*dir);
			transformCSS["-webkit-transform-origin"]="0 100%";
			transformCSS["transform-origin"]="0 100%";
			transformCSS.height=((sidec/outerCon.height())*100)+"%";
			if(isIE8){
				transformCSS.height=sideb;
				var right=parseInt(ff.point.right);
				var percent=((info.width()/2)/outerCon.width())*100;
				console.log("right:"+right+"	percent:"+percent);
				right=(right-percent)+"%";
				info.css({right:right,marginLeft:0});
			}
			$.extend(transformCSS,ff.point);
			//line.css(ff.point);
			var bottom=parseInt(transformCSS.bottom);
			var right =parseInt(transformCSS.right);
			console.log(bottom+"	"+right);
			line.css(transformCSS);
			//console.log(angle);
		}
		else{
			outerCon.removeClass("show-feature");
		}
	},
	onKeyDown:function(e){
		
		if(e.keyCode==38 || e.keyCode==37){
			e.stopPropagation();
			e.preventDefault();
			if(app.spiner){
				app.spiner.previousFrame();
			}
		}
		else if(e.keyCode==39 || e.keyCode == 40){
			e.stopPropagation();
			e.preventDefault();
			if(app.spiner){
				app.spiner.nextFrame();
			}
		}
	},
	onEnter:function(slide){
		$(document).on("keydown",app.slideliner.onKeyDown);
	},
	onExit:function(slide){
		$(document).off("keydown",app.slideliner.onKeyDown);
	},
	onExitStart:function(slide){
		 app.slideliner.onExit(slide);
	},
	start:function () {
		var setting={
			length:76,
            imgBaseSrc:"assets/spin3d-new/FINAL_Frame_.00[index].png",
            canvas:$(".content.slideliner .frames"),
            indexPadding:"0",
            startIndex:0,
            addEvent:true,
            from:null,
            getFilename:function(index){
            	return index;
            	//return index%2>0  ? index : index+1;
            }
		};
		var track=$(".spin-3d .track-outer");
		var toucher=new app.touchUtil(track,{preventDefault:true,stopPropagation:true});
		track.on("ontouchstart",function(e){
			e.stopPropagation();
			e.preventDefault();
		}).on("moveleft",function(e){
			console.log($(e.target).attr("class")+":moveleft");
	    	e.preventDefault();
	    	e.stopPropagation();
        }).on("moveright",function(e){
        	console.log($(e.target).attr("class")+":moveright");
            e.preventDefault();
	    	e.stopPropagation();
        }).on('swipeleft',function(e){
        	console.log($(e.target).attr("class")+":swipeleft");
        	e.preventDefault();
	    	e.stopPropagation();
        }).on('swiperight',function(e){
        	console.log($(e.target).attr("class")+":swiperight");
        	e.preventDefault();
	    	e.stopPropagation();
        });
        var toExtend={};
        for(var f in app.slideliner.frameFetures){
        	var ff=app.slideliner.frameFetures[f];
        	if(ff.extend){
        		$(ff.extend).each(function(i,item){

        			toExtend[item]=$.extend(true,{},ff);
        			var newPercent=(parseInt(toExtend[item].point.right)-(ff.extendIncreaseRight*(item-(parseInt(f)))));
        			if(newPercent<1){
        				newPercent=0;
        			}
        			toExtend[item].point.right=newPercent+"%";
        			if(ff[item]){
        				toExtend[item].point=ff[item].point;
        			}
        		});
        	}
        }
        $.extend(app.slideliner.frameFetures,toExtend);
		setting.showIndexChanged=function(index){
			$(".spin-slider .track  .inner").slider('setValue', index+1);
			app.slideliner.showFeature(index+1);
		};	
		$(".spin-slider .track  .inner").slider({tooltip:"hide",handle:"square",min:1,max:76,step:1,value:1}).on("slide",function(ev){
			app.spiner.showFrame(ev.value-1);
		});
		$(".spin-slider .track  .slider-handle").addClass("diamond-shape");
		app.spiner=new app.control.spin3d(setting);
		
	}
    
};
app.social=function(option){
	var started = false;
	this.start   = function(){
			if(started){
				return;
			}
			started=true;
			var $container = option.container;
			var mapi = null;
			var isLoading = false;
			var pageindex = 1;
			var totalPage = null;
			var endOfPage=false;
			var loadedImage=[];
			var loader=$('<div class="loader">');
			var startMasonry =function()
			{
				if(mapi==null)
				{
					$container.masonry({
						itemSelector: option.namespace +' .social-item',
						columnWidth: option.width ? option.width : 320,
						isFitWidth:true
					});
					mapi=$container.data('masonry');
				}
			};
			var checkAllLoaded = function (length,index,elems){
				loadedImage.push(index);
				console.log(length+"  "+index+"  "+loadedImage.length);
				if(loadedImage.length===length){
					//console.log("allLoaded");

					$(option.namespace +" .social-item.loading").removeClass("loading");
					loadedImage=[];
					if(mapi===null)
					{
						if(isIE8)
						{
							setTimeout(function(){startMasonry();},700);
						}
						else
						{
							setTimeout(function(){startMasonry();},200);
						}
					}
					else
					{
						$container.masonry('appended',elems);
					}
					loader.detach();
				}
			};
			var findLinks = function(s,sourceUrl) {
		          var hlink=/\S+\.\S+/g;
		          return (s.replace(hlink, function(a, b, c) {
		              return ('<a href="'+a+'" target="_blank">'+a+'</a>');
		          }));
		     };
			var getShareLink = function (url){
				return "window.open('"+url+"','','width=500,height=580')";
			};
			var showInContainer = function(data){
				var elems=[];
				$(data).each(function(i,feed){
					feed.source=feed.SocialMessage.socialType=="ig" ? "instagram" :"twitter";
					feed.type=(feed.SocialMessage.videoUrl && feed.SocialMessage.videoUrl.length>5 && !isIE8) ? "video" : "photo";
					feed.media_url_https=feed.SocialMessage.videoUrl;
					feed.media_url=feed.SocialMessage.postImageLarge;
					feed.source_id=feed.SocialMessage.postId;
					feed.screen_name=feed.SocialMessage.profileName;
					feed.source_url=feed.SocialMessage.postUrl;
					feed.profile_image_url=feed.SocialMessage.profileImage;
					feed.name=null;
					feed.text=feed.SocialMessage.postMessage;
					var div=$($(".template-social-feed-item."+feed.type).val()).addClass(feed.source);
					div.addClass(feed.type);
					if(feed.type==='photo'){
						$("<img>").load(function(e){
							checkAllLoaded(data.length,i,elems);
						}).error(function(e){ 
							checkAllLoaded(data.length,i,elems);
						}).attr({src:feed.media_url}).appendTo(div.find(".media-container"));
					}
					else if(feed.media_url_https){

						var video=$("<video>").attr({width:320,head:320,controls:true}).append(
							$("<source>").attr({type:'video/mp4',src:feed.media_url_https})
							).appendTo(div.find(".media-container"));
						$("<img>").load(function(e){
							video.attr({poster:feed.media_url});
							checkAllLoaded(data.length,i,elems);
						}).error(function(e){
							checkAllLoaded(data.length,i,elems);
						}).attr({src:feed.media_url})
					}
					var sourceUrl=null;
					var retweeturl="https://twitter.com/intent/retweet?tweet_id=[id]";
					var twitterShareFB="https://www.facebook.com/sharer/sharer.php?u=https://twitter.com/[username]/status/[id]";
					var shareOnTwitter="https://twitter.com/intent/tweet?url=[source_url]&text=Check%20out%20the%20Samsung%20SlideLiner%20in%20action&hashtags=SlideLiner";
					var instagramShareFB="https://www.facebook.com/sharer/sharer.php?u=[source_url]";
					
					var userlinkUrl=null;
					switch(feed.source){
						case "twitter":
							sourceUrl=("https://twitter.com/[username]/status/[id]").replace('[id]', feed.source_id).replace("[username]",feed.screen_name);
							userlinkUrl="http://www.twitter.com/";
							div.find(".action-row a.retweet").attr({onclick:getShareLink(retweeturl.replace('[id]',feed.source_id))});
							div.find(".action-row a.fb-share").attr({onclick:getShareLink(twitterShareFB.replace('[id]', feed.source_id).replace("[username]",feed.screen_name))});
							break;
						case  "instagram":
							sourceUrl=feed.source_url;
							userlinkUrl="http://instagram.com/";
							div.find(".action-row a.twitter-share").attr({onclick:getShareLink(shareOnTwitter.replace('[source_url]',feed.source_url))});
							div.find(".action-row a.fb-share").attr({onclick:getShareLink(instagramShareFB.replace('[source_url]',feed.source_url))});
							break;
					}			
					var a =$("<a>").attr({href:userlinkUrl+feed.screen_name+"/",target:"_blank"});
					a.append($("<img>").attr({src:feed.profile_image_url}));
					div.find(".profile-image").append(a);
					a =$("<a>").attr({href:userlinkUrl+feed.screen_name+"/",target:"_blank"}).html(feed.screen_name);
					//div.find(".user-name .name").append(a)
					div.find(".user-name .screen-name").append(a);
					div.find(".post-message").html(findLinks(feed.text,sourceUrl));	
					div.appendTo($container);
					elems.push(div[0]);
				});
			};
			var  loadNextPage = function(){
				isLoading=true;
				loader.detach().appendTo($container);
				if(option.loadNextPage)
				{
					var data=option.loadNextPage(pageindex);
					if(data && data.length>0){
						showInContainer(data);
						isLoading=false;
					}
				}
				else
				{
					$.ajax({
						type:"POST",
						url:"services/SocialMessages/getImages/"+pageindex
					}).success(function(e){
						if(e.data && e.data.length>0){
							if(e.data.length<20){
								endOfPage=true;
							}
							if(!option.checkScroll){
								if(e.data.length>10){
									e.data=e.data.splice(0,10);
								}
							}
							showInContainer(e.data);
							//totalPage=e.TotalPage;
							isLoading=false;
						}
						else
						{

						}
					}).error(function(e){
						isLoading=false;
					}
					);
				};
			};
			if(option.checkScroll)
			{
				var scrollElm=$(window);
				scrollElm.on("scroll",function(e){
					var currentSlide=app.slider.getCurrentSlide();
					if(currentSlide.route==="social")
					{
						if (($(window).height() + $(window).scrollTop()) >= $(document.body).outerHeight()) {
					        if(!isLoading)
					        {
					          	pageindex++;
					          	if(!endOfPage)
					          	{
					          		loadNextPage();
					          	}
					        }
					    }
					}
				});
			}
			loadNextPage();
	}
}

 
 $(document).ready(function(e){
	app.slider=new app.slidingNav();
	app.slider.start();
	//app.toucher=new app.touchUtil($(".main-content"));
	setTimeout(function() {
		 
		if(!isOkToLive)
		{
			$(".holding-page").addClass("in");
			var onResize=function(){
				var height=$(document.body).height();
				$(".holding-page").css({height:height,lineHeight:height+"px"});	
			}
			
			$(window).bind("resize",function(e){
				onResize();
			});
			onResize();
		}
		else
		{
			$(".holding-page").remove();	
		}
	},1000);
});
app.ui={
	versaTagStart:function(root){
		root.find("[data-omniture]").each(
			function(i,item){
				$(item).click(function(e){
					var type="o";
					var currentSlide=app.slider.getCurrentSlide();
					var buttonName=$(item).attr("data-omniture");
					type=$(item).attr("omniture-event-type");
					if(type==null || type=="undefined"){
						type="o";
					}
					var prop5 = "au:campaign:seau:samsungslideliner:"+buttonName;
			  	 	var hier1  = "au>campaign>seau>samsungslideliner>"+buttonName;
			  	 	s.pageName="au:campaign:seau:samsungslideliner:"+currentSlide.route;
			  	 	s.prop5 = prop5;
			  	 	s.hier1=hier1;
			  	 	s.linkTrackVars='eVar33,events';
					s.linkTrackEvents='event45';
					s.eVar33="au:campaign:seau:samsungslideliner:"+buttonName;
					s.events='event45';
					console.log("omniture:"+type+" "+buttonName);
					s.tl(this,type,s.eVar33);
				});
			}
		);
		root.find("[data-versa]").each(
			function(i,item){
				$(item).click(function(e){
					var tag=$(item).attr("data-versa");
					var disabled=$(item).attr("disabled");
					var fireOnce=$(item).attr("data-versa-once");
					if(disabled){
						return;
					}
					if(tag)
					{
						if(fireOnce){
							$(item).attr({"data-versa":""});
							$(item).attr({"_xxxdata-versa":tag});
						}
						window.sendVersaTag(tag); 
					}
				});
			}
		);
	},
	modal:function(messageObj){
		var div='<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">'
					+'<div class="modal-dialog">'
						+'<div class="modal-content">'
						+'</div>'
					+'</div>'
				+'</div>';
		var modalUI=$(div);
		if(messageObj.className){
			modalUI.addClass(messageObj.className);
		}
		if(messageObj.title){
			var header=$('<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button><h4 class="modal-title">'+messageObj.title+'</h4></div>');
			header.appendTo(modalUI.find(".modal-content"));
		}
		var body=$("<div>").addClass("modal-body");
		body.appendTo(modalUI.find(".modal-content"));	
		if(messageObj.message){
			body.html(messageObj.message);
		}
		if(messageObj.modalSize){
			modalUI.find(".modal-dialog").addClass(messageObj.modalSize);
		}
		if(messageObj.dataUrl){
			var loader=$("<div>").addClass("loader");
			var mdiv=$("<div>").load(messageObj.dataUrl,function(e){
				loader.remove();
				modalUI.find(".modal-body").append(mdiv);
			});
		}
		var footer=$('<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>');
		footer.appendTo(modalUI.find(".modal-content"));
		modalUI.appendTo($(document.body));
		modalUI.on("hidden.bs.modal",function (e) {
			//$("html,body").css({overflow:""});
			modalUI.remove();
			if(messageObj.onHidden){
				messageObj.onHidden();
			}
		});
		if(messageObj.alertType){
			modalUI.addClass(messageObj.alertType);
		};
		modalUI.on("show.bs.modal",function (e) {
			 //$("html,body").css({overflow:"hidden"});
		});
		modalUI.on("shown.bs.modal",function (e) {
			 if(messageObj.onShown){
			 	messageObj.onShown(modalUI);
			 }
		});
		modalUI.modal();

	}
};
$(document).ready(function(e){
	var onResizing=function(e){
		var currentSlide=app.slider.getCurrentSlide();
		if(currentSlide){
			var height=currentSlide.ui.div.find(".content-wrapper").outerHeight(true);
			//height=height+parseInt(currentSlide.ui.div.find(".content").css("paddingTop").replace("px"));
			//height=height+parseInt(currentSlide.ui.div.find(".content").css("paddingBottom").replace("px"));
			var winHeight=($(window).height())-$(".footer").outerHeight(true)-$(".global_header").outerHeight(true);
			if(height<winHeight){
				height=winHeight;
			}
			if($(".navigation-small").css("display").toLowerCase()=="block"){
				height=height+$(".navigation-small").outerHeight(true);
				height=height+currentSlide.ui.div.find(".slide-linder-footer").outerHeight(true);
			}
			var content=currentSlide.ui.div.find(".content");
			height=height+(content.outerHeight(true)-content.height());
			//height=height-$(".global_header").height();
			 
			$(".top-container").css({height:height});
			window.parent.postMessage(JSON.stringify({t:{type:"height",height:height}}),"*");	
		}
		//console.log(height+"	"+$(".top-container").height());
		
	}
	$(window).bind("resize",onResizing);
 	setInterval(onResizing,500);
 	//onResizing();
});