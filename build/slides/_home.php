<?php 
        date_default_timezone_set("Australia/Sydney");
        $now    =  new DateTime("now");
        $enddate = new DateTime('2014-9-30 23:59:59');
        $interval = $now->diff($enddate);
        $days = str_pad($interval->format('%a'),2,"0",STR_PAD_LEFT);
        $hours = $interval ->h;
        $hours = str_pad(($hours),2,"0",STR_PAD_LEFT);
        $swtichOver = false;
        if(date_timestamp_get($now) > date_timestamp_get($enddate)){
            $swtichOver = true;    
        }
        $swtichOver = true;
        //print("$swtichOver");
        //$days  = $enddate->diff($now);
?>
<div class="content home max-width">
    <div class="content-wrapper">
        <div>
            <h2 class="font-size-lg it-is-for-you text-shadow">Samsung Transforms</h2>
            <h2 class="font-size-lg your-view-experience text-shadow">Your viewing experience</h2>
            <!--<h1 class="font-size-xlg win-a-seat text-shadow">Samsung Transforms</h1>-->
            <!--<h1 class="font-size-xlg samsung-slideliner text-shadow">Your viewing experience</h1>-->
        </div>

        <div class="hide row-margin-top-20">
            <div class="row">
                <div class="xs-align-center find-out-more-container lg-align-right col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <a data-omniture="home_find_out_more" data-versa="http://www.samsung.com.au/samsungslideliner/findoutmore" class="button btn-white diamond-shape btn-lg font-black" href="#prizes">Find out more<i class="fa fa-chevron-right"></i></a>
                </div>
            </div>
        </div>
        <div class="row-margin-top-20">
            <div class="slide-liner-container">
                <div class="rail-image">
                    <div class="image-container">
                        <div class="inner">
                            <img src="assets/images/rail.png" alt="Samsung slide liner rail"/>
                        </div>
                    </div>
                </div>
                <div class="slide-liner-image">
                    <div class="image-container">
                        <div class="inner">
                            <img src="assets/images/slideliner_new.png" alt="Samsung slide liner"/>
                        </div>
                    </div>
                </div>
                <div class="sport-man-container">
                    <div class="image-container">
                        <div class="inner">
                            <img src="assets/images/sportman.png" alt="Sport man"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tv-video-player">                
                <div class="tv-bg">
                    <div class="tv-bg-wrapper">
                        <div class="background-wrapper">
                            <div class="image-container">
                                <div class="inner">
                                    <img src="css/img/tv-bg.png" alt="Samsung smart tv"/>
                                </div>
                            </div>
                        </div>
                        <div class="tv-player-foreground">
                            <div class="tv-player-container">
                                <div class="player-bg">
                                    <div class="image-container">
                                        <div class="inner">
                                            <img src="assets/images/tv-player-foreground.jpg" alt="Samsung smart tv"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="player-container">
                                    <div class="btn-play">
                                        <div class="image-container">
                                            <div class="inner">
                                                <img data-versa="http://www.samsung.com.au/samsungslideliner/tvc" src="assets/images/play-btn.png" alt="play video"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="video-object-container">
                                        <div id="video-object-container-inner" class="video-object-container-inner"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>

            </div>
        </div>
         
    </div>
    <textarea class="hide template template-home-play-video">
        <div class="gallery-player video">
            <div class="home-video-player"></div>
        </div>
    </textarea>
     <div class="slide-linder-footer">
	<div class="visible-xs padding padding-40 footer-inner">
		<div class="text-center">
			<div class="vspacing vspacing-20">
				<div class="share-group clearfix">
					<div class="share-icon facebook">
						<a data-omniture="facebook-samsungslideliner" data-versa="http://www.samsung.com.au/samsungslideliner/facebook" onclick="window.open(app.facebook_share_url,'','width=500,height=580')"><img src="assets/images/dummy.png"></a>
					</div>
					<div class="share-icon twitter">
						 <a onclick="window.open(app.twitter_share_url,'','width=500,height=580')"  data-versa="http://www.samsung.com.au/samsungslideliner/twitter"  data-omniture="twitter-samsungslideliner"><img src="assets/images/dummy.png"></a>
					</div>
				</div>
				<div class="desktop-logo-container">
					<div class="logo"></div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<a href="http://www.samsung.com/au/promotions/pdf/SamsungSlideliner-TermsandConditions.pdf" class="word-break-all"  omniture-event-type="e" data-omniture="terms-conditions" target="_blank">Terms & conditions</a>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<a href="http://www.samsung.com/au/info/privacy.html" omniture-event-type="e" data-omniture="privacy-policy" target="_blank">Privacy policy</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
    var socialCon=null;
    var homeSlide = {
        onEnter: function (slide) {
            setTimeout(function(){
                if(!slide.ui.div.find(".slide-liner-container").hasClass("slide-in"))
                {
                    slide.ui.div.find(".slide-liner-container,.tv-video-player").addClass("slide-in");
                }
            },200); 
        },
        onExit: function (slide) {
            slide.ui.div.find(".slide-liner-container,.tv-video-player").removeClass("slide-in");
        }

    };
 
    $(".player-container .btn-play").click(function(e){
       $(this).fadeOut("fast");
       app.gallery.playerVideo({
            videoUrl:"TSwjz3Dz3I4",
            playlist:["TSwjz3Dz3I4","7u7T_nSwwnU"],
            getNext: function(current){
                var index = this.playlist.indexOf(current);
                index++;
                if(index > this.playlist.length-1){
                    return null;
                };
                return this.playlist[index]
            }
        },$(".video-object-container-inner"));
    });
    app.gallery.installYouTube();
    app.ui.versaTagStart($(".content.home"));
    //homeSlide.onEnter();
</script>