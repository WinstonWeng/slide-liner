<?php
	function curPageURL() {
		 $pageURL = 'http';
		 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		 $pageURL .= "://";
		 if ($_SERVER["SERVER_PORT"] != "80") {
		  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		 } else {
		  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		 }
		 return $pageURL;
	} 
	$gallery =$_GET["gallery"];
	$curl= curPageURL();
	$url = $curl;
	$pieces = explode("?", $url);
	array_pop($pieces);
	$url = $pieces[0];
	$pieces = explode("/", $url);
	array_pop($pieces);
	$url = join("/",$pieces);
	$image = $_GET["gallery"];
	if(empty($image)){
		$image=$url."/OGshare.jpg";
	}
	else
	{
		$image=$url."/".$image;
	}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" class="ltr not-iframe static-header" style="">
<head>
<title>Samsung SlideLiner</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="keywords" content="Samsung, Samsung Slideliner, slideliner, Smart TV, LED TV, UHD TV, UHD, LED, #slideliner, rugby, union, rugby union, nathan sharpe, nathan sharp, win seats, win tickets, wallabies, all blacks, ANZ stadium, Pattersons stadium, Suncorp stadium, Sydney, Perth, Brisbane" />
<meta name="description" content="Win seats on the Samsung SlideLiner & see the Wallabies from the best seats in the house. You’ll also meet the Wallabies, lunch with legend Nathan Sharpe & heaps more. Plus there are hundreds of other prizes to be won.">
<meta name="title" content="Win seats on the Samsung SlideLiner" />
<meta name="date" content="2014-07-08" />
<meta name="sitecode" content="au" />

<meta property="og:site_name" content="Samsung SlideLiner" />
<meta property="og:locale" content="en_us" />
<meta property="og:type" content="samsungslideLiner:photo" />
<meta property="og:image" content="<?php echo $image; ?>" />
<meta property="og:image:secure_url" content="<?php echo $image; ?>" />
<meta property="og:title" content="Win seats on the Samsung SlideLiner" />
<meta property="og:url" content="<?php echo $curl; ?>" />
<meta property="og:description" content="Win seats on the Samsung SlideLiner & see the Wallabies from the best seats in the house. You’ll also meet the Wallabies, lunch with legend Nathan Sharpe & heaps more. Plus there are hundreds of other prizes to be won." />
<link rel="stylesheet" type="text/css" href="js/venders/bootstrap-3.1.1/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="js/venders/bootstrap-slider/css/slider.css"/>
<link rel="stylesheet" type="text/css" href="css/venders/font-awesome/css/font-awesome.min.css"/>
 
<link rel="stylesheet" type="text/css" href="css/app.css?v=3.7"/>
<script type="text/javascript" data-main="js/app" src="js/venders/jquery/jquery-1.10.0.min.js">
</script>
<body>
<script type="text/javascript">
	document.location.href="<?php echo $url;  ?>#gallery/?gallery=<?php echo $gallery; ?>";
</script>
</body>
</html>