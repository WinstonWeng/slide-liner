
<?php
include('config.php');
require('class/ResponseObject.php');
function outputJson($db, $hashtag,$page,$pagesize){
    $startRow=($page-1) * $pagesize;
    $endrow = $startRow + $pagesize;
    //echo $startRow."  ".$endrow."  "."SELECT * FROM media WHERE hashtag='$hashtag' ORDER BY source_id DESC limit $startRow, $pagesize";
    $db_con = mysqli_connect($db['host'], $db['user'], $db['password'], $db['name']);
    $query = mysqli_query($db_con, "SELECT * FROM media WHERE hashtag='$hashtag' ORDER BY source_id DESC limit $startRow, $pagesize");
    $rows = array();
    if (mysqli_num_rows($query) > 0) {
        while ($post = mysqli_fetch_assoc($query)) {
            $rows[] = $post;
        }
    }
    
    $totalRecords=0;
    $query = mysqli_query($db_con, "SELECT count(*) as totalRecords FROM media WHERE hashtag='$hashtag'");
    if (mysqli_num_rows($query) > 0) {
        while ($post = mysqli_fetch_assoc($query)) {
              $totalRecords=$post['totalRecords'];   
        }
    }
    mysqli_close($db_con);
    $repsonse =new ResponseObject();
    $repsonse->TotalPage=ceil($totalRecords / $pagesize);
    $repsonse->TotalRecords= intval($totalRecords);
    $repsonse->Data=$rows;
    return json_encode($repsonse);
}

$pageindex = null;
$pagesize  = null;
if(array_key_exists("pageindex",$_POST))
{
    try
    {
        $pageindex=intval($_POST["pageindex"]);
    }
    catch (HttpException $ex)
    {

    }
}
if(array_key_exists("pagesize",$_POST))
{
    try
    {
        $pagesize=intval($_POST["pagesize"]);
    }
    catch (HttpException $ex){

    }
}

if($pageindex === null){
    $pageindex = 1;
}
if($pagesize === null){
    $pagesize = 50;
}

header('Content-Type:application/json');
echo outputJson($db, $hashtag,$pageindex,$pagesize);
?>