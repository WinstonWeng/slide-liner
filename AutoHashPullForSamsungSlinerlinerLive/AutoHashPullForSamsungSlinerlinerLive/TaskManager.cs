﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Timers;
using System.Threading;
using Timer =System.Timers.Timer;
namespace AutoHashPullForSamsungSlinerlinerLive
{
    public partial class TaskManager : Form
    {
        public Timer timer;
        public int totalRequest = 0;
        public Thread thread;
        public int count = 0;
        public int failCunter = 0;
        public int inteval = 120;
        public string[] tasks;
        public TaskManager()
        {
            InitializeComponent();
        }


        public bool DoRequest(string url)
        {
            bool result = true;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                result = (response.StatusCode == HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                result = false;
                failCunter++;
            }

            return result;
        }
        public void StartNextTask()
        {
            if (timer != null)
            {
                timer.Stop();
                timer.Dispose();
            }
            ReportMessage("Waiting for next interval");
            timer = new Timer();
            timer.Interval = inteval * 60 * 1000;
            timer.Elapsed += timer_Elapsed;
            timer.Start();
        }

        public void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            RunTask();
        }

        public delegate void Delegate_Report_Status(string message);
        public void ReportMessage(string message){
               if( this.InvokeRequired)
               {
                   this.Invoke(new Delegate_Report_Status(ReportMessage), new object[] { message });
               }
               else
               {
                   this.toolStripStatusLabel1.Text = message;
                   this.toolStripStatusLabel2.Text = "("+totalRequest.ToString() + "):(" + failCunter.ToString() + "):";
               }
        }
        public void StartTask()
        {

            string url = tasks[count];
            ReportMessage(url);
            if(DoRequest(url)){
                count++;
                if (count < tasks.Length)
                {
                    StartTask();
                }
                else
                {
                    StartNextTask();
                }
            }
            else
            {
                ReportMessage("Error");
                StartNextTask();
            }
        }

        public void RunTask()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(RunTask));
            }
            else
            {
                totalRequest++;
                tasks = this.richTextBox1.Text.Split(',');
                inteval = int.Parse(this.textBox1.Text);
                count = 0;
                if (thread != null)
                {
                    thread.Abort();
                }
                thread = new Thread(new ThreadStart(StartTask));
                thread.Start();
            }
            
        }
        private void button1_Click(object sender, EventArgs e)
        {
            RunTask();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
