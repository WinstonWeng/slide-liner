var
    
    gulp            = require('gulp'),
    template        = require('gulp-template'),
    sass            = require('gulp-sass');
    insert          = require('./nodejs/insert'),
    watch           = require('gulp-watch'),
    closureCompiler = require('gulp-closure-compiler'),
    minifyCSS       = require('gulp-minify-css'),
    isRelease       = true,
    version         = "3.7",
    twitterText     = encodeURIComponent("It’s on for you & 3 mates - win seats on the Samsung #SlideLiner plus a VIP day with Nathan Sharpe & more."),
    url             = encodeURIComponent("https://www.samsung.com.au/samsungslideliner"),
    files           = ['../*.php','../*.html','../slides/*.html','../slides/*.php','../Korea/*.html','../js/*.js', '../scss/*.scss']
    settings = {
        insert:insert,
        build:"../build",
        name:"Samsung-sound",
        uploadUrl:"../upload.aspx",
        subscribeUrl:"upload.aspx",
        omiture:"s_code_microsite.js",
        omitureName:"samsungslideliner",
        release:isRelease,
        version:version,
        siteUrl:"http://samsung.traffikdev.com/samsungslideliner",
        tncUrl:"http://www.samsung.com/au/promotions/pdf/SamsungSlideliner-TermsandConditions.pdf",
        privacyUrl:"http://www.samsung.com/au/info/privacy.html",
        facebookShareUrl:"https://www.facebook.com/sharer.php?app_id=257838867740570&u="+url+"&display=popup",
        twitterShareUrl:"https://twitter.com/intent/tweet?original_referer=http://www.samsung.com.au/samsungslideliner&text="+twitterText+"&tw_p=tweetbutton&url=http://bit.ly/1qMiv8T",
        twitterShareText:twitterText,
        serviceServer:"http://sound.traffikdev.com/samsungsound/",
        iframeUrl:'../index.html',
        winConsole:"",
        jqueryUrl:'<script type="text/javascript" src="../js/venders/jquery/jquery-1.10.0.min.js"></script>',
        pkg:{

        }
    };
gulp.task('releasedesktop',function(){
    settings.build="../build/release/";
    
    var desktop={
        release:isRelease,
        insert:require('./nodejs/insert'),
        uploadUrl:"services/participants/register",
        subscribeUrl:"services/edms/subscribe",
        omiture:"s_code_microsite.js",
        omitureName:"samsungslideliner",
        siteUrl:"https://www.samsung.com.au/samsungslideliner",
        tncUrl:"http://www.samsung.com/au/promotions/pdf/SamsungSlideliner-TermsandConditions.pdf",
        privacyUrl:"http://www.samsung.com/au/info/privacy.html",
        facebookShareUrl:"https://www.facebook.com/sharer.php?app_id=257838867740570&u="+url+"&display=popup",
        twitterShareUrl:"https://twitter.com/intent/tweet?original_referer=http://www.samsung.com.au/samsungslideliner&text="+twitterText+"&tw_p=tweetbutton&url=http://bit.ly/1qMiv8T",
        twitterShareText:"",
        serviceServer:"",
        jqueryUrl:"",
        version:version,
        iframeUrl:'//www.samsungslideliner.com.au',
        winConsole:"window.console={log:function(msg){ }};"
    };
    gulp.src('../js/app.js').pipe(template(desktop)).pipe(closureCompiler({compilerPath:'bower_components/closure-compiler/compiler.jar',fileName:'app.js'})).pipe(gulp.dest(settings.build+'/js/'));
    //gulp.src('./js/app.js').pipe(template(desktop)).pipe(gulp.dest(settings.build+'/js/'));
    gulp.src(['../*.php']).pipe(template(desktop)).pipe(gulp.dest(settings.build));
    gulp.src(['../*.html']).pipe(template(desktop)).pipe(gulp.dest(settings.build));
    gulp.src(['../Korea/index.html']).pipe(template(desktop)).pipe(gulp.dest(settings.build+"/Korea/"));
    gulp.src(['../slides/*.html','../slides/*.php']).pipe(template(desktop)).pipe(gulp.dest(settings.build+"/slides/"));
    gulp.src(['../slides/partials/*.html']).pipe(template(settings)).pipe(gulp.dest(settings.build+'/slides/partials/'));   
    gulp.src("../scss/*.scss").pipe(sass({style: 'compressed'})).pipe(minifyCSS({compatibility:"ie7"})).pipe(gulp.dest(settings.build+'/css/'));
    gulp.src(["../build/OGshare.jpg"]).pipe(gulp.dest(settings.build));
    gulp.src(["../build/js/venders/**", "!../build/js/venders/Thumbs.db"]).pipe(gulp.dest(settings.build+'/js/venders/'));
    gulp.src(["../build/assets/**", "!../build/assets/Thumbs.db","!./build/assets/images/Thumbs.db"]).pipe(gulp.dest(settings.build+'/assets/'));
    gulp.src(["../build/css/img/**","!../build/css/img/Thumbs.db"]).pipe(gulp.dest(settings.build+'/css/img/'));
    gulp.src(["../build/css/venders/**","!../build/css/venders/Thumbs.db"]).pipe(gulp.dest(settings.build+'/css/venders/'));
    gulp.src(["../build/css/fonts/**","!../build/css/fonts/Thumbs.db"]).pipe(gulp.dest(settings.build+'/css/fonts/'));
    /*
    settings.build="./Release/Singapore/Mobile";
    settings.pkg.uploadUrl="../../samsungsound/services/participants/register";
    settings.pkg.subscribeUrl="../../samsungsound/services/edms/subscribe";
    settings.pkg.omiture="s_code_microsite_m.com.js";
    settings.pkg.omitureName="samsungsound_mobile";
    gulp.src(['./*.html']).pipe(template(settings)).pipe(gulp.dest(settings.build));
    */
});

gulp.task('releasemobile',function(){

    settings.build="./Release/Singapore/samsungsound-mobile";
    var mobile={
        release:isRelease,
        insert:require('./nodejs/insert'),
        uploadUrl:"../../samsungsound/services/participants/register",
        subscribeUrl:"../../samsungsound/services/edms/subscribe",
        omiture:"s_code_microsite_m.com.js",
        omitureName:"samsungsound_mobile",
        serviceServer:"../../samsungsound/",
        winConsole:"window.console={log:function(msg){ }};"
    };
    gulp.src('./js/app.js').pipe(template(mobile)).pipe(closureCompiler({compilerPath: 'bower_components/closure-compiler/compiler.jar',fileName: 'app.js'})).pipe(gulp.dest(settings.build+'/js/'));
    //gulp.src('./js/app.js').pipe(template(mobile)).pipe(gulp.dest(settings.build+'/js/'));
    gulp.src(['./*.html']).pipe(template(settings)).pipe(gulp.dest(settings.build));
    gulp.src(['./slides/*.html']).pipe(template(settings)).pipe(gulp.dest(settings.build+'/slides/'));
    gulp.src(['./slides/partials/.html']).pipe(template(settings)).pipe(gulp.dest(settings.build+'/slides/partials/'));   
    gulp.src("./scss/*.scss").pipe(sass({style: 'compressed'})).pipe(minifyCSS({compatibility:"ie7"})).pipe(gulp.dest(settings.build+'/css/'));
    
    gulp.src(["./production/js/venders/**", "!./production/js/venders/Thumbs.db"]).pipe(gulp.dest(settings.build+'/js/venders/'));
    gulp.src(["./production/assets/**", "!./production/assets/Thumbs.db"]).pipe(gulp.dest(settings.build+'/assets/'));
    gulp.src(["./production/css/img/**","!./production/css/img/Thumbs.db"]).pipe(gulp.dest(settings.build+'/css/img/'));
    gulp.src(["./production/css/fonts/**","!./production/css/fonts/Thumbs.db"]).pipe(gulp.dest(settings.build+'/css/fonts/'));
    gulp.src(["./production/css/venders/**","!./production/css/venders/Thumbs.db"]).pipe(gulp.dest(settings.build+'/css/venders/'));
     
});
//
gulp.task('insert', function(){

     gulp.src(['../*.html'])
    .pipe(template(settings))
    .pipe(gulp.dest(settings.build));
    
     gulp.src(['../*.php'])
    .pipe(template(settings))
    .pipe(gulp.dest(settings.build));

     gulp.src(['../Korea/index.html']).pipe(template(settings)).pipe(gulp.dest(settings.build+"/Korea/"));
     gulp.src(['../slides/*.html','./slides/*.php'])
    .pipe(template(settings))
    .pipe(gulp.dest(settings.build+'/slides/'));

    gulp.src(['../slides/partials/*.html'])
    .pipe(template(settings))
    .pipe(gulp.dest(settings.build+'/slides/partials/'));    

    
     /* gulp.src('./js/app.js')
    .pipe(template(settings))
    .pipe(closureCompiler({
         compilerPath: 'bower_components/closure-compiler/compiler.jar',
         fileName: 'app.js'
    }))
    .pipe(gulp.dest(settings.build+'/js/'));
     */
     
     gulp.src('../js/app.js')
    .pipe(template(settings))
    .pipe(gulp.dest(settings.build+'/js/'));
    
   
    /*
     gulp.src('./views/slides/account/*.html')
    .pipe(template(settings))
    .pipe(gulp.dest(settings.build+'/views/slides/account/'));

     gulp.src('./js/*.js')
    .pipe(template(settings))
    .pipe(gulp.dest(settings.build+'/js/'));
    */
     
});

gulp.task('copy', function(){
     gulp.src([settings.build+"/*"])
    .pipe(gulp.dest(settings.dest));
    
     gulp.src([settings.build+"/views/*"])
    .pipe(gulp.dest(settings.dest+"/views/"));

     gulp.src([settings.build+"/views/slides/*"])
    .pipe(gulp.dest(settings.dest+"/views/slides/"));

     gulp.src([settings.build+"/views/slides/account/*"])
    .pipe(gulp.dest(settings.dest+"/views/slides/account/"));

     gulp.src([settings.build+"/js/*"])
    .pipe(gulp.dest(settings.dest+"/js/"));

    gulp.src([settings.build+"/js/*"])
    .pipe(gulp.dest(settings.dest+"/js/"));

    gulp.src([settings.build+"/css/*"])
    .pipe(gulp.dest(settings.dest+"/css/"));
});
 
gulp.task('sass',function(){
    //.pipe(minifyCSS({compatibility:"ie7"}))

    return gulp.src("../scss/*.scss").pipe(sass({style: 'compressed'})).pipe(gulp.dest(settings.build+'/css/'));
});

gulp.task('default', function () {
    //gulp.src(['index.html','./'./pages/win-tickets/*.html', /*.html','./views/**/*.html','./views/**/**/*.html','./js/*.js','./scss/*.scss']).pipe(
    gulp.src(files).pipe(    
        watch(files,function(files){
            if(isRelease)
            {
                gulp.start("releasedesktop");
            }
            else
            {
                gulp.start("insert","sass");   
            }
        }
    ));
});
