module.exports = function (path, indent, withoutNewLines) {

    var
        //
        fs     = require('fs'),
        lodash = require('lodash'),

        //
        eol,
        reg,

        //
        eolInject,
        regInject,

        //
        str,

        //
        type,

        //
        win;

    try {
        str = fs.readFileSync(path, 'utf-8');
    } catch (error) {
        throw new Error(error);
    }

    //
    if (withoutNewLines !== true) {
        withoutNewLines = false;
    }

    //
    win = str.indexOf('\r') !== -1;

    //
    eol = win ? '\r\n' : '\n';
    reg = win ? '\\r\\n' : '\\n';

    //
    if (str.substr(str.length - eol.length) === eol) {
        str = str.substr(0, str.length - eol.length);
    }

    //
    while (str.indexOf('<%') !== -1) {
        str = lodash.template(str, this);
    }

    //
    if (indent == null) {
        return str;
    }

    //
    if (typeof indent === 'number') {

        if (indent === 0) {
            indent = '';
        } else {

            //
            if (isNaN(indent) || indent < 0 || indent === Infinity) {
                throw new Error('TODO: Invalid number');
            }

            //
            indent = new Array(indent);
            indent = indent.join(' ') + ' ';

        }

    }

    //
    if (typeof indent !== 'string') {
        throw new Error('TODO: Invalid indent');
    }

    //
    eolInject = withoutNewLines ? indent : eol + indent;
    regInject = withoutNewLines ? indent.replace(/\r/g, '\\r').replace(/\n/g, '\\n') + reg : reg + indent + reg;

    // Do the indent
    str = str.replace(new RegExp(reg, 'g'), eolInject);

    // Strip out the "indent" from "empty" lines.
    while (str.indexOf(eolInject + eol) !== -1) {
        str = str.replace(new RegExp(regInject, 'g'), eol + eol);
    }

    // Remove the "indent" from the last line. It won't match the test
    // above because the test doesn't end in an `eol`.
    if (str.substr(str.length - indent.length) === indent) {
        str = str.substr(0, str.length - indent.length);
    }

    // Return the clean and indented file.
    return str;

};
